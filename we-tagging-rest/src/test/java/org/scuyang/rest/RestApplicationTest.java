package org.scuyang.rest;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.redisson.api.RAtomicDouble;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.scuyang.core.impl.RedisServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;


@Slf4j
@SpringBootTest
class RestApplicationTest {
    private static final String key = "kkkk1111";

    @Resource
    private RedisServiceImpl redisServiceImpl;


    @Resource(name = "weTaggingRedissonClient")
    private RedissonClient redissonClient;

    @Test
    void testRedisson(){
        RAtomicDouble atomicDouble = redissonClient.getAtomicDouble(key);

        System.out.println(atomicDouble.get());
        atomicDouble.set(1000.00);
        System.out.println(atomicDouble.get());
    }


    private void subNumber(){

    }


}