package org.scuyang.rest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.junit.jupiter.api.Test;
import org.scuyang.common.model.ImageInfo;
import org.scuyang.common.model.ImageLabel;
import org.scuyang.core.parse.Voc;
import org.scuyang.dao.ImageInfoMapper;
import org.scuyang.dao.LabelInfoMapper;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.*;
import java.math.BigDecimal;
import java.util.*;

@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ImportVocLabelTest {
    @Resource
    private ImageInfoMapper imageInfoMapper;

    @Resource
    private LabelInfoMapper labelInfoMapper;

    @Test
    void importLbale(){
        String imagePostfix = ".tif";
        String folder = "C:\\Users\\zjianfa\\Desktop\\Annotations\\";
        for(String fullName: new File(folder).list()){
            String fileName = FilenameUtils.getBaseName(fullName);
            fileName = fileName.substring(0, 5) + (Integer.valueOf(fileName.substring(5))+1);
            ImageInfo imageInfo = imageInfoMapper.getImageInfoByVersionAndSrcName("类器官数据", fileName + imagePostfix);
            if (imageInfo == null){
                log.error("图片不存在：{}", fileName + imagePostfix);
                continue;
            }
            String labelFilePath = folder + fullName;
            try {
                InputStream input= new FileInputStream(labelFilePath);
                JacksonXmlModule module = new JacksonXmlModule();
                XmlMapper mapper = new XmlMapper(module);
                Voc voc = mapper.readValue(input, Voc.class);
                List<ImageLabel> labels = vocToImageLabel(voc, imageInfo);
                labelInfoMapper.deleteHistory(imageInfo.getId(), 10);
                labelInfoMapper.deleteLabels(imageInfo.getId());
                labelInfoMapper.batchInsert(labels);
                labelInfoMapper.batchInsertHistory(labels, "system");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (JsonMappingException e) {
                e.printStackTrace();
            } catch (JsonParseException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private static Integer categoryId = 1;
    private static Map<String, Integer> classMap = new HashMap<>();
    static {
        classMap.put("清晰类器官", 5);
        classMap.put("模糊类器官", 7);
        classMap.put("杂质", 9);
    }

    private List<ImageLabel> vocToImageLabel(Voc voc, ImageInfo info){
        List<ImageLabel> result = new ArrayList<>();
        for(Voc.Annotation annotation : voc.getObject()){
            ImageLabel label = new ImageLabel();
            label.setImgId(info.getId());
            label.setCategoryId(categoryId);
            label.setClassId(classMap.get(annotation.getName()));

            BigDecimal w = voc.getSize().getWidth();
            BigDecimal h = voc.getSize().getHeight();
            BigDecimal x = annotation.getBndbox().getXmin();
            BigDecimal y = annotation.getBndbox().getYmin();
            BigDecimal ww = annotation.getBndbox().getXmax().subtract(x);
            BigDecimal hh = annotation.getBndbox().getYmax().subtract(y);
            label.setWidth(ww.divide(w, 8, BigDecimal.ROUND_HALF_UP));
            label.setHeight(hh.divide(h, 8, BigDecimal.ROUND_HALF_UP));
            BigDecimal xCenter = x.add(ww.divide(new BigDecimal(2), 8, BigDecimal.ROUND_HALF_UP));
            BigDecimal yCenter = y.add(hh.divide(new BigDecimal(2), 8, BigDecimal.ROUND_HALF_UP));
            label.setCoordinateX(xCenter.divide(w, 8, BigDecimal.ROUND_HALF_UP));
            label.setCoordinateY(yCenter.divide(h, 8, BigDecimal.ROUND_HALF_UP));
            label.setTime(new Date());
            result.add(label);
        }
        return result;
    }
}
