package org.scuyang.rest.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.StringCodec;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class RedissonConfig {

    @Value("${spring.redis.host}")
    private String redisHost;
    @Value("${spring.redis.port}")
    private String redisPort;

    @Value("${spring.redis.db:1}")
    private String redisDb;

    @Bean(name = "weTaggingRedissonClient", destroyMethod = "shutdown")
    public RedissonClient redissonClient() {
        Config config = new Config();
        config.setCodec(new StringCodec()).useSingleServer()
                .setDatabase(Integer.parseInt(redisDb))
                .setAddress("redis://" + redisHost + ":" + redisPort);

        return Redisson.create(config);
    }
}