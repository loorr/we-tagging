package org.scuyang.rest.mq;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBlockingQueue;
import org.redisson.api.RedissonClient;
import org.scuyang.core.constant.RedisConstant;
import org.scuyang.ws.WebSocketSupport;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

@Slf4j
@Component
public class RedisQueueMsgReceiver {

    @Resource(name = "weTaggingRedissonClient")
    private RedissonClient redissonClient;

    @Value("${we-tagging.enable-mq-listener:false}")
    private String enableMqListener;


    @PostConstruct
    public void init() {
        new Thread(()->{startLister();}).start();
    }

    private void startLister(){
        if (!Boolean.parseBoolean(enableMqListener)) {
            log.info("MQ listener is disabled");
            return;
        }
        log.info("======== startLister ==========");
        while (true){
            try {
                RBlockingQueue<String> blockingQueue = redissonClient.getBlockingQueue(RedisConstant.MODEL_TASK_QUEUE);
                if(blockingQueue == null){
                    continue;
                }
                String data = blockingQueue.take();
                JSONObject msgDo = JSONObject.parseObject(data);
                List<String> uids =JSONObject.parseArray(String.valueOf(msgDo.get("uids")), String.class);
                log.info("uids: {}, len:{}", uids, data.getBytes().length);
                if(CollectionUtils.isEmpty(uids)){
                    continue;
                }
                for(String uid: uids){
                    log.info("seed data   uid: {}", uid);
                    WebSocketSupport.pushByUid(String.valueOf(uid), data);
                }
                Thread.sleep(100);
            } catch (InterruptedException e) {
                log.warn(e.getMessage(),e);
            } catch (Exception e){
                log.warn(e.getMessage(),e);
            }
        }
    }
}
