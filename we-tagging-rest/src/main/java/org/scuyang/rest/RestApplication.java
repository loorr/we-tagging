package org.scuyang.rest;

import cn.dev33.satoken.SaManager;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Slf4j
@EnableTransactionManagement
@EnableAsync
@MapperScan("org.scuyang.dao")
@EnableFeignClients(basePackages = {
        "org.example.account.center.api",
        "org.scuyang.rest.api.feign"
})
@EnableScheduling
@SpringBootApplication(scanBasePackages = {"org.scuyang.rest", "org.scuyang.core", "org.scuyang.ws"})
public class RestApplication {
    public static void main(String[] args) {
        SpringApplication.run(RestApplication.class, args);
        log.info("启动成功：Sa-Token配置如下：{} ", SaManager.getConfig());
    }
}
