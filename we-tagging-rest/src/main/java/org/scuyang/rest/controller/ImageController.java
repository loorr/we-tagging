package org.scuyang.rest.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.stp.StpUtil;
import com.tove.web.infra.common.Response;
import lombok.extern.log4j.Log4j2;
import org.scuyang.core.ApproveService;
import org.scuyang.core.ImageService;
import org.scuyang.core.LabelConfigService;
import org.scuyang.rest.api.ImageApi;
import org.scuyang.rest.api.req.GetImageFeatureReq;
import org.scuyang.rest.api.req.RefreshImageLabelReq;
import org.scuyang.rest.api.req.UpdateImageFeatureReq;
import org.scuyang.rest.api.req.image.GetAllImageVersionReq;
import org.scuyang.rest.api.req.image.GetImageGroupsReq;
import org.scuyang.rest.api.vo.ImageFeatureVo;
import org.scuyang.rest.api.vo.ImageGroupVo;
import org.scuyang.rest.api.vo.ImageVersionVo;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Log4j2
@RestController
public class ImageController implements ImageApi {

    @Resource
    private LabelConfigService labelConfigService;

    @Resource
    private ImageService imageService;

    @Resource
    private ApproveService approveService;

    @SaCheckLogin
    @Override
    public Response<List<ImageVersionVo>> getAllImageVersion(GetAllImageVersionReq req) {
        req.setUid(StpUtil.getLoginIdAsLong());
        log.info("api getAllImageVersion req:{}", req);
        if (req == null || !req.getChangeToApproval()){
            return Response.getOk(labelConfigService.getAllImageVersion());
        }
        return Response.getOk(approveService.getAllImageVersion(req));
    }


    @SaCheckLogin
    @Override
    public Response<List<ImageGroupVo>> getImageGroups(GetImageGroupsReq req) {
        req.setUid(StpUtil.getLoginIdAsLong());
        log.info("api getImageGroups req:{}", req);
        return Response.getOk(labelConfigService.getImageGroups(req));
    }

    @SaCheckLogin
    @Override
    public Response<ImageFeatureVo> getImageFeature(GetImageFeatureReq req) {
        ImageFeatureVo imageFeatureVo = imageService.getImageFeature(req);
        return Response.getOk(imageFeatureVo);
    }

    @SaCheckLogin
    @Override
    public Response<Boolean> updateImageFeature(UpdateImageFeatureReq req) {
        Boolean result = imageService.updateImageFeature(req);
        return Response.getOk(result);
    }

    @SaCheckLogin
    @Override
    public Response<Boolean> refreshImageLabel(RefreshImageLabelReq req) {
        return null;
    }

}
