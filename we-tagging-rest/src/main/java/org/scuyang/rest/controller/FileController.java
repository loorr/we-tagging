package org.scuyang.rest.controller;

import lombok.extern.log4j.Log4j2;
import org.scuyang.rest.api.FileApi;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j2
@RestController
public class FileController implements FileApi {
    @Deprecated
    @Override
    public Object uploadImg(MultipartFile img) {
        return null;
    }

    @Deprecated
    @Override
    public void getLabelConfig(MultipartFile file, HttpServletResponse response) {
        try {
            response.sendRedirect("http://localhost:8080/file-manager/add-file");
        } catch (IOException e) {
            log.error("转发错误");
            e.printStackTrace();
        }
    }
}
