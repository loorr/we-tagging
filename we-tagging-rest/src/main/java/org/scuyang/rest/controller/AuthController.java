package org.scuyang.rest.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckRole;
import cn.dev33.satoken.stp.StpUtil;
import com.alibaba.fastjson.JSON;
import com.tove.web.infra.common.Response;
import lombok.extern.log4j.Log4j2;
import org.example.account.center.api.AccountApi;
import org.example.account.center.api.entity.AccountVo;
import org.example.account.center.api.entity.req.GetAccountInfoReq;
import org.scuyang.core.AuthService;
import org.scuyang.core.RedisService;
import org.scuyang.rest.api.AuthApi;
import org.scuyang.rest.api.req.LoginReq;
import org.scuyang.rest.api.req.SignReq;
import org.scuyang.rest.api.req.auth.ModifyUserInfoReq;
import org.scuyang.rest.api.req.auth.UserInfo;
import org.scuyang.ws.WebSocketSupport;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Log4j2
@RestController
public class AuthController implements AuthApi {

    @Resource
    private AuthService authService;

    @Resource
    private RedisService redisService;

    @Resource
    private AccountApi accountApi;

    @Value("${account-center.tenantCode:c5779512-fd63-4f20-bffe-e34c66612640}")
    private String tenantCode;

    @Override
    public Response<UserInfo> login(@RequestBody LoginReq req) {
        log.info(JSON.toJSONString(req));
        UserInfo result = authService.login(req);
        return Response.getOk(result);
    }


    @Override
    public Response<UserInfo> getUserInfo() {
        boolean isLogin = StpUtil.isLogin();
        if (!isLogin) {
            return Response.getOk(null);
        }
        UserInfo userInfo = authService.getUserInfo();
        return Response.getOk(userInfo);
    }

    @Deprecated
    @Override
    public Response<Boolean> isLogin() {
        return Response.getOk(StpUtil.isLogin());
    }

    @Override
    public Response<Boolean> logout() {
        Long uid = Long.valueOf(String.valueOf(StpUtil.getLoginId()));
        redisService.removeOnlineUser(uid);

        WebSocketSupport.releaseSession(String.valueOf(uid));

        StpUtil.logout();
        return Response.getOk(true);
    }

    @SaCheckRole("SUPER_ADMIN")
    @Override
    public Response<Long> sign(@Validated SignReq req) {
        Long result = authService.sign(req);
        return Response.getOk(result);
    }

    @SaCheckLogin
    @Override
    public Response<Boolean> modifyUserInfo(ModifyUserInfoReq req) {
        boolean result = authService.modifyUserInfo(req);
        return Response.getOk(result);
    }

}
