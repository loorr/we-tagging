package org.scuyang.rest.controller;

import com.tove.web.infra.common.Response;
import lombok.extern.log4j.Log4j2;
import org.scuyang.core.FeatureService;
import org.scuyang.rest.api.FeatureApi;
import org.scuyang.rest.api.req.GetImageFeatureReq;
import org.scuyang.rest.api.req.feature.*;
import org.scuyang.rest.api.vo.feature.FeatureItem;
import org.scuyang.rest.api.vo.feature.FeatureVo;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Log4j2
@RestController
public class FeatureController implements FeatureApi {

    @Resource
    private FeatureService featureService;

    @Override
    public Response<FeatureVo> getImageFeature(GetImageFeatureReq req) {
        FeatureVo result = featureService.getImageFeature(req);
        return Response.getOk(result);
    }

    @Override
    public Response<FeatureVo> getImageGroupFeature(GetImageGroupFeatureReq req) {
        FeatureVo result = featureService.getImageGroupFeature(req);
        return Response.getOk(result);
    }

    @Override
    public Response<Boolean> saveImageGroupFeature(SaveImageGroupFeatureReq req) {
        Boolean result = featureService.saveImageGroupFeature(req);
        return Response.getOk(result);
    }

    @Override
    public Response<Boolean> saveImageFeatureConfig(SaveImageFeatureReq req) {
        return Response.getOk(featureService.saveImageFeatureData(req));
    }

    @Override
    public Response<Boolean> addImageFeatureConfig(AddImageFeatureConfigReq req) {
        Boolean result = featureService.addImageFeatureConfig(req);
        return Response.getOk(result);
    }

    @Override
    public Response<Boolean> updateImageFeatureConfig(UpdateImageFeatureConfigReq req) {
        Boolean result = featureService.updateImageFeatureConfig(req);
        return Response.getOk(result);
    }

    @Override
    public Response<Boolean> deleteImageFeatureConfig(DeleteImageFeatureConfigReq req) {
        Boolean result = featureService.deleteImageFeatureConfig(req);
        return Response.getOk(result);
    }

    @Override
    public Response<List<FeatureItem>> getImageFeatureConfig(GetImageFeatureConfigReq req) {
        List<FeatureItem> result = featureService.getImageFeatureConfig(req);
        return Response.getOk(result);
    }
}
