package org.scuyang.rest.controller.admin;

import com.tove.web.infra.common.Response;
import lombok.extern.log4j.Log4j2;
import org.scuyang.rest.api.admin.StatisticApi;
import org.scuyang.rest.api.admin.domain.GetImageStatisticReq;
import org.scuyang.rest.api.admin.domain.ImageStatisticVo;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
public class StatisticController implements StatisticApi {
    @Override
    public Response<ImageStatisticVo> getImageStatistic(GetImageStatisticReq req) {
        return null;
    }
}
