package org.scuyang.rest.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.stp.StpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tove.web.infra.common.BaseErrorCode;
import com.tove.web.infra.common.Response;
import lombok.extern.log4j.Log4j2;
import org.redisson.api.RSet;
import org.redisson.api.RedissonClient;
import org.scuyang.common.model.ImageInfo;
import org.scuyang.common.model.ImageLabel;
import org.scuyang.common.model.ImageLabelAlgorithm;
import org.scuyang.common.model.ModelConfig;
import org.scuyang.common.ws.WsMsgDO;
import org.scuyang.common.ws.WsMsgType;
import org.scuyang.core.ModelConfigService;
import org.scuyang.core.constant.RedisConstant;
import org.scuyang.core.mq.RedisQueueMsgProducer;
import org.scuyang.dao.ImageInfoMapper;
import org.scuyang.dao.LabelInfoMapper;
import org.scuyang.rest.api.AlgorithmApi;
import org.scuyang.rest.api.error.WeTagErrorCode;
import org.scuyang.rest.api.error.WeTagException;
import org.scuyang.rest.api.feign.FeignAlgorithmApi;
import org.scuyang.rest.api.feign.domain.SingleImageRecognizeReq;
import org.scuyang.rest.api.req.algorithm.GetSmartModelListReq;
import org.scuyang.rest.api.req.algorithm.AlgorithmImageReq;
import org.scuyang.rest.api.req.algorithm.SaveAlgorithmLabelReq;
import org.scuyang.rest.api.vo.SmartModelVo;
import org.scuyang.ws.WebSocketSupport;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class AlgorithmController implements AlgorithmApi {

    @Resource
    private FeignAlgorithmApi feignAlgorithmApi;

    @Resource
    private RedissonClient redissonClient;

    @Resource(name = "taskExecutor2")
    private Executor taskExecutor2;

    @Resource
    private ImageInfoMapper imageInfoMapper;

    @Resource
    private LabelInfoMapper labelInfoMapper;

    @Resource
    private ModelConfigService modelConfigService;

    @Resource
    private RedisQueueMsgProducer redisQueueMsgProducer;

    /**
     * redis key: IMAGE_ID: <UID SET>
     * 一、一个用户调用
     * 1. 获取 uid, 存入 redis 对应图片的set
     * 2. 调用模型
     * 3. 模型返回, 并将数据写入ws
     *
     * 二、多个用户同时调用
     * 1. 获取 uid, 存入 redis 对应图片的set
     *
     * 三、一个用户多次调用
     *
     * @param req
     * @return
     */
    @SaCheckLogin
    @Override
    public Response<Boolean> recognizeImage(AlgorithmImageReq req) {
        String uid = String.valueOf(StpUtil.getLoginId());
        log.info("recognizeImage, req: {}, uid: {}", req, uid);

        if (!StringUtils.hasLength(uid )) {
            throw new WeTagException(WeTagErrorCode.NO_LOGIN);
        }
        if (req == null || req.getImgId() == null) {
            throw new WeTagException(BaseErrorCode.ILLEGAL_PARAMETERS);
        }

        ImageInfo imageInfo = imageInfoMapper.getImageInfo(req.getImgId());
        if (imageInfo == null){
            throw new WeTagException(WeTagErrorCode.NO_IMAGE);
        }
        ModelConfig modelConfig = getModel(req, imageInfo);
        if (modelConfig != null){
            List<ImageLabel> labels = labelInfoMapper.getAlgorithmLabels(req.getImgId(), modelConfig.getModelCode(), modelConfig.getModelVersion());
            if (!CollectionUtils.isEmpty(labels)){
                // TODO 从数据库中获取历史标签
                taskExecutor2.execute(()->{
                    WsMsgDO<String> wsMsgDO =new WsMsgDO<>(WsMsgType.SINGLE_IMAGE, JSON.toJSONString(labels));
                    HashSet<String> uids = new HashSet<>();
                    uids.add(uid);
                    wsMsgDO.setUids(uids);
                    String data = JSON.toJSONString(wsMsgDO);
                    WebSocketSupport.pushByUid(uid, data);
                });
                return Response.getOk(true);
            }
        }

        // 如果历史标签中没有
        RSet<String> userSet = redissonClient.getSet(String.format(RedisConstant.IMAGE_ID_SET, req.getImgId()));
        if (userSet.isExists()){
            if (!userSet.contains(uid)){
                userSet.add(uid);
            }
            userSet.expire(5, TimeUnit.MINUTES);
            Response.getOk(true);
        }
        userSet.add(uid);
        userSet.expire(5, TimeUnit.MINUTES);

        String newUri = imageInfo.getUri().replace("//","/");
        String root = "/home/image_root/ossRoot";
        SingleImageRecognizeReq arg = new SingleImageRecognizeReq();
        arg.setImageName(imageInfo.getMd5());
        arg.setImagePath(root + newUri);
        arg.setImgId(imageInfo.getId());
        arg.setModelCode(modelConfig.getModelCode());
        arg.setWidth(req.getWidth());
        arg.setHeight(req.getHeight());
        arg.setX(req.getX());
        arg.setY(req.getY());

        log.info(arg);
        taskExecutor2.execute(()->feignAlgorithmApi.singleImageRecognize(arg));

        return Response.getOk(true);
    }

    private ModelConfig getModel(AlgorithmImageReq req, ImageInfo imageInfo){
        List<ModelConfig> models = modelConfigService.getModelConfig(imageInfo.getVersion());
        if (CollectionUtils.isEmpty(models)){
            return null;
        }
        for(ModelConfig model : models){
            if(model.getModelCode().equals(req.getModelCode())){
                return model;
            }
        }
        return null;
    }


    @SaCheckLogin
    @Override
    public Response<List<SmartModelVo>> getSmartModelList(GetSmartModelListReq req) {
        List<ModelConfig> modelConfigs = modelConfigService.getModelConfig(req.getVersion());
        if (CollectionUtils.isEmpty(modelConfigs)){
            Response.getOk(null);
        }
        List<SmartModelVo> result = modelConfigs.stream().map(o->{
            SmartModelVo vo = new SmartModelVo();
            vo.setModelName(o.getModelName());
            vo.setModelCode(o.getModelCode());
            vo.setModelType(o.getModelType());
            vo.setModelVersion(o.getModelVersion());
            vo.setModelUrl(o.getModelUrl());
            if(StringUtils.hasLength(o.getLabelMapping())){
                try{
                    JSONObject json = JSONObject.parseObject(o.getLabelMapping());
                    vo.setLabelMapping(json);
                }catch (Exception e){
                    log.error("labelMapping parse error", e);
                }
            }
            return vo;
        }).collect(Collectors.toList());
        return Response.getOk(result);
    }


    @SaCheckLogin
    @Override
    public Response<Boolean> saveAlgorithmLabel(SaveAlgorithmLabelReq req) {
        List<ImageLabelAlgorithm> allTagInfo = req.getLabels().stream().map(o->{
            ImageLabelAlgorithm vo = new ImageLabelAlgorithm();
            BeanUtils.copyProperties(o, vo);
            vo.setImgId(req.getImgId());
            vo.setModelCode(req.getModelCode());
            vo.setModelVersion(req.getModelVersion());
            return vo;
        }).collect(Collectors.toList());
        try{
            labelInfoMapper.deleteNewAlgorithmLabel(req.getImgId(), req.getModelCode(), req.getModelVersion());
            labelInfoMapper.batchInsertNewAlgorithmLabels(allTagInfo);
        }catch (Exception e){
            log.error("saveAlgorithmLabel error", e);
            return Response.getFail();
        }
        return Response.getOk(true);
    }
}
