package org.scuyang.rest.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.stp.StpUtil;
import com.alibaba.fastjson.JSON;
import com.tove.web.infra.common.Response;
import lombok.extern.log4j.Log4j2;
import org.scuyang.core.ApproveService;
import org.scuyang.rest.api.ApproveApi;
import org.scuyang.rest.api.req.approve.ApproveImageReq;
import org.scuyang.rest.api.req.approve.RequestApproveReq;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Log4j2
@RestController
public class ApproveController implements ApproveApi {

    @Resource
    private ApproveService approveService;

    @SaCheckLogin
    @Override
    public Response<Boolean> requestApprove(RequestApproveReq req) {
        req.setUid(StpUtil.getLoginIdAsLong());
        log.info("request arg, requestApprove: {}", JSON.toJSONString(req));
        boolean result = approveService.requestApprove(req);
        return Response.getOk(result);
    }

    @SaCheckLogin
    @Override
    public Response<Boolean> approveImage(ApproveImageReq req) {
        req.setUid(StpUtil.getLoginIdAsLong());
        log.info("request arg, approveImage: {}", JSON.toJSONString(req));
        boolean result = approveService.approveImage(req);
        return Response.getOk(result);
    }

    @Deprecated
    @SaCheckLogin
    @Override
    public Response<List<RequestApproveReq>> getMyRequestList() {
        Long uid = StpUtil.getLoginIdAsLong();
        log.info("request arg, getMyRequestList: {}", uid);
        List<RequestApproveReq> result = approveService.getMyRequestList(uid);
        return Response.getOk(result);
    }
}
