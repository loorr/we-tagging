package org.scuyang.rest.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckRole;
import cn.dev33.satoken.stp.StpUtil;
import com.tove.web.infra.common.BaseErrorCode;
import com.tove.web.infra.common.PageHelperUtil;
import com.tove.web.infra.common.Response;
import org.scuyang.core.ApproveService;
import org.scuyang.core.AuthService;
import org.scuyang.core.TagService;
import org.scuyang.rest.api.TaggingApi;
import org.scuyang.rest.api.error.WeTagErrorCode;
import org.scuyang.rest.api.error.WeTagException;
import org.scuyang.rest.api.req.*;
import org.scuyang.rest.api.req.auth.UserInfo;
import org.scuyang.rest.api.req.label.GetImagePageReq;
import org.scuyang.rest.api.vo.ImageGroupNoteVo;
import org.scuyang.rest.api.vo.tag.ImageInfoVo;
import org.scuyang.rest.api.vo.ImageLabelInfoVo;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class TaggingController implements TaggingApi {

    @Resource
    private TagService tagService;

    @Resource
    private AuthService authService;

    @Resource
    private ApproveService approveService;

    @Deprecated
    @Override
    public Response<Boolean> saveTagInfo(SaveTagInfoReq req) {
        try{
            tagService.saveTagInfo(req);
        } catch (WeTagException e){
            throw e;
        }
        return Response.getOk(true);
    }

    @Deprecated
    @Override
    public Response<Boolean> deleteTagInfo(DeleteTagInfoReq req) {
        try{
            tagService.deleteTagInfo(req);
        } catch (WeTagException e){
            throw e;
        }
        return Response.getOk(true);
    }

    @SaCheckLogin
    @Override
    public Response<Boolean> saveAllTagInfo(SaveAllTageInfoReq req) {
        try{
            UserInfo userInfo = authService.getUserInfo();
            if (userInfo == null){
                throw new WeTagException(WeTagErrorCode.NO_LOGIN);
            }
            req.setLastModifyUsername(userInfo.getUsername());
            tagService.saveAllTagInfo(req);
        } catch (WeTagException e){
            throw e;
        }
        return Response.getOk(true);
    }

    @Override
    public Response<ImageLabelInfoVo> getImageLabelInfo(GetImageLabelInfoReq req) {
        ImageLabelInfoVo imageLabelInfoVo = tagService.getImageLabelInfo(req);
        return Response.getOk(imageLabelInfoVo);
    }

    @SaCheckLogin
    @Override
    public Response<PageHelperUtil<ImageInfoVo>> getImageInfoPage(GetImagePageReq req) {
        req.setUid(StpUtil.getLoginIdAsLong());
        PageHelperUtil<ImageInfoVo> pageHelperUtil;
        try{
            if (!req.getChangeToApproval()){
                pageHelperUtil = tagService.getImageInfoPage(req);
            }else{
                pageHelperUtil = approveService.getImageInfoPage(req);
            }
            return Response.getOk(pageHelperUtil);
        } catch (WeTagException e){
            throw e;
        }
    }

    @SaCheckRole("AUDITOR")
    @Override
    public Response<Boolean> changeImageTagState(ConfirmImageHaveTagReq req) {
        Boolean result = tagService.confirmImageHaveTag(req);
        return Response.getOk(result);
    }

    @Override
    public Response<Boolean> addImageGroupNote(AddImageGroupNoteReq req) {
        // TODO 如果为null 暂时不做处理
        if (req.getDescribe() == null || req.getDescribe().isEmpty()){
            return Response.getOk(true);
        }
        if (req.getImgId() == null && !checkGroupParams(req.getImgGroup(), req.getVersion())){
            return Response.getFail(BaseErrorCode.ILLEGAL_PARAMETERS);
        }
        Boolean result = tagService.addImageGroupNote(req);
        return Response.getOk(result);
    }

    @SaCheckLogin
    @Override
    public Response<ImageGroupNoteVo> getImageGroupNote(GetImageGroupNoteReq req) {
        if (req.getImgId() == null && !checkGroupParams(req.getImgGroup(), req.getVersion())){
            return Response.getFail(BaseErrorCode.ILLEGAL_PARAMETERS);
        }
        ImageGroupNoteVo result = tagService.getImageGroupNote(req);
        return Response.getOk(result);
    }


    private boolean checkGroupParams(String imgGroup, String version){
        return StringUtils.hasLength(imgGroup) && StringUtils.hasLength(version);
    }
}
