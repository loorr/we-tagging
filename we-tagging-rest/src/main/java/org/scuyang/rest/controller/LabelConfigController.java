package org.scuyang.rest.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckRole;
import cn.dev33.satoken.stp.StpUtil;
import com.tove.web.infra.common.Response;
import org.scuyang.core.AuthService;
import org.scuyang.core.LabelConfigService;
import org.scuyang.core.constant.CommonConstant;
import org.scuyang.rest.api.LabelConfigApi;
import org.scuyang.rest.api.error.WeTagErrorCode;
import org.scuyang.rest.api.error.WeTagException;
import org.scuyang.rest.api.req.AddOrUpdateLabelReq;
import org.scuyang.rest.api.req.DeleteLabelReq;
import org.scuyang.rest.api.req.auth.UserInfo;
import org.scuyang.rest.api.req.feature.DeleteImageFeatureConfigReq;
import org.scuyang.rest.api.req.label.*;
import org.scuyang.rest.api.req.GetLabelConfigReq;
import org.scuyang.rest.api.req.config.AddOrUpdateCategoryLabelReq;
import org.scuyang.rest.api.vo.LabelConfigVo;
import org.scuyang.rest.api.vo.LabelUsedVo;
import org.scuyang.rest.api.vo.LabelVersionVo;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class LabelConfigController implements LabelConfigApi {

    @Resource
    private LabelConfigService labelConfigService;

    @Resource
    private AuthService authService;

    @Override
    public Response<LabelConfigVo> getLabelConfig(GetLabelConfigReq req) {
        LabelConfigVo labelConfigVo = labelConfigService.getLabelConfig(req);
        return Response.getOk(labelConfigVo);
    }

    @Deprecated
    @Override
    public Response<List<LabelVersionVo>> getAllLabelVersion() {
        return Response.getOk(labelConfigService.getAllLabelVersion(new GetAllLabelVersionReq()));
    }

    @SaCheckLogin
    @Override
    public Response<List<LabelVersionVo>> getAllLabelVersion(GetAllLabelVersionReq req) {
        return Response.getOk(labelConfigService.getAllLabelVersion(req));
    }

    @SaCheckRole(value = {"AUDITOR"})
    @Override
    public Response<Boolean> addLabelVersionSet(AddLabelVersionSetReq req) {
        return Response.getOk(labelConfigService.addLabelVersionSet(req));
    }

    @SaCheckRole(value = {"AUDITOR"})
    @Override
    public Response<Boolean> deleteLabelVersionSet(DeleteImageFeatureConfigReq req) {
        return Response.getOk(labelConfigService.deleteLabelVersionSet(req));
    }

    @SaCheckRole(value = {"AUDITOR"})
    @Override
    public Response<Boolean> updateLabelVersionSet(UpdateLabelVersionSetReq req) {
        return Response.getOk(labelConfigService.updateLabelVersionSet(req));
    }

    @SaCheckRole(value = {"AUDITOR"})
    @Override
    public Response<Boolean> addLabel(AddOrUpdateLabelReq req) {
        Boolean result = labelConfigService.addLabel(req);
        return Response.getOk(result);
    }

    @SaCheckRole(value = {"AUDITOR"})
    @Override
    public Response<Boolean> updateLabel(AddOrUpdateLabelReq req) {
        Boolean result = labelConfigService.updateLabel(req);
        return Response.getOk(result);
    }

    @SaCheckRole(value = {"AUDITOR"})
    @Override
    public Response<Boolean> deleteLabel(DeleteLabelReq req) {
        Boolean result = labelConfigService.deleteLabel(req);
        return Response.getOk(result);
    }

    @SaCheckRole(value = {"AUDITOR"})
    @Override
    public Response<Boolean> addCategoryLabel(AddOrUpdateCategoryLabelReq req) {
        req.setClassName(CommonConstant.CONFIG_SIGN);
        req.setCategoryId(null);
        req.setCategoryId(null);
        Boolean result = labelConfigService.addCategoryLabel(req);
        return Response.getOk(result);
    }

    @SaCheckRole(value = {"AUDITOR"})
    @Override
    public Response<Boolean> updateCategoryLabel(AddOrUpdateCategoryLabelReq req) {
        req.setClassName(CommonConstant.CONFIG_SIGN);
        Boolean result  = labelConfigService.updateCategoryLabel(req);
        return Response.getOk(result);
    }

    @SaCheckLogin
    @Override
    public Response<Boolean> userLabelConfigUpdate(UserLabelConfigUpdateReq req) {
        req.setUid(StpUtil.getLoginIdAsLong());
        Boolean result = labelConfigService.userLabelConfigUpdate(req);
        return Response.getOk(result);
    }

    @SaCheckLogin
    @Override
    public Response<LabelConfigVo> userLabelConfigGet(UserLabelConfigGetReq req) {
        req.setUid(StpUtil.getLoginIdAsLong());
        return Response.getOk(labelConfigService.userLabelConfigGet(req));
    }

    @SaCheckLogin
    @Override
    public Response<Boolean> resetUserLabelConfig(ResetUserLabelConfigReq req) {
        req.setUid(StpUtil.getLoginIdAsLong());
        return Response.getOk(labelConfigService.resetUserLabelConfig(req));
    }

    @SaCheckLogin
    @Override
    public Response<LabelUsedVo> checkLabelUsed(CheckLabelUsedReq req) {
        req.setUid(StpUtil.getLoginIdAsLong());
        return Response.getOk(labelConfigService.checkLabelUsed(req));
    }

    @SaCheckLogin
    @Override
    public Response<Boolean> replaceLabel(ReplaceLabelReq req) {
        UserInfo userInfo = authService.getUserInfo();
        if (userInfo == null){
            throw new WeTagException(WeTagErrorCode.NO_LOGIN);
        }
        req.setLastModifyUsername(userInfo.getUsername());
        req.setUid(StpUtil.getLoginIdAsLong());
        return Response.getOk(labelConfigService.replaceLabel(req));
    }

    @SaCheckLogin
    @Override
    public Response<Boolean> labelResort(LabelResortReq req) {
        req.setUid(StpUtil.getLoginIdAsLong());
        return Response.getOk(labelConfigService.labelResort(req));
    }
}
