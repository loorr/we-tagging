# 制品下载目录
rootPath="/home/yunxiao/dev/"
workPath="${rootPath}/we-tagging-rest/"
dockerfilePath="${rootPath}/we-tagging-rest/docker-qa/Dockerfile-rest-qa"

# 解压到当前目录
tar zxvf "${rootPath}"app.tgz -C $rootPath
# docker 容器&镜像名称
NAME="we-tagging-dev"
# docker 镜像版本
version="v"$(date "+%Y%m%d-%H%M%S")
# build image
docker build  -f $dockerfilePath -t $NAME:$version $workPath

IS_RUNNING=$(docker ps -a | grep $NAME | wc -l)
if [[ $IS_RUNNING == 1 ]]; then
    echo "container $NAME is running..."
    docker stop $NAME
    echo "container $NAME is stop ..."
    docker rm $NAME
    echo "container $NAME is remove ..."
fi
docker stop $NAME
docker rm $NAME
docker run -i -t -d -p 4000:4000 --restart=always --name $NAME $NAME:$version
echo "container $NAME is start ..."
