rootPath=/home/release/we-tag/fontend/cd-label-tool
dockerfilePath="${rootPath}/Dockerfile-PROD"
cd ${rootPath}
git pull

# docker 容器&镜像名称
NAME="we-tagging-prod-page"
# docker 镜像版本
version="v"$(date "+%Y%m%d-%H%M%S")
docker build -f $dockerfilePath -t ${NAME}:${version} ${rootPath}

IS_RUNNING=$(docker ps -a | grep $NAME | wc -l)
if [[ $IS_RUNNING == 1 ]]; then
    echo "container $NAME is running..."
    docker stop $NAME
    echo "container $NAME is stop ..."
    docker rm $NAME
    echo "container $NAME is remove ..."
fi
docker rm $NAME
docker run -i -t -d -p 8080:80 --restart=always --name ${NAME} ${NAME}:${version}
echo "container $NAME is start ..."