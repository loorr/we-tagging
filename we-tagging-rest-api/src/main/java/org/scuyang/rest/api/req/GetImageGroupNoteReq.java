package org.scuyang.rest.api.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel
public class GetImageGroupNoteReq {
    @ApiModelProperty(value = "图片ID")
    private Long imgId;

    @ApiModelProperty(value = "图片人物组")
    private String imgGroup;

    @ApiModelProperty(value = "图片人物组")
    private String version;
}
