package org.scuyang.rest.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel
public class ImageLabelItemVo {

    @ApiModelProperty(value = "标签类编号")
    private Integer categoryId;

    @ApiModelProperty(value = "标签编号")
    private Integer classId;

    @ApiModelProperty(value = "x")
    private BigDecimal coordinateX;

    @ApiModelProperty(value = "y")
    private BigDecimal coordinateY;

    @ApiModelProperty(value = "width")
    private BigDecimal width;

    @ApiModelProperty(value = "height")
    private BigDecimal height;

    @ApiModelProperty(value = "time")
    private Long time;

    @ApiModelProperty(value = "标签类扩展")
    private String labelExt;

    @ApiModelProperty(value = "修改人Uid")
    private String modifyUsername;
}
