package org.scuyang.rest.api.admin;

import com.tove.web.infra.common.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.scuyang.rest.api.admin.domain.GetImageStatisticReq;
import org.scuyang.rest.api.admin.domain.ImageStatisticVo;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@Api(value = "统计相关接口")
public interface StatisticApi {
    @ApiOperation("get image statistic")
    @PostMapping(value = "/statistic/get-image-statistic", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<ImageStatisticVo> getImageStatistic(@RequestBody @Validated GetImageStatisticReq req);
}
