package org.scuyang.rest.api;

import com.tove.web.infra.common.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.scuyang.rest.api.req.GetImageFeatureReq;
import org.scuyang.rest.api.req.feature.*;
import org.scuyang.rest.api.vo.ImageFeatureVo;
import org.scuyang.rest.api.vo.feature.FeatureItem;
import org.scuyang.rest.api.vo.feature.FeatureVo;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Api(value = "图片特征相关,图片集和图片特征是一对一关系")
public interface FeatureApi {
    @ApiOperation("获取图片特征")
    @PostMapping(value = "/feature/get-image-feature", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<FeatureVo> getImageFeature(@RequestBody @Validated GetImageFeatureReq req);

    @ApiOperation("获取图片组特征")
    @PostMapping(value = "/feature/get-image-group-feature", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<FeatureVo> getImageGroupFeature(@RequestBody @Validated GetImageGroupFeatureReq req);

    @ApiOperation("保存图片组特征")
    @PostMapping(value = "/feature/save-image-group-feature", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> saveImageGroupFeature(@RequestBody @Validated SaveImageGroupFeatureReq req);

    @ApiOperation("保存图片特征")
    @PostMapping(value = "/feature/save-image-feature", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> saveImageFeatureConfig(@RequestBody @Validated SaveImageFeatureReq req);

    /** ==== ===== */
    @ApiOperation("新增图片特征配置")
    @PostMapping(value = "/feature/add-image-feature-config", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> addImageFeatureConfig(@RequestBody @Validated AddImageFeatureConfigReq req);

    @ApiOperation("更新图片特征配置")
    @PostMapping(value = "/feature/update-image-feature-config", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> updateImageFeatureConfig(@RequestBody @Validated UpdateImageFeatureConfigReq req);

    @ApiOperation("删除图片特征配置")
    @PostMapping(value = "/feature/delete-image-feature-config", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> deleteImageFeatureConfig(@RequestBody @Validated DeleteImageFeatureConfigReq req);

    @ApiOperation("获取图片集的图片特征配置")
    @PostMapping(value = "/feature/get-image-feature-config", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<List<FeatureItem>> getImageFeatureConfig(@RequestBody @Validated GetImageFeatureConfigReq req);

}
