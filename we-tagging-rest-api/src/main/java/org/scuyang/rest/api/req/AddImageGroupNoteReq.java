package org.scuyang.rest.api.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@ApiModel
public class AddImageGroupNoteReq {
    @ApiModelProperty(value = "图片ID")
    private Long imgId;

    @ApiModelProperty(value = "图片人物组")
    private String imgGroup;

    @ApiModelProperty(value = "version")
    private String version;

    @ApiModelProperty(value = "describe")
    private String describe;
}
