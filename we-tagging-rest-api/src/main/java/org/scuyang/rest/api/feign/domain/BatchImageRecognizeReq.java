package org.scuyang.rest.api.feign.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@ApiModel
public class BatchImageRecognizeReq {

    @ApiModelProperty(value = "请求的uri", hidden = true)
    @NotEmpty
    private String uri = "/batch-seg-img";

    @ApiModelProperty(value = "文件夹路径")
    @NotEmpty
    private String groupFolderPath;

    @ApiModelProperty(value = "group文件夹名字")
    @NotEmpty
    private String groupName;

    @ApiModelProperty(value = "如果不为空, 则迭代查询")
    private String versionFolderPath;
}
