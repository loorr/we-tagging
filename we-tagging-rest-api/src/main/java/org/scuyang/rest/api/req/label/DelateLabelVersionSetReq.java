package org.scuyang.rest.api.req.label;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@ApiModel
public class DelateLabelVersionSetReq {
    @ApiModelProperty(value = "图片数据集")
    @NotNull
    private Long id;
}
