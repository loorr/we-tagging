package org.scuyang.rest.api.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.scuyang.rest.api.vo.ImageLabelItemVo;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@ApiModel
public class SaveAllTageInfoReq {

    @ApiModelProperty(value = "图片ID")
    @NotNull
    private Long imgId;

    @ApiModelProperty(value = "图片扩展字段")
    private String imgExt;

    @ApiModelProperty(value = "modifyUid", hidden = true)
    private String lastModifyUsername;

    @ApiModelProperty(value = "所有标签")
    private List<ImageLabelItemVo> labelItemVos;
}
