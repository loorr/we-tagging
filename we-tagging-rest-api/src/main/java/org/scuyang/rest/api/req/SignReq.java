package org.scuyang.rest.api.req;

import lombok.Data;
import org.scuyang.rest.api.req.auth.RoleEnum;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class SignReq {
    @NotBlank
    @Email
    private String email;

    @NotBlank
    @Size(min = 6, max = 20)
    private String password;

    private List<String> roles;
}
