package org.scuyang.rest.api.admin.domain;

import lombok.Data;

@Data
public class UserStatisticVo {
    private Integer totalTaggedImages;
    private Integer totalTaggedLabels;
    private String username;
}
