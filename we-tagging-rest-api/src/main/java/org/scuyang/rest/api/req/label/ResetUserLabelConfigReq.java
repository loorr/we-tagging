package org.scuyang.rest.api.req.label;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@ApiModel
public class ResetUserLabelConfigReq {
    @ApiModelProperty(value = "用户id", hidden = true)
    private Long uid;

    @ApiModelProperty(value = "标签分组，标签版本")
    @NotEmpty
    private String version;
}
