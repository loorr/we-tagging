package org.scuyang.rest.api.req.algorithm;

import com.tove.web.infra.common.common.BaseIdReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel
public class AlgorithmImageReq extends BaseIdReq {

    @ApiModelProperty(value = "需要调用的模型类型")
    private String modelCode;

    @ApiModelProperty(value = "图片ID")
    private Long imgId;

    @ApiModelProperty(value = "图片组", hidden = true)
    private String imageGroup;

    @ApiModelProperty(value = "x")
    private BigDecimal x = BigDecimal.ONE;
    @ApiModelProperty(value = "y")
    private BigDecimal y = BigDecimal.ONE;
    @ApiModelProperty(value = "w")
    private BigDecimal width = BigDecimal.ONE;
    @ApiModelProperty(value = "h")
    private BigDecimal height = BigDecimal.ONE;
}
