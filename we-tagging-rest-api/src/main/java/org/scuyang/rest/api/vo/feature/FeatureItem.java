package org.scuyang.rest.api.vo.feature;

import lombok.Data;

@Data
public class FeatureItem {
    private Long id;

    private String featureName;
    private String type;
    private String remark;
    private Boolean valid;
    private String value;
    private String dataFormat;
    private String defaultValue;
}
