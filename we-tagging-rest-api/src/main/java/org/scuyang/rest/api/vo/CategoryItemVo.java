package org.scuyang.rest.api.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel
public class CategoryItemVo {
    private Long id;
    private Integer categoryId;
    private String categoryName;
    private String shortcutKey;
    private String color;
    private String remark;
    private Integer priority;
    List<ClassItemVo> classItemVoList;
}
