package org.scuyang.rest.api.req.image;

import com.tove.web.infra.common.common.BaseIdReq;
import lombok.Data;

@Data
public class GetAllImageVersionReq extends BaseIdReq {
    private Boolean changeToApproval = false;
}
