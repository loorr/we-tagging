package org.scuyang.rest.api.req.label;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@ApiModel
public class UserLabelConfigUpdateReq {
    @ApiModelProperty(value = "用户id", hidden = true)
    private Long uid;
    @NotNull
    private Integer classId;
    @NotNull
    private Integer categoryId;

    private String shortcutKey;
    private String color;
    private String remark;
    private Integer priority;
    @NotEmpty
    @Size(min = 1)
    private String version;
}
