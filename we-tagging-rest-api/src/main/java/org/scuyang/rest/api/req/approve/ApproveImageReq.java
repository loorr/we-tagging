package org.scuyang.rest.api.req.approve;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel
public class ApproveImageReq {
    @NotNull
    private Long imageId;

    @NotNull
    private Long userId;

    @ApiModelProperty(value = "图片版本", hidden = true)
    private Long uid;

    @NotNull
    @ApiModelProperty(value = "是否审批通过")
    private Boolean isPass;

    @ApiModelProperty(value = "审批意见")
    private String remark;
}
