package org.scuyang.rest.api.vo.feature;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class FeatureVo {
    private List<FeatureItem> featureList = new ArrayList<>();
}
