package org.scuyang.rest.api.error;

import com.tove.web.infra.common.BaseError;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum WeTagErrorCode implements BaseError {

    NO_RECORD_MATCH("400","没有对应的记录"),
    DUPLICATE_KEY("401", "记录重复"),
    NO_DATA_SAVE("402", "没有可保存的数据"),
    NO_THIS_VERSION("403","没有这个版本，请重新输入"),
    NO_IMAGE_FEATURES("404","没有对应的图片特征"),
    NO_IMAGE("405","没有对应的图片"),
    NO_IMAGES("412","没有对应的图片, 请联系管理员分配权限"),
    NO_LOGIN("406", "未登陆，请登陆"),
    USER_NOT_EXIST("407", "不存在该用户"),
    PWD_MISTAKE("408", "账户或密码错误"),
    NOT_ROLE("409", "没有权限"),
    IMAGE_TAG_FINISH("410", "图片标注已完成, 不能再修改"),
    NO_CATEGORY_LABEL("411", "没有对应的分类标签"),
    IMAGE_VERSION_NOT_EXIST("412", "图片版本不存在"),
    APPROVE_TASK_NOT_EXIST("413", "审批任务不存在"),
    APPROVE_NO_APPROVER("414", "无需审批"),
    APPROVE_REQUEST_DUPLICATE("415", "审批任务已存在，请勿重复提交"),
    LABEL_VERSION_NOT_EXIST("416", "图片集中没有对应标签信息"),
    CANNOT_SAME_LABEL("417", "不能选择相同的标签"),

    CANNOT_REPLACE_CATEGORY("418", "不能替换大类, 请联系管理员"),
    NO_MODEL_CONFIG("419", "没有模型配置");

    private final String code;
    private final String msg;

}
