package org.scuyang.rest.api.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ImageVersionVo {
    private String version;
    private Integer totalNum;
    private Integer haveTagNum;
    private Integer noTagNum;
    private List<ImageGroupVo> imageGroupVoList;

    public ImageVersionVo(){
        this.totalNum = 0;
        this.haveTagNum = 0;
        this.noTagNum = 0;
        this.imageGroupVoList = new ArrayList<>();
    }

    public void addTagNum(Boolean tagState){
        if (tagState){
            this.haveTagNum += 1;
        }else{
            this.noTagNum += 1;
        }
        this.totalNum += 1;
    }
}
