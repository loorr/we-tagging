package org.scuyang.rest.api.feign;


import com.tove.web.infra.common.Response;
import io.swagger.annotations.ApiOperation;
import org.scuyang.rest.api.feign.domain.BatchImageRecognizeReq;
import org.scuyang.rest.api.feign.domain.SingleImageRecognizeReq;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "oss-algorithm-service",url = "${feign.oss-algorithm-service}")
public interface FeignAlgorithmApi {

    @ApiOperation("调用算法接口，批量识别图片")
    @PostMapping(value = "/job/batch-image-recognize", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> batchImageRecognize(@RequestBody @Validated BatchImageRecognizeReq req);

    @ApiOperation("调用算法接口，识别单张图片")
    @PostMapping(value = "/job/single-image-recognize", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> singleImageRecognize(@RequestBody @Validated SingleImageRecognizeReq req);

}
