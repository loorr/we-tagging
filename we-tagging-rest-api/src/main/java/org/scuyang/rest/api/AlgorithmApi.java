package org.scuyang.rest.api;

import com.tove.web.infra.common.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.scuyang.rest.api.req.algorithm.GetSmartModelListReq;
import org.scuyang.rest.api.req.algorithm.AlgorithmImageReq;
import org.scuyang.rest.api.req.algorithm.SaveAlgorithmLabelReq;
import org.scuyang.rest.api.vo.SmartModelVo;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


@Api(value = "Algorithm")
@RequestMapping("/algorithm")
public interface AlgorithmApi {
    @ApiOperation("调用模型, 重新识别图片标签")
    @PostMapping(value = "/recognize-image", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> recognizeImage(@Validated @RequestBody AlgorithmImageReq req);

    @ApiOperation("获取图片集智能识别模型列表")
    @PostMapping(value = "/get-smart-model-list", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<List<SmartModelVo>> getSmartModelList(@RequestBody @Validated GetSmartModelListReq req);

    @ApiOperation("保存智能识别出来的标签")
    @PostMapping(value = "/save-algorithm-label", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> saveAlgorithmLabel(@RequestBody @Validated SaveAlgorithmLabelReq req);
}
