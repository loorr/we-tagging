package org.scuyang.rest.api.vo.user;

import lombok.Data;

import java.util.List;

@Data
public class UserGroupVo {
    private String groupName;
    private List<String> permissions;
    private List<String> roles;
}
