package org.scuyang.rest.api.req.label;


import com.tove.web.infra.common.common.BaseIdReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.scuyang.common.model.LabelConfig;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@ApiModel
public class CheckLabelUsedReq extends BaseIdReq {
    @ApiModelProperty(value = "标签分组，标签版本")
    @NotEmpty
    private String version;

    @ApiModelProperty(value = "大类 或 小类 id")
    @NotNull
    private Long id;

    @ApiModelProperty(value = "标签详细", hidden = true)
    private LabelConfig labelConfig;
}
