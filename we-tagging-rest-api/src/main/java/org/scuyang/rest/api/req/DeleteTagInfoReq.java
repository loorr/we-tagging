package org.scuyang.rest.api.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@ApiModel
public class DeleteTagInfoReq {

    @ApiModelProperty(value = "图片ID")
    @NotNull
    private Long imgId;

    @ApiModelProperty(value = "标签类编号")
    private Integer categoryId;

    @ApiModelProperty(value = "标签编号")
    private Integer classId;

    @ApiModelProperty(value = "x")
    private BigDecimal coordinateX;

    @ApiModelProperty(value = "y")
    private BigDecimal coordinateY;

    @ApiModelProperty(value = "width")
    private BigDecimal width;

    @ApiModelProperty(value = "height")
    private BigDecimal height;
}
