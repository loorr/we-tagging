package org.scuyang.rest.api.req.label;

import com.tove.web.infra.common.common.BaseIdReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@ApiModel
public class ReplaceLabelReq extends BaseIdReq {
    @ApiModelProperty(value = "oldLabelId")
    @NotNull
    private Long oldLabelId;

    @ApiModelProperty(value = "newLabelId， 如果为null 表示删除该标签所有信息")
    private Long newLabelId;

    @ApiModelProperty(value = "图片数据集版本")
    @NotEmpty
    private String imageVersion;

    @ApiModelProperty(value = "标签分组，标签版本")
    @NotEmpty
    private String version;

    @ApiModelProperty(value = "lastModifyUsername", hidden = true)
    private String lastModifyUsername;
}
