package org.scuyang.rest.api.req.approve;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel
public class RequestApproveReq {
    @NotNull
    private Long imageId;

    @ApiModelProperty(value = "图片版本", hidden = true)
    private Long uid;
}
