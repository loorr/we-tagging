package org.scuyang.rest.api.req.label;

import com.tove.web.infra.common.common.BaseIdReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@ApiModel
public class LabelResortReq extends BaseIdReq {
    @ApiModelProperty(value = "标签分组，标签版本")
    @NotEmpty
    private String version;

    @ApiModelProperty(value = "是否用户标签配置")
    private Boolean isUserLabel = false;

    @NotNull
    private List<LabelSortItem> labelSortItems;
}
