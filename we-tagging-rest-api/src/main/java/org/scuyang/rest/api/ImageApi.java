package org.scuyang.rest.api;

import com.tove.web.infra.common.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.scuyang.rest.api.req.GetImageFeatureReq;
import org.scuyang.rest.api.req.RefreshImageLabelReq;
import org.scuyang.rest.api.req.UpdateImageFeatureReq;
import org.scuyang.rest.api.req.image.GetAllImageVersionReq;
import org.scuyang.rest.api.req.image.GetImageGroupsReq;
import org.scuyang.rest.api.vo.ImageFeatureVo;
import org.scuyang.rest.api.vo.ImageGroupVo;
import org.scuyang.rest.api.vo.ImageVersionVo;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Api(value = "图片相关接口")
public interface ImageApi {
    @ApiOperation("获取所有图片数据集版本")
    @GetMapping(value = "/image/get-all-image-version", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<List<ImageVersionVo>> getAllImageVersion(@Validated GetAllImageVersionReq req);

    @ApiOperation("get image group")
    @PostMapping(value = "/image/get-image-groups", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<List<ImageGroupVo>> getImageGroups(@RequestBody @Validated GetImageGroupsReq req);

    @ApiOperation("获取图片特征")
    @PostMapping(value = "/image/get-image-feature", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<ImageFeatureVo> getImageFeature(@RequestBody @Validated GetImageFeatureReq req);

    @ApiOperation("更新图片特征")
    @PostMapping(value = "/image/update-image-feature", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> updateImageFeature(@RequestBody @Validated UpdateImageFeatureReq req);

    @Deprecated
    @ApiOperation("重新调用模型，更新图片标签，调用此接口会导致原有图片标签丢失")
    @PostMapping(value = "/image/refrsh-image-label", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> refreshImageLabel(@RequestBody @Validated RefreshImageLabelReq req);

}