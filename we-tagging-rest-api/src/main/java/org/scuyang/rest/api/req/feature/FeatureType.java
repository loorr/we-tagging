package org.scuyang.rest.api.req.feature;

import lombok.Getter;

@Getter
public enum FeatureType {
    IMAGE,
    IMAGE_GROUP;
}
