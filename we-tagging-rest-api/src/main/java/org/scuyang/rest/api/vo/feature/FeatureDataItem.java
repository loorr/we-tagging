package org.scuyang.rest.api.vo.feature;

import lombok.Data;

@Data
public class FeatureDataItem {
    private Long id;
    private String value;
}
