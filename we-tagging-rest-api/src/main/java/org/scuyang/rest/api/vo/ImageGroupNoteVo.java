package org.scuyang.rest.api.vo;

import lombok.Data;

@Data
public class ImageGroupNoteVo {
    private String describe;
}
