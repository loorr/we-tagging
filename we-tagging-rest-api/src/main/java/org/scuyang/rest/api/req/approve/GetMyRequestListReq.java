package org.scuyang.rest.api.req.approve;

import com.tove.web.infra.common.Page;
import lombok.Data;

import java.util.Date;

@Data
public class GetMyRequestListReq extends Page {
    private String imageVersion;
    private Date startTime;
    private Date endTime = new Date();
    private String status;
}
