package org.scuyang.rest.api.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel
public class GetImageFeatureReq {

    @ApiModelProperty(value = "图片ID")
    @NotNull
    private Long imgId;

    @ApiModelProperty(value = "图片版本")
    private String imageVersion;

}
