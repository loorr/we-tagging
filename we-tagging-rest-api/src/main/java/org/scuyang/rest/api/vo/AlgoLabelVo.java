package org.scuyang.rest.api.vo;

import lombok.Data;

import java.util.List;

@Data
public class AlgoLabelVo {
    private List<ImageLabelItemVo> algorithmLabels;
    private String modelVersion;
    private String modelCode;
}
