package org.scuyang.rest.api.req.feature;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.scuyang.rest.api.vo.feature.FeatureDataItem;
import org.scuyang.rest.api.vo.feature.FeatureItem;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
public class SaveImageFeatureReq {

    @ApiModelProperty(value = "图片ID")
    @NotNull
    private Long imageId;

    @ApiModelProperty(value = "图片特征")
    private List<FeatureDataItem> featureList = new ArrayList<>();

}
