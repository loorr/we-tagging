package org.scuyang.rest.api;



import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

@Api(value = "文件上传和下载")
public interface FileApi {
    @Deprecated
    @ApiOperation("重定向设置")
    @PostMapping(value = "/file/get-label-config-2", produces = MediaType.APPLICATION_JSON_VALUE)
    Object uploadImg(@RequestParam("faceimg") MultipartFile img);

    @Deprecated
    @ApiOperation("重定向设置2")
    @PostMapping(value = "/file/get-label-config-3", produces = MediaType.APPLICATION_JSON_VALUE)
    void getLabelConfig(@RequestBody MultipartFile file, HttpServletResponse response);
}
