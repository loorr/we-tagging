package org.scuyang.rest.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel
public class ImageLabelInfoVo {

    private Long imgId;
    /** 图片资源url */
    private String imgUrl;
    private String imgSrcName;
    @ApiModelProperty(value = "图片扩展字段")
    private String imgExt;
    private String lastModifyUsername;
    /** 图片label信息 */
    private List<ImageLabelItemVo> imageLabels;

    private List<HistoryItem> historyItems;

    private List<AlgoLabelVo> algoLabelVo;
}
