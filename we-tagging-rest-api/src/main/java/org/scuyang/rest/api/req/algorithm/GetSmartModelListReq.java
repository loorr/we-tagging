package org.scuyang.rest.api.req.algorithm;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@ApiModel
public class GetSmartModelListReq {
    @ApiModelProperty(value = "图片集版本")
    @NotEmpty
    private String version;
}
