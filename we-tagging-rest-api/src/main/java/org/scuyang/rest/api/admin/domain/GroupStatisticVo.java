package org.scuyang.rest.api.admin.domain;

import lombok.Data;

@Data
public class GroupStatisticVo {
    private String groupName;
    private Integer totalImages;
    private Integer havaLabelImages;
    private Integer endTagImages;
    private Integer totalLabels;
}
