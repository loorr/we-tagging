package org.scuyang.rest.api;


import com.tove.web.infra.common.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.scuyang.rest.api.req.approve.ApproveImageReq;
import org.scuyang.rest.api.req.approve.RequestApproveReq;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Api(value = "审批")
public interface ApproveApi {

    @ApiOperation("请求审核专家审批")
    @PostMapping(value = "/approve/request-approve", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> requestApprove(@Validated @RequestBody RequestApproveReq req);

    @ApiOperation("专家审批")
    @PostMapping(value = "/approve/approve-image", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> approveImage(@Validated @RequestBody ApproveImageReq req);

    @Deprecated
    @ApiOperation("查看我的请求列表")
    @PostMapping(value = "/approve/get-my-request-list", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<List<RequestApproveReq>> getMyRequestList();

}
