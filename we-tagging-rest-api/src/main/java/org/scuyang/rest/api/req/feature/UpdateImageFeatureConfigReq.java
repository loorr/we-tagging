package org.scuyang.rest.api.req.feature;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Data
@ApiModel
public class UpdateImageFeatureConfigReq {
    @ApiModelProperty(value = "id")
    @NotNull
    private Long id;

    @ApiModelProperty(value = "特征ID")
    private String featureName;

    @ApiModelProperty(value = "类型")
    private String type;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "对应type的数据格式")
    private String dataFormat;

    @ApiModelProperty(value = "默认数据")
    private String defaultValue;


    @ApiModelProperty(value = "类型 IMAGE/IMAGE_GROUP")
    private FeatureType featureType = FeatureType.IMAGE;
}
