package org.scuyang.rest.api.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@ApiModel
public class LoginReq {

    @ApiModelProperty(value = "email")
    @NotEmpty
    private String email;

    @ApiModelProperty(value = "password")
    @NotEmpty
    private String password;

}
