package org.scuyang.rest.api.vo;

import lombok.Data;

@Data
public class LabelVersionVo {
    private String version;
    private String remark;
    private Long id;
}
