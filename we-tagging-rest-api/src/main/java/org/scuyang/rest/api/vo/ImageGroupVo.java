package org.scuyang.rest.api.vo;

import lombok.Data;

@Data
public class ImageGroupVo {
    private String groupName;
    private Integer totalNum;
    private Integer haveTagNum;
    private Integer noTagNum;

    public ImageGroupVo(){
        this.totalNum = 0;
        this.haveTagNum = 0;
        this.noTagNum = 0;
    }


    public void addTagNum(Boolean tagState){
        if (tagState){
            this.haveTagNum += 1;
        }else{
            this.noTagNum += 1;
        }
        this.totalNum += 1;
    }
}
