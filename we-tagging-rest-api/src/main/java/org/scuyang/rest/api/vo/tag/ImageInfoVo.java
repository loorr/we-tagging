package org.scuyang.rest.api.vo.tag;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.scuyang.common.ws.domain.vo.LockStateEnum;

@Data
@ApiModel
public class ImageInfoVo {

    private Long id;

    /** 图片资源url */
    private String imgUrl;
    private String imgSrcName;
    private Integer labelNum;
    private Integer labelState;

    private Long imgWidth;
    private Long imgHeight;

    /** 如果是审批模式 */
    private Long requestUid;

    private LockStateEnum lockState;
}
