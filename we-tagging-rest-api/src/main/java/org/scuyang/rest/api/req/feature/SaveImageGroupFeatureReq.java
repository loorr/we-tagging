package org.scuyang.rest.api.req.feature;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.scuyang.rest.api.vo.feature.FeatureDataItem;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Data
@ApiModel
public class SaveImageGroupFeatureReq {
    @ApiModelProperty(value = "图片组")
    @NotEmpty
    private String imageGroup;

    @ApiModelProperty(value = "图片版本")
    @NotEmpty
    private String imageVersion;

    @ApiModelProperty(value = "图片特征")
    private List<FeatureDataItem> featureList = new ArrayList<>();
}
