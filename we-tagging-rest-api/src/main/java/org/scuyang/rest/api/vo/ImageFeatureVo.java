package org.scuyang.rest.api.vo;

import lombok.Data;

import java.util.Map;

@Data
public class ImageFeatureVo {
    private Map<String, Boolean> featureMap;
}
