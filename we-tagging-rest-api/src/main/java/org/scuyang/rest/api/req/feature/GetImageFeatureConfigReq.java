package org.scuyang.rest.api.req.feature;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@ApiModel
public class GetImageFeatureConfigReq {
    @ApiModelProperty(value = "图片ID")
    @NotEmpty
    private String imageVersion;

    @ApiModelProperty(value = "类型 IMAGE/IMAGE_GROUP")
    private FeatureType featureType = FeatureType.IMAGE;
}
