package org.scuyang.rest.api.req.feature;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@ApiModel
public class GetImageGroupFeatureReq {

    @ApiModelProperty(value = "图片组")
    @NotEmpty
    private String imageGroup;

    @ApiModelProperty(value = "图片版本")
    private String imageVersion;

}
