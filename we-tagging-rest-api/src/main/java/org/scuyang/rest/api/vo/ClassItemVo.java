package org.scuyang.rest.api.vo;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@ApiModel
public class ClassItemVo {
    private Long id;
    private Integer classId;
    private String className;
    private String shortcutKey;
    private String abnormalEnum;
    private String color;
    private String remark;
    private Integer priority;
}
