package org.scuyang.rest.api.error;

import com.tove.web.infra.common.BaseError;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WeTagException extends RuntimeException implements BaseError {
    private String code;
    private String msg;

    public  WeTagException(BaseError baseError){
        this.code = baseError.getCode();
        this.msg = baseError.getMsg();
    }
}
