package org.scuyang.rest.api.req.feature;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel
public class DeleteImageFeatureConfigReq {
    @ApiModelProperty(value = "id")
    @NotNull
    private Long id;
}
