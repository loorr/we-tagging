package org.scuyang.rest.api;

import com.tove.web.infra.common.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.scuyang.rest.api.req.AddOrUpdateLabelReq;
import org.scuyang.rest.api.req.DeleteLabelReq;
import org.scuyang.rest.api.req.feature.DeleteImageFeatureConfigReq;
import org.scuyang.rest.api.req.label.*;
import org.scuyang.rest.api.req.GetLabelConfigReq;
import org.scuyang.rest.api.req.config.AddOrUpdateCategoryLabelReq;
import org.scuyang.rest.api.vo.LabelConfigVo;
import org.scuyang.rest.api.vo.LabelUsedVo;
import org.scuyang.rest.api.vo.LabelVersionVo;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.BitSet;
import java.util.List;


@Api(value = "获取标签配置信息")
public interface LabelConfigApi {
    @ApiOperation("获取标签配置信息")
    @PostMapping(value = "/label-config/get-label-config", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<LabelConfigVo> getLabelConfig(@RequestBody @Validated GetLabelConfigReq req);

    @Deprecated
    @ApiOperation("获取所有标签版本信息")
    @GetMapping(value = "/label-config/get-all-label-version", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<List<LabelVersionVo>> getAllLabelVersion();

    @ApiOperation("标签配置集-获取所有标签版本信息")
    @PostMapping(value = "/label-config/get-all-label-version", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<List<LabelVersionVo>> getAllLabelVersion(@RequestBody @Validated GetAllLabelVersionReq req);

    @ApiOperation("标签配置集-添加")
    @PostMapping(value = "/label-config/add-label-version-set", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> addLabelVersionSet(@RequestBody @Validated AddLabelVersionSetReq req);

    @ApiOperation("标签配置集-删除")
    @PostMapping(value = "/label-config/delete-label-version-set", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> deleteLabelVersionSet(@RequestBody @Validated DeleteImageFeatureConfigReq req);

    @ApiOperation("标签配置集-更新")
    @PostMapping(value = "/label-config/update-label-version-set", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> updateLabelVersionSet(@RequestBody @Validated UpdateLabelVersionSetReq req);

    @ApiOperation("小类-新增标签配置信息")
    @PostMapping(value = "/label-config/add-label", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> addLabel(@RequestBody @Validated AddOrUpdateLabelReq req);

    @ApiOperation("小类-更新标签配置信息")
    @PostMapping(value = "/label-config/update-label", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> updateLabel(@RequestBody @Validated AddOrUpdateLabelReq req);

    @ApiOperation("删除标签配置信息（大类 or 小类）")
    @PostMapping(value = "/label-config/delete-label", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> deleteLabel(@RequestBody @Validated DeleteLabelReq req);

    @ApiOperation("大类-新增")
    @PostMapping(value = "/label-config/add-category-label", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> addCategoryLabel(@RequestBody @Validated AddOrUpdateCategoryLabelReq req);

    @ApiOperation("大类-更新")
    @PostMapping(value = "/label-config/update-category-label", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> updateCategoryLabel(@RequestBody @Validated AddOrUpdateCategoryLabelReq req);

    @ApiOperation("用户标签配置更新")
    @PostMapping(value = "/label-config/user-label-config-update", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> userLabelConfigUpdate(@RequestBody @Validated UserLabelConfigUpdateReq req);

    @ApiOperation("用户标签配置获取")
    @PostMapping(value = "/label-config/user-label-config-get", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<LabelConfigVo> userLabelConfigGet(@RequestBody @Validated UserLabelConfigGetReq req);

    @ApiOperation("重置用户标签配置")
    @PostMapping(value = "/label-config/user-label-config-reset", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> resetUserLabelConfig(@RequestBody @Validated ResetUserLabelConfigReq req);

    @ApiOperation("查看标签被使用情况")
    @PostMapping(value = "/label-config/check-label-used", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<LabelUsedVo> checkLabelUsed(@RequestBody @Validated CheckLabelUsedReq req);

    @ApiOperation("替换标签")
    @PostMapping(value = "/label-config/replace-label", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> replaceLabel(@RequestBody @Validated ReplaceLabelReq req);

    @ApiOperation("用户标签/标签排序")
    @PostMapping(value = "/label-config/label-resort", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> labelResort(@RequestBody @Validated LabelResortReq req);

}
