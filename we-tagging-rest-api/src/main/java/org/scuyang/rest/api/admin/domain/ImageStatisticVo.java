package org.scuyang.rest.api.admin.domain;

import lombok.Data;

import java.util.List;

@Data
public class ImageStatisticVo {
    private Integer totalImages;
    private Integer totalGroups;
    private Integer totalLabels;
    private Integer havaLabelImages;
    private Integer endTagImages;

    private List<GroupStatisticVo> groupStatisticVos;
    private List<UserStatisticVo> userStatisticVos;
}

