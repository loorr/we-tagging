package org.scuyang.rest.api.req.auth;

import lombok.Data;
import org.scuyang.rest.api.vo.user.UserGroupVo;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserInfo {
    private Long uid;
    private String email;
    private String token;
    private String username;

    private List<String> permissions = new ArrayList<>();
    private List<String> roles = new ArrayList<>();
    private List<UserGroupVo> userGroupList = new ArrayList<>(0);
}
