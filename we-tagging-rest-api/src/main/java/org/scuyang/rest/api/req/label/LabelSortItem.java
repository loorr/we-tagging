package org.scuyang.rest.api.req.label;

import lombok.Data;

@Data
public class LabelSortItem {
    private Long id;
    private Integer priority;
}
