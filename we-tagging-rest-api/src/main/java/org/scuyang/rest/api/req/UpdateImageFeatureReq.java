package org.scuyang.rest.api.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class UpdateImageFeatureReq {
    @ApiModelProperty(value = "图片ID")
    @NotNull
    private Long imgId;

    @ApiModelProperty(value = "image features")
    @NotNull
    private List<String> imageFeatures;
}
