package org.scuyang.rest.api.req.label;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@ApiModel
public class AddLabelVersionSetReq {

    @ApiModelProperty(value = "图片数据集")
    @NotEmpty
    private String imageVersion;

    @ApiModelProperty(value = "标签数据集名称")
    @NotEmpty
    private String version;

    @ApiModelProperty("备注")
    private String remark;
}
