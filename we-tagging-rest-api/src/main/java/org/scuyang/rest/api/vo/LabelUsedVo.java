package org.scuyang.rest.api.vo;

import lombok.Data;

@Data
public class LabelUsedVo {
    private Integer usedImages;
    private Integer usedLabels;
}
