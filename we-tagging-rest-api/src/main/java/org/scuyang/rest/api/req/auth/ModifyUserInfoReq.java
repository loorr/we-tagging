package org.scuyang.rest.api.req.auth;

import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class ModifyUserInfoReq {
    @NotEmpty
    private String oldPassword;

    @NotEmpty
    @Size(min = 6, max = 20)
    private String newPassword;
}
