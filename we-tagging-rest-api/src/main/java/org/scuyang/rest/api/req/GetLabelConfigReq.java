package org.scuyang.rest.api.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@ApiModel
public class GetLabelConfigReq {
    @ApiModelProperty(value = "标签分组，标签版本")
    @NotEmpty
    private String version;

    @ApiModelProperty(value = "图片数据集版本")
    // @NotEmpty
    private String imageVersion;
}
