package org.scuyang.rest.api.admin.domain;

import com.tove.web.infra.common.common.BaseIdReq;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Data
public class GetImageStatisticReq extends BaseIdReq {
    @NotEmpty
    private String imageVersion;

    private Date startDateTime;
    private Date endDateTime;
}
