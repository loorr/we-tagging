package org.scuyang.rest.api.req;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@Data
@ApiModel
public class DeleteLabelReq {
    @NotNull
    private Integer categoryId;
    private Integer classId;

    @NotEmpty
    private String version;
}
