package org.scuyang.rest.api.vo;

import lombok.Data;

import java.util.List;

@Data
public
class HistoryItem{
    private Long timestamp;
    private String lastModifyUsername;
    private List<ImageLabelItemVo> imageLabels;
}