package org.scuyang.rest.api.req.label;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@ApiModel
public class GetAllLabelVersionReq {

    // TODO
    @ApiModelProperty(value = "图片数据集")
    private String imageVersion;

}
