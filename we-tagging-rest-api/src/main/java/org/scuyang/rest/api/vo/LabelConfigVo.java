package org.scuyang.rest.api.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel
public class LabelConfigVo {

    private String version;
    List<CategoryItemVo> categoryItemVoList;

}
