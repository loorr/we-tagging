package org.scuyang.rest.api.req.algorithm;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@ApiModel
public class SaveAlgorithmLabelReq {

    @ApiModelProperty(value = "图片ID")
    @NotNull
    private Long imgId;

    @Size(min = 1)
    @ApiModelProperty(value = "标签列表")
    private List<AlgorithmLabelItem> labels;

    @NotEmpty
    @ApiModelProperty(value = "model code")
    private String modelCode;

    @NotEmpty
    @ApiModelProperty(value = "model version")
    private String modelVersion;

}
