package org.scuyang.rest.api.req.image;

import com.tove.web.infra.common.common.BaseIdReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.scuyang.rest.api.vo.feature.FeatureDataItem;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class GetImageGroupsReq extends BaseIdReq {


    @ApiModelProperty(value = "图片版本")
    @NotEmpty
    private String imageVersion;

    @ApiModelProperty(value = "groupNameFilter")
    private String groupNameFilter;

    @ApiModelProperty(value="image group feature")
    private List<FeatureDataItem> groupFeatureFilter;

    @ApiModelProperty(value="image group", hidden = true)
    private List<String> imageGroups;

    // 基于图片特征搜索
    @ApiModelProperty(value="打标签状态")
    private Boolean labelStateFilter;

    @ApiModelProperty(value="标签数量")
    private Boolean havaLabelFilter;

    @ApiModelProperty(value="最后修改人")
    private String lastModifyUsernameFilter;

    @ApiModelProperty(value="最后修改时间")
    private String minModifyTimeFilter;

    @ApiModelProperty(value="最后修改时间")
    private String maxModifyTimeFilter;

    @ApiModelProperty(value="图片名称")
    private String imgSrcNameFilter;

    @ApiModelProperty(value="image feature")
    private List<FeatureDataItem> imgFeatureFilter;
}
