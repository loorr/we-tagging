package org.scuyang.rest.api;

import com.tove.web.infra.common.PageHelperUtil;
import com.tove.web.infra.common.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.scuyang.rest.api.req.*;
import org.scuyang.rest.api.req.label.GetImagePageReq;
import org.scuyang.rest.api.vo.ImageGroupNoteVo;
import org.scuyang.rest.api.vo.tag.ImageInfoVo;
import org.scuyang.rest.api.vo.ImageLabelInfoVo;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@Api(value = "图片标签接口")
public interface TaggingApi {
    @Deprecated
    @ApiOperation("保存单个标签信息, 条件更新，如果存在记录则不更新")
    @PostMapping(value = "/tagging/save-tag-info", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> saveTagInfo(@RequestBody @Validated SaveTagInfoReq req);

    @Deprecated
    @ApiOperation("删除单个标签信息")
    @PostMapping(value = "/tagging/delete-tag-info", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> deleteTagInfo(@RequestBody @Validated DeleteTagInfoReq req);

    @ApiOperation("保存整个图片的所有标签信息，条件更新，如果存在记录则不更新")
    @PostMapping(value = "/tagging/save-all-tag-info", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> saveAllTagInfo(@RequestBody @Validated SaveAllTageInfoReq req);

    @ApiOperation("获取图片的所有标签信息")
    @PostMapping(value = "/tagging/get-image-label-info", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<ImageLabelInfoVo> getImageLabelInfo(@RequestBody @Validated GetImageLabelInfoReq req);

    @ApiOperation("分页获取打标签的所有图片")
    @PostMapping(value = "/tagging/get-image-info-page", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<PageHelperUtil<ImageInfoVo>> getImageInfoPage(@RequestBody @Validated GetImagePageReq req);

    @ApiOperation("修改图片标签是否打完状态")
    @PostMapping(value = "/tagging/change-image-tag-state", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> changeImageTagState(@RequestBody @Validated ConfirmImageHaveTagReq req);

    @ApiOperation("给图片组添加评语")
    @PostMapping(value = "/tagging/add-image-group-note", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> addImageGroupNote(@RequestBody @Validated AddImageGroupNoteReq req);

    @ApiOperation("获取图片组评语")
    @PostMapping(value = "/tagging/get-image-group-note", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<ImageGroupNoteVo> getImageGroupNote(@RequestBody @Validated GetImageGroupNoteReq req);
}
