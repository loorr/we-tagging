package org.scuyang.rest.api.req.auth;

import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor
@Getter
public enum RoleEnum {
    ADMIN,
    /** TESTER */
    TESTER,
    /** 普通用户*/
    USER,
    /** 超级管理员 */
    SUPER_ADMIN,
    /** 审核专家 */
    AUDITOR,
    ;
}
