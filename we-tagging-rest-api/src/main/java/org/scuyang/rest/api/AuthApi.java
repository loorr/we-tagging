package org.scuyang.rest.api;

import com.tove.web.infra.common.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.scuyang.rest.api.req.LoginReq;
import org.scuyang.rest.api.req.SignReq;
import org.scuyang.rest.api.req.auth.ModifyUserInfoReq;
import org.scuyang.rest.api.req.auth.UserInfo;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Api(value = "鉴权相关")
public interface AuthApi {

    @ApiOperation("登陆接口")
    @PostMapping(value = "/auth/login", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<UserInfo> login(@Validated @RequestBody LoginReq req);

    @ApiOperation("获取用户信息")
    @GetMapping(value = "/auth/get-user-info", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<UserInfo> getUserInfo();

    @Deprecated
    @ApiOperation("[未来会移除] 是否登陆")
    @GetMapping(value = "/auth/is-login", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> isLogin();

    @ApiOperation("登出")
    @GetMapping(value = "/auth/login-out", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> logout();

    @ApiOperation("修改密码")
    @PostMapping(value = "/auth/modify-user-info", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> modifyUserInfo(@Validated @RequestBody ModifyUserInfoReq req);

    @ApiOperation("需超级管理员权限: 注册用户")
    @PostMapping(value = "/auth/sign", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Long> sign(@Validated @RequestBody  SignReq req);

    // 需超级管理员权限: 添加用户图片数据集版本权限


}
