package org.scuyang.rest.api.req.algorithm;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@ApiModel
public class AlgorithmLabelItem {

    @ApiModelProperty(value = "标签类编号")
    private Integer categoryId;

    @ApiModelProperty(value = "标签编号")
    private Integer classId;

    @NotNull
    @ApiModelProperty(value = "x")
    private BigDecimal coordinateX;

    @NotNull
    @ApiModelProperty(value = "y")
    private BigDecimal coordinateY;

    @NotNull
    @ApiModelProperty(value = "width")
    private BigDecimal width;

    @NotNull
    @ApiModelProperty(value = "height")
    private BigDecimal height;

    @NotNull
    @ApiModelProperty(value = "分数")
    private BigDecimal score;

    @ApiModelProperty(value = "model code", hidden = true)
    private String modelCode;

    @ApiModelProperty(value = "model version", hidden = true)
    private String modelVersion;

    @ApiModelProperty(value = "image id", hidden = true)
    private Long imgId;
}
