package org.scuyang.rest.api.vo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

@Data
public class SmartModelVo {
    private String modelName;
    private String modelCode;
    private String modelVersion;

    /** FONTEND, BACKEND */
    private String modelType;
    private String modelUrl;
    private JSONObject labelMapping;
}
