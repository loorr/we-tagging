package org.scuyang.rest.api.req;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NonNull;


@Data
@ApiModel
public class RefreshImageLabelReq {

    @ApiModelProperty(value = "imgId")
    @NonNull
    private Long imageId;

}
