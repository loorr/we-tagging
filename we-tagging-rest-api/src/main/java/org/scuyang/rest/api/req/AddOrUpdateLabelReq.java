package org.scuyang.rest.api.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@ApiModel
public class AddOrUpdateLabelReq {

    private Long id;

    @ApiModelProperty(value = "categoryId", hidden = true)
    private Integer categoryId;

    @ApiModelProperty(value = "categoryId", hidden = true)
    private Integer classId;

    private String categoryName;

    private String className;

    private String shortcutKey;
    private String color;

    @NotEmpty
    @Size(min = 1)
    private String version;

    private String remark;
    private Integer priority;
}
