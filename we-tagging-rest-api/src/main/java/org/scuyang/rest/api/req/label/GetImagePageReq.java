package org.scuyang.rest.api.req.label;

import com.tove.web.infra.common.BaseErrorCode;
import com.tove.web.infra.common.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.scuyang.rest.api.error.WeTagException;
import org.scuyang.rest.api.vo.feature.FeatureDataItem;
import org.scuyang.rest.api.vo.feature.FeatureItem;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@ApiModel
public class GetImagePageReq extends Page {

    private static final Set<String> GET_IMAGE_PAGE_REQ_FILED = new HashSet(2){{
        add("img_src_name");
        add("label_num");
    }};

    @ApiModelProperty(value = "图片数据集版本")
    @NotEmpty
    private String version;

    @ApiModelProperty(value = "图片分组")
    private String imgGroup;

    @ApiModelProperty(value = "图片打标签的状态, 0: 未标注, 1: 已标注, 2: 待审批图片")
    private Integer labelState;

    private Boolean changeToApproval = false;

    @ApiModelProperty(value="uid", hidden = true)
    private Long uid;

    @ApiModelProperty(value="feature")
    private List<FeatureDataItem> featureFilter;

    @ApiModelProperty(value="标签数量")
    private Boolean havaLabelFilter;

    @ApiModelProperty(value="最后修改人")
    private String lastModifyUsernameFilter;

    @ApiModelProperty(value="最后修改时间")
    private String minModifyTimeFilter;

    @ApiModelProperty(value="最后修改时间")
    private String maxModifyTimeFilter;

    @ApiModelProperty(value="图片名称")
    private String imgSrcNameFilter;

    @Override
    protected void checkOrderStatement() {
        if (StringUtils.hasLength(super.getOrder())){
            if (!GET_IMAGE_PAGE_REQ_FILED.contains(super.getOrder())){
                throw new WeTagException(BaseErrorCode.ILLEGAL_PARAMETERS);
            }
        }else{
            super.setOrder("img_src_name");
        }
    }
}

