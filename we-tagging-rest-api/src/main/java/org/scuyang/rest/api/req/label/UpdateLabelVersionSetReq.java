package org.scuyang.rest.api.req.label;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel
public class UpdateLabelVersionSetReq {
    @NotNull
    private Long id;

    private String remark;
}
