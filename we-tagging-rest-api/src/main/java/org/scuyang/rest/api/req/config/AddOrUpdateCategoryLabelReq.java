package org.scuyang.rest.api.req.config;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class AddOrUpdateCategoryLabelReq {
    @ApiModelProperty(value = "小类id", hidden = true)
    private Integer classId;

    @ApiModelProperty(value = "小类名称", hidden = true)
    private String className;

    private Integer categoryId;

    private String categoryName;

    private String shortcutKey;
    private String color;
    private String remark;
    private Integer priority;

    @NotEmpty
    @Size(min = 1)
    private String version;
}
