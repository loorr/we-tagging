package org.scuyang.rest.api.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel
public class GetImageLabelInfoReq {

    @ApiModelProperty(value = "图片 version", hidden = true)
    private String imageVersion;

    @ApiModelProperty(value = "图片ID")
    @NotNull
    private Long imgId;

    @ApiModelProperty(value = "是否需要加载历史数据")
    private Boolean needHistory;

    @ApiModelProperty(value = "是否需要加载算法标签")
    private Boolean needAlgorithmLabel;
}
