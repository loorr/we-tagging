CREATE TABLE `label_config` (
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '����id',
    `version` varchar(20) NOT NULL COMMENT '��ǩ����,��ǩ�汾',
    `category_id` int unsigned NOT NULL COMMENT '��ǩ�����',
    `category_name` varchar(20) NOT NULL COMMENT '��ǩ�������',
    `class_id` int unsigned NOT NULL COMMENT '��ǩ����',
    `class_name` varchar(20) NOT NULL COMMENT '��ǩ����',
    `abnormal_state` tinyint(3) NOT NULL DEFAULT 0 COMMENT '�쳣״̬(�쳣, ���ɣ�����)',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '���ݿ����ʱ��',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '���ݿ����ʱ��',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uniq_cate_class` (`category_id`,`class_id`),
    INDEX idx_group_label_num(`version`, `category_id`, `class_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='��ǩ��Ϣ���ñ�';
DROP INDEX uniq_cate_class ON `label_config`;
DROP INDEX idx_group_label_num ON `label_config`;
ALTER TABLE `label_config` ADD UNIQUE idx_group_label_num(`version`, `category_id`, `class_id`) USING BTREE;
alter table label_config add column `model_class_id` int unsigned default NULL COMMENT '模型class id';

CREATE TABLE `image_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '����id',
  `md5` varchar(50) NOT NULL COMMENT 'ͼƬMD5ֵ',
  `version` varchar(20) NOT NULL COMMENT 'ͼƬ�汾',
  `uri` varchar(50) NOT NULL COMMENT 'ͼƬURI��URL = oss.url.prefix + URI',
  `folder` varchar(50) NOT NULL COMMENT 'ͼƬ�洢���ļ���',
  `img_group` varchar(50) NOT NULL COMMENT 'ͼƬ����',
  `img_src_name` varchar(50) NOT NULL COMMENT 'ͼƬԭʼ����',
  `img_size` bigint(20) NOT NULL COMMENT 'ͼƬ��С, KB',
  `img_length` bigint(20) NOT NULL COMMENT 'ͼƬԭʼ���� KB',
  `img_width` bigint(20) NOT NULL COMMENT 'ͼƬԭʼ���, KB',
  `label_state` tinyint(3) NOT NULL DEFAULT 0 COMMENT '�Ƿ���ǩlabel_state(NO[0], ING[1], HAD[2])',
  `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '���ݿ����ʱ��',
  `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '���ݿ����ʱ��',
  INDEX  `uniq_md5` (`md5`(5)),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='ͼƬ��Ϣ';

CREATE TABLE `image_label` (
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '����id',
    `md5` varchar(50) NOT NULL COMMENT 'ͼƬMD5ֵ',
    `version` varchar(20) NOT NULL COMMENT '��ǩ����,��ǩ�汾',
    `category_id` int unsigned NOT NULL COMMENT '��ǩ����',
    `class_id` int unsigned NOT NULL COMMENT '��ǩ���',
    `coordinate_x` decimal(20,15) DEFAULT NULL COMMENT 'x',
    `coordinate_y` decimal(20,15) DEFAULT NULL COMMENT 'y',
    `width` decimal(20,15) DEFAULT NULL COMMENT 'w',
    `height` decimal(20,15) DEFAULT NULL COMMENT 'h',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '���ݿ����ʱ��',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '���ݿ����ʱ��',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uniq_info` (`md5`, `coordinate_x`,`coordinate_y`, `width`, `height`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='��ǩ��Ϣ';

CREATE TABLE `image_group_info` (
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '����id',
    `version` varchar(20) NOT NULL COMMENT 'ͼƬ�汾',
    `img_group` varchar(50) NOT NULL COMMENT 'ͼƬ����',
    `describe` varchar(255) NULL COMMENT '����������',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '���ݿ����ʱ��',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '���ݿ����ʱ��',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='ͼƬ����Ϣ';

CREATE TABLE `asyn_task` (
   `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '����id',
   `task_name` varchar(50) NOT NULL COMMENT '��������',
   `task_code` int unsigned NOT NULL COMMENT '����CODE',
   `task_state` tinyint(3) NOT NULL DEFAULT 0 COMMENT '����״̬',
   `task_start_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '����ʼʱ��',
   `task_end_time` datetime(3)  NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '�������ʱ��',
   `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '���ݿ����ʱ��',
   `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '���ݿ����ʱ��',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='�첽����';

CREATE TABLE `image_label_history` (
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `img_id` bigint(20) NOT NULL COMMENT '图片ID',
    `category_id` int(10) unsigned DEFAULT NULL COMMENT '标签类编号',
    `class_id` int(10) unsigned DEFAULT NULL COMMENT '标签编号',
    `coordinate_x` decimal(20,15) DEFAULT NULL COMMENT 'x',
    `coordinate_y` decimal(20,15) DEFAULT NULL COMMENT 'y',
    `width` decimal(20,15) DEFAULT NULL COMMENT 'w',
    `height` decimal(20,15) DEFAULT NULL COMMENT 'h',
    `time` datetime(3) NOT NULL COMMENT '历史版本插入时间',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    PRIMARY KEY (`id`),
    UNIQUE KEY `unique_index` (`time`, `img_id`,`category_id`,`class_id`,`coordinate_x`,`coordinate_y`,`width`,`height`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='历史标签信息';

CREATE TABLE `image_label_algorithm` (
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `img_id` bigint(20) NOT NULL COMMENT '图片ID',
    `category_id` int(10) unsigned DEFAULT NULL COMMENT '标签类编号',
    `class_id` int(10) unsigned DEFAULT NULL COMMENT '标签编号',
    `coordinate_x` decimal(20,15) DEFAULT NULL COMMENT 'x',
    `coordinate_y` decimal(20,15) DEFAULT NULL COMMENT 'y',
    `width` decimal(20,15) DEFAULT NULL COMMENT 'w',
    `height` decimal(20,15) DEFAULT NULL COMMENT 'h',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    PRIMARY KEY (`id`),
    UNIQUE KEY `unique_index` (`img_id`,`category_id`,`class_id`,`coordinate_x`,`coordinate_y`,`width`,`height`) USING BTREE
) ENGINE=InnoDB  COMMENT='模式识别出来的标签信息';


create table `account`(
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
  `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
   `uid` bigint(20) unsigned unique NOT NULL  COMMENT 'uid',
  `email` varchar(127) unique not null comment '用户邮箱',
  `password` varchar(255) not null comment '密码',
  PRIMARY KEY (`id`)
)ENGINE=InnoDB  COMMENT='用户账户';

create table `user_role`(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `uid` bigint(20) unsigned NOT NULL  COMMENT 'uid',
    `role` varchar(50) not null comment '角色',
    UNIQUE KEY `unique_index` (`uid`, `role`) USING BTREE,
    PRIMARY KEY (`id`)
)ENGINE=InnoDB  COMMENT='用户账户';

# 用户资源权限
CREATE TABLE `permissions`(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `type` varchar(50) not null comment '资源类型',
    `name` varchar(50) not null comment '资源名称',
    `parent_id` bigint(20) unsigned NOT NULL  COMMENT '父级资源id',
    unique key `uni_type` (`type`) USING BTREE,
    PRIMARY KEY (`id`)
)ENGINE=InnoDB  COMMENT='用户资源权限';

create table `relation_role_permission`(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `role_id` bigint(20) unsigned NOT NULL  COMMENT '角色id',
    `permission_id` bigint(20) unsigned NOT NULL  COMMENT '资源id',
    unique key `uni_role_permission` (`role_id`, `permission_id`) USING BTREE,
    PRIMARY KEY (`id`)
)ENGINE=InnoDB  COMMENT='用户资源权限';

CREATE TABLE `app_config` (
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `key` varchar(50) unique NOT NULL COMMENT '配置key',
    `value` text NOT NULL COMMENT '配置value',
    `type` varchar(20) NOT NULL COMMENT '配置类型',
    `valid` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有效',
    PRIMARY KEY (`id`),
) ENGINE=InnoDB DEFAULT COMMENT='应用配置';

CREATE TABLE `user_label_config` (
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `uid` bigint(20) unsigned NOT NULL  COMMENT 'uid',
    `version` varchar(20) NOT NULL COMMENT '标签分组,标签版本',
    `category_id` int(10) unsigned NOT NULL COMMENT '标签类别编号',
    `class_id` int(10) unsigned NOT NULL COMMENT '标签类编号',
    `category_name` varchar(20) NOT NULL COMMENT '标签类别描述',
    `class_name` varchar(20) NOT NULL COMMENT '标签描述',
    `shortcut_key` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '快捷键',
    `color` varchar(20) DEFAULT NULL COMMENT '颜色值',
    `remark` varchar(255) DEFAULT NULL COMMENT '标签备注',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uniq_cate_class` (`uid`,`version`, `category_id`,`class_id`) USING BTREE
) ENGINE=InnoDB COMMENT='用户标签信息配置表';

alter table label_config add column `priority` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '优先级' ;
alter table `user_label_config` add column `priority` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '优先级' ;

alter table image_label_algorithm add column `model_version` varchar(50) DEFAULT '1.0.0' COMMENT '模型版本';
alter table image_label_algorithm add column `score` decimal(10,5) DEFAULT NULL COMMENT '分数';
alter table image_label_algorithm add column  `model_code` varchar(50) DEFAULT NULL COMMENT '模型类别';
ALTER TABLE model_config ADD CONSTRAINT uni_code UNIQUE KEY (model_code);

create table algo_job_log(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `img_id` bigint(20) unsigned NOT NULL  COMMENT '图片id',
    `model_code` varchar(50) DEFAULT NULL COMMENT '模型类别',
    `model_version` varchar(50) DEFAULT '1.0.0' COMMENT '模型版本',
    `state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态',
    unique key `uni_img_model` (`img_id`, `model_code`, `model_version`) USING BTREE,
)ENGINE=InnoDB COMMENT='algo_job_log';
