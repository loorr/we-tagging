-- image_info
SELECT
    id AS img_id,
    REPLACE(replace(uri,'//test1',''),'//','/') as uri
FROM
    `image_info`;

-- image_label
SELECT img_id,category_id,class_id, coordinate_x, coordinate_y, width, height  from image_label
WHERE category_id is not null
ORDER BY img_id desc;

-- image_label_no_fill_202203251642
SELECT img_id,category_id,class_id, coordinate_x, coordinate_y, width, height  from image_label
ORDER BY img_id desc;

SELECT id, category_id, category_name, class_id, class_name FROM label_config
WHERE version = 'v1';
