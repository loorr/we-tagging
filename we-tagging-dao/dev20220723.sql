-- 表单配置表
create table form_config(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `form_name` varchar(255) NOT NULL COMMENT '表单名称',
    `form_desc` varchar(255) NOT NULL COMMENT '表单描述',
    `form_type` varchar(255) NOT NULL COMMENT '表单类型',
    primary key(id)
)engine=innodb default charset=utf8;


create table form_type(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `form_type` varchar(255) NOT NULL COMMENT '表单类型',

    primary key(id)
)engine=innodb default charset=utf8;