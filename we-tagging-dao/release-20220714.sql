create table `account`(
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
  `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
  `uid` bigint(20) unsigned unique NOT NULL  COMMENT 'uid',
  `email` varchar(127) unique not null comment '用户邮箱',
  `password` varchar(255) not null comment '密码',
  PRIMARY KEY (`id`)
)ENGINE=InnoDB  COMMENT='用户账户';

create table `user_role`(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `uid` bigint(20) unsigned NOT NULL  COMMENT 'uid',
    `role` varchar(50) not null comment '角色',
    UNIQUE KEY `unique_index` (`uid`, `role`) USING BTREE,
    PRIMARY KEY (`id`)
)ENGINE=InnoDB  COMMENT='用户账户';

CREATE TABLE `image_label_algorithm` (
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `img_id` bigint(20) NOT NULL COMMENT '图片ID',
    `category_id` int(10) unsigned DEFAULT NULL COMMENT '标签类编号',
    `class_id` int(10) unsigned DEFAULT NULL COMMENT '标签编号',
    `coordinate_x` decimal(20,15) DEFAULT NULL COMMENT 'x',
    `coordinate_y` decimal(20,15) DEFAULT NULL COMMENT 'y',
    `width` decimal(20,15) DEFAULT NULL COMMENT 'w',
    `height` decimal(20,15) DEFAULT NULL COMMENT 'h',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    PRIMARY KEY (`id`),
    UNIQUE KEY `unique_index` (`img_id`,`category_id`,`class_id`,`coordinate_x`,`coordinate_y`,`width`,`height`) USING BTREE
) ENGINE=InnoDB  COMMENT='模式识别出来的标签信息';

CREATE TABLE `image_label_history` (
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `img_id` bigint(20) NOT NULL COMMENT '图片ID',
    `category_id` int(10) unsigned DEFAULT NULL COMMENT '标签类编号',
    `class_id` int(10) unsigned DEFAULT NULL COMMENT '标签编号',
    `coordinate_x` decimal(20,15) DEFAULT NULL COMMENT 'x',
    `coordinate_y` decimal(20,15) DEFAULT NULL COMMENT 'y',
    `width` decimal(20,15) DEFAULT NULL COMMENT 'w',
    `height` decimal(20,15) DEFAULT NULL COMMENT 'h',
    `time` datetime(3) NOT NULL COMMENT '历史版本插入时间',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    PRIMARY KEY (`id`),
    UNIQUE KEY `unique_index` (`time`, `img_id`,`category_id`,`class_id`,`coordinate_x`,`coordinate_y`,`width`,`height`) USING BTREE
) ENGINE=InnoDB COMMENT='历史标签信息';
alter table image_label_history add index `index_img_id` (`img_id`), add index `index_time` (`time`);
alter table image_info add index `index_img_version` (`version`);
alter table image_info add index `index_img_group` (`img_group`);
alter table `image_label_history` add column `label_ext` text default null comment '标签扩展信息';
alter table `image_label_algorithm` add column `label_ext` text default null comment '标签扩展信息';

alter table `image_label_algorithm` add column `model_code` varchar(50) default null comment '模型类别';
alter table `image_label_algorithm` add column `model_version` varchar(50) default null comment '模型版本';
alter table `image_label_algorithm` add column `score` decimal(10,5) default null comment '分数';
alter table `image_label_algorithm` drop index `unique_index`;
alter table `image_label_algorithm` add unique index `unique_index` (`img_id`,`category_id`,`class_id`,`coordinate_x`,`coordinate_y`,`width`,`height`,`model_code`,`model_version`) USING BTREE;

create table `model_config`(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `version` varchar(20) NOT NULL COMMENT '图片数据集版本',
    `model_name` varchar(50) NOT NULL COMMENT '模型名称',
    `model_code` varchar(50) NOT NULL COMMENT '模型code',
    `model_version` varchar(50) NOT NULL COMMENT '模型版本',
    `label_mapping` text default NULL COMMENT '标签映射',
    `model_type` varchar(50) default 'BACKEND' COMMENT '模型类型 BACKEND, FONTEND',
    `model_url` text default NULL COMMENT '模型地址',
    unique key `unique_index` (`version`,`model_code`,`model_version`, `model_type`) USING BTREE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT='图片模型配置';


alter table `image_label` add column `label_ext` text default null comment '标签扩展信息';
alter table `image_info` add column `image_ext` text default null  comment '图片扩展信息';

alter table label_config add column `remark` varchar(255) default null comment '标签备注';
create table `image_version_role`(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `role` varchar(50) not null comment '角色',
    `version` varchar(20) NOT NULL COMMENT '图片数据集版本',
    UNIQUE KEY `uni_role_version` (`role`, `version`) USING BTREE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT='角色数据版本权限';


# 替换原有的tiff图片格式为png格式
select * from image_info ii where version IN ('类器官数据');
update image_info set uri=replace(uri,'.tif','.png') where version IN ('类器官数据');

# 数据更新
update label_config t set t.version = "血细胞标签集V1" where version='v1';
update image_info t set t.version = "血细胞图V1" where version ='test1';

alter table account add column `username` varchar(255) default null comment '用户名';
alter table account add column `login_num` int unsigned default 0 comment '登录次数';
alter table account add column `last_login_time` datetime(3) default null comment '最后登录时间';

create table `role`(
`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
`db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
`db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
`name` varchar(50) not null unique comment '角色名',
`remark` varchar(255) default null comment '角色备注',
PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT='角色';

create table `label_set`(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `name` varchar(50) not null unique comment '标签集名',
    `image_version` varchar(50) NOT NULL COMMENT '图片数据集版本',
    `valid` tinyint(1) unsigned default 1 comment '是否有效',
    `remark` varchar(255) default null comment '标签集备注',
    unique index `uni_name_image_version` (`name`, `image_version`) USING BTREE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT='标签集';


# create table `image_feature_set`(
#     `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
#     `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
#     `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
#     `set_name` varchar(50) not null unique comment '图片特点集名称',
#     `image_version` varchar(50) NOT NULL COMMENT '图片数据集版本',
#     `valid` tinyint(1) unsigned default 1 comment '是否有效',
#     `remark` varchar(255) default null comment '备注',
#     unique index `uni_name_image_version` (`set_name`, `image_version`) USING BTREE,
#     PRIMARY KEY (`id`)
# ) ENGINE=InnoDB COMMENT='图片特征集合';

# 图片特征配置化
create table `image_feature_config`(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `image_version` varchar(50) NOT NULL COMMENT '图片集',
    `feature_name` varchar(50) not null unique comment '图片特点名',
    `type` varchar(50) NOT NULL COMMENT '数据类型',
    `valid` tinyint(1) unsigned default 1 comment '是否有效',
    `remark` varchar(255) default null comment '备注',
    unique index `uni_name_image_version` (`image_version`, `feature_name`) USING BTREE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT='图片特征集配置';
alter table image_feature_config add column `data_format` varchar(1023) default null comment '数据格式';
alter table image_feature_config add column `feature_type` varchar(50) default 'IMAGE' comment '特征类型';
alter table image_feature_config add column `default_value` varchar(1023) default null comment '默认值';
alter table image_feature_config drop index `uni_name_image_version`;
alter table image_feature_config add unique index `uni_name_image_version` (`image_version`, `feature_name`, `feature_type`) USING BTREE;


create table `image_feature_data`(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `feature_id` varchar(50) not null unique comment '图片特点id',
    `image_id` bigint(20) unsigned NOT null COMMENT '图片id',
    `data` varchar(255) default NULL COMMENT '数据',
    unique index `uni_name_image_version` (`image_id`, `feature_id`) USING BTREE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT='图片特征数据';

create table `image_group_feature_data`(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `feature_id` bigint(20) unsigned  not null comment '图片特点id',
    `image_group` varchar(255) NOT null COMMENT '图片组',
    `image_version` varchar(255) NOT null COMMENT '图片集',
    `data` text default NULL COMMENT '数据',
    unique index `uni_image_version_group` (`image_version`, `image_group`, `feature_id`) USING BTREE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT='图片特征数据';


# 增加标签修改人信息
alter table `image_label_history` add column `modify_username` varchar(50) default null comment '标签修改用户';
alter table `image_label_history` add column `img_modify_username` varchar(50) default null comment '标签修改用户';
alter table `image_label` add column `modify_username` varchar(50) default null comment '标签修改用户';
alter table image_info add column `last_modify_username` varchar(50) default null comment '图片修改用户';
