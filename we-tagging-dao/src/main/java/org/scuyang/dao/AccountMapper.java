package org.scuyang.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.scuyang.common.model.Account;
import org.scuyang.common.model.Role;
import org.scuyang.common.model.UserRole;

import java.util.List;

@Mapper
public interface AccountMapper {

    @Select("select * from `account` where email=#{email}")
    Account getUser(String email);

    @Insert("insert into `account`(password, email, uid) values(#{password}, #{email}, #{uid})")
    int insertUser(Account account);

    @Select("select * from account where uid=#{uid}")
    Account getUserByUid(Long uid);

    @Update("update account set password=#{password} where uid=#{uid}")
    int updatePassword(Long uid, String password);

    @Select("select * from user_role where uid=#{uid}")
    List<UserRole> getRoles(Long uid);

    @Select("select role from user_role where uid=#{uid}")
    List<String> getRoleNames(Long uid);

    @Insert("<script>" +
            "insert into `user_role`(`uid`,`role`) values " +
            "<foreach collection='list' item='user' separator=','> " +
            "( " +
            "#{user.uid}, #{user.role} "+
            ") " +
            "</foreach>" +
            "</script>" )
    int batchInsertRole(List<UserRole> roles);

    @Select("select * from `role`")
    List<Role> getAllRole();

    @Update("update `account` set login_num = login_num + 1, last_login_time = now() where uid=#{uid}")
    int updateLoginLog(Account account);
}
