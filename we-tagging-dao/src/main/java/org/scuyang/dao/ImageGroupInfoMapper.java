package org.scuyang.dao;

import org.apache.ibatis.annotations.*;
import org.scuyang.common.model.ImageGroupInfo;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface ImageGroupInfoMapper {

    @Select("select * from `image_group_info` where version=#{version} and img_group=#{imageGroup}")
    ImageGroupInfo getNote(@Param("version") String version, @Param("imageGroup") String imageGroup);

    @Update("replace into `image_group_info`(`version`, `img_group`, `describe`) VALUES(#{version}, #{imgGroup}, #{describe}) ")
    int replaceNote(ImageGroupInfo imageGroupInfo);

    @Insert("insert into `image_group_info`(`version`, `img_group`, `describe`) VALUES(#{version}, #{imgGroup}, #{describe}) ")
    int insertNote(ImageGroupInfo imageGroupInfo);
}
