package org.scuyang.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.scuyang.common.model.AppConfigModel;

import java.util.List;

@Mapper
public interface AppConfigMapper {
    @Select("select * from app_config where valid = true")
    List<AppConfigModel> selectAll();
}
