package org.scuyang.dao;

import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.*;
import org.scuyang.common.model.ApproveConfig;
import org.scuyang.common.model.ApproveTask;
import org.scuyang.common.model.ApproveTaskLog;
import org.scuyang.common.model.ImageInfo;
import org.scuyang.rest.api.req.label.GetImagePageReq;
import org.scuyang.rest.api.req.approve.ApproveImageReq;
import org.scuyang.rest.api.req.image.GetAllImageVersionReq;
import org.scuyang.rest.api.vo.tag.ImageInfoVo;

import java.util.List;

@Mapper
public interface ApproveMapper {

    @Select("select * from approve_config where delete_id != id")
    List<ApproveConfig> getApproveConfigList();


    @Insert("insert into approve_task (image_version, image_group, image_id,request_uid, approve_num, start_time) " +
            "values (#{imageVersion}, #{imageGroup}, #{imageId},#{requestUid}, #{approveNum}, #{startTime})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insertApproveTask(ApproveTask task);

    @Insert("insert into approve_task_log (task_id, approver_uid, approve_status) " +
            "values (#{taskId}, #{approverUid}, #{approveStatus})")
    int insertTaskLog(ApproveTaskLog taskLog);

    @Insert("<script>" +
            "insert into approve_task_log (task_id, approver_uid, approve_status) values " +
            "<foreach collection='taskLogList' item='item' index='index' separator=','>" +
            "(#{item.taskId}, #{item.approverUid}, #{item.approveStatus})" +
            "</foreach>" +
            "</script>")
    int batchInsertTaskLog(@Param("taskLogList") List<ApproveTaskLog> taskLogList);

    @Select("select b.* from approve_task a join approve_task_log b on a.id=b.task_id \n" +
            "where a.image_id =#{imageId} and a.request_uid =#{userId} and b.approver_uid =#{uid} and a.delete_id != a.id  and b.id != b.delete_id ")
    ApproveTaskLog getTaskLog(ApproveImageReq req);

    @Update("update approve_task_log set approve_status =#{approveStatus},approve_time=#{approveTime}, remark=#{remark},delete_id=#{deleteId} " +
            " where id =#{id}")
    int     updateTaskLog(ApproveTaskLog taskLog);

    @Update("update approve_task set approve_num =greatest(0, approve_num-1) where id =#{id}")
    int addApproveNum(Long taskId);

    @Select("select task.image_version as version, image_group as img_group from approve_task task join approve_task_log as task_log on task.id  = task_log.task_id" +
            " where task_log.approver_uid = #{uid}  group by image_version, image_group ")
    List<ImageInfo> getAllImageVersionInfo(GetAllImageVersionReq req);

    @Select("select  task.image_id as id, task.request_uid " +
            "from approve_task task join approve_task_log as task_log on task.id  = task_log.task_id " +
            "where task_log.approver_uid = #{uid} and task.delete_id = 0 and task_log.delete_id=0")
    Page<ImageInfoVo> getImageInfoPage(GetImagePageReq req);

    @Select("select * from approve_task where id=#{taskId} and delete_id != id")
    ApproveTask getTask(Long taskId);

    @Update("<script>" +
            "update approve_task " +
            "<set>" +
            "id=#{task.id}, "+
            "<if test='task.approveNum!=null'> approve_num=#{task.approveNum}, </if>"+
            "<if test='task.endTime!=null'> end_time=#{task.endTime}, </if>"+
            "<if test='task.deleteId!=null'> delete_id=#{task.deleteId}, </if>"+
            "</set>" +
            "where id=#{task.id} and approve_num=#{oldApproveNum}"+
            "</script>")
    int updateApproveTask(ApproveTask task, @Param("oldApproveNum")Integer oldApproveNum);

    @Update("update approve_task_log set delete_id=id where task_id=#{taskId}")
    int setAllTaskLogPass(@Param("taskId") Long taskId);
}
