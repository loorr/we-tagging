package org.scuyang.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.scuyang.common.model.ModelConfig;

import java.util.List;

@Mapper
public interface ModelConfigMapper {
    @Select("SELECT * FROM model_config WHERE version = #{imageVersion}")
    List<ModelConfig> getModelConfig(String imageVersion);

    @Select("SELECT * FROM model_config WHERE model_code = #{modelCode} and version = #{imageVersion}")
    ModelConfig getModelConfigByModelCodeAndVersion(String imageVersion, String modelCode);

    @Select("SELECT * FROM model_config")
    List<ModelConfig> getAllModelConfig();
}
