package org.scuyang.dao;

import org.apache.ibatis.annotations.*;
import org.scuyang.common.model.RoleImageVersion;

import java.util.List;

@Mapper
public interface RoleMapper {

    @Select("<script>" +
            "select * from `image_version_role` where role in " +
            "<foreach item='item' index='index' collection='roles' open='(' separator=',' close=')'>#{item}</foreach> " +
            "</script>")
    List<RoleImageVersion> getRoleImageVersion(@Param("roles") List<String> roles);

    @Insert("insert into `image_version_role` (`role`, `version`) values (#{role}, #{version})")
    int insertRoleImageVersion(@Param("role") String role, @Param("version") String version);

    @Delete("delete from `image_version_role` where `role`=#{role} and `version`=#{version}")
    int deleteRoleImageVersion(@Param("role") String role, @Param("version") String version);

}
