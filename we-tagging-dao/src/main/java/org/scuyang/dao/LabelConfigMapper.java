package org.scuyang.dao;

import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.*;
import org.scuyang.common.model.LabelConfig;
import org.scuyang.oss.api.req.AddOrUpdateLabelAdminReq;
import org.scuyang.oss.api.req.DeleteLabelAdminReq;
import org.scuyang.oss.api.req.GetLabelInfoPageAdminReq;
import org.scuyang.oss.api.vo.LabelConfigOssVo;
import org.scuyang.rest.api.req.label.LabelSortItem;
import org.scuyang.rest.api.vo.LabelVersionVo;
import org.springframework.stereotype.Repository;

import java.awt.*;
import java.util.List;

@Mapper
@Repository
public interface LabelConfigMapper {

    @Select("select * from `label_config` where version=#{version} order by db_create_time desc")
    List<LabelConfig>  getLabelConfigListByVersion(@Param("version") String version);

    @Select("SELECT DISTINCT(version) as version, COUNT(id) as total_num FROM `label_config` GROUP BY version")
    List<LabelVersionVo> getAllVersion();


    @Select("<script>" +
            "select * from `label_config` " +
            "<where>" +
                "<if test='null != version and \"\" != version'>and version=#{version} </if>" +
                "<if test='null != categoryId'>and category_id=#{categoryId} </if>" +
                "<if test='null != nameId'>and class_id=#{classId} </if>" +
                "<if test='null != name and \"\" != name'>and class_name=#{name} </if>" +
                "<if test='null != category and \"\" != category'>and category_name=#{category} </if>" +
            "</where>" +
            "order by db_modify_time DESC" +
            "</script>")
    Page<LabelConfigOssVo> getLabelConfigPage(GetLabelInfoPageAdminReq req);

    @Insert("insert into `label_config` " +
            " (version,category_id,category_name,class_id,class_name,shortcut_key,abnormal_state,color) " +
            " values(#{version},#{categoryId},#{categoryName},#{classId},#{className},#{shortcutKey},#{abnormalState}, #{color})")
    int addLabelInfo(AddOrUpdateLabelAdminReq req);

    @Insert("insert into `label_config` " +
            " (version,category_id,category_name,class_id,class_name,shortcut_key,color, remark, priority) " +
            " values(#{version},#{categoryId},#{categoryName},#{classId},#{className},#{shortcutKey},#{color}, #{remark}, #{priority})")
    int addLabelInfoByModel(LabelConfig data);



    @Update("<script>" +
            "UPDATE label_config " +
            "<set> " +
            "<if test=\"null != version and '' != version\">version = #{version},</if>\n" +
            "<if test=\"null != categoryId\">category_id = #{categoryId},</if>\n" +
            "<if test=\"null != categoryName and '' != categoryName\">category_name = #{categoryName},</if>\n" +
            "<if test=\"null != classId\">class_id = #{classId},</if>\n" +
            "<if test=\"null != className and '' != className\">class_name = #{className},</if>\n" +
            "<if test=\"null != shortcutKey and '' != shortcutKey\">shortcut_key = #{shortcutKey},</if>\n" +
            "<if test=\"null != abnormalState and '' != abnormalState\">abnormal_state = #{abnormalState},</if>\n" +
            "<if test=\"null != color and '' != color\">color = #{color}</if>\n" +
            "</set>\n" +
            "WHERE id = #{id}" +
            "</script>")
    int updateLabelInfo(AddOrUpdateLabelAdminReq req);

    @Delete("delete from `label_config` where id=#{id}")
    int deleteLabelInfo(DeleteLabelAdminReq req);

    @Select("select max(category_id) from `label_config`")
    Integer getMaxCategoryId(String version);

    @Select("select max(class_id) from `label_config` where version=#{version}")
    Integer getMaxClassId(String version);

    @Select("select category_id from `label_config` where version=#{version} and  category_name = #{categoryName} limit 1")
    Integer existCategoryName(String categoryName, String version);

    @Select("select class_name from `label_config` " +
            "where version=#{version} and class_name=#{className} and category_name=#{categoryName}" +
            "limit 1")
    String existClassName(String className, String version, String categoryName);

    @Delete("delete from `label_config` where " +
            "version=#{version} and category_id=#{categoryId} and class_id=#{classId}")
    int deleteClass(String version, Integer categoryId, Integer classId);

    @Delete("delete from `label_config` where " +
        "version=#{version} and category_id=#{categoryId}")
    int deleteCategory(String version, Integer categoryId);

    @Update("<script>" +
            "UPDATE label_config " +
            "<set> " +
            "<if test=\"null != categoryName and '' != categoryName\">category_name = #{categoryName}, </if>\n" +
            "<if test=\"null != shortcutKey and '' != shortcutKey\">shortcut_key = #{shortcutKey}, </if>\n" +
            "<if test=\"null != abnormalState and '' != abnormalState\">abnormal_state = #{abnormalState}, </if>\n" +
            "<if test=\"null != color and '' != color\">color = #{color}, </if>\n" +
            "<if test=\"null != remark and '' != remark\">remark = #{remark}, </if>\n" +
            "<if test=\"null != priority\">priority = #{priority} </if>\n" +
            "</set>\n" +
            "WHERE category_id = #{categoryId} and version=#{version} and class_name='-'" +
            "</script>")
    int updateCategory(LabelConfig data);

    @Update("UPDATE label_config set category_name=#{name} where version=#{version} and category_id=#{categoryId} ")
    int updateCategoryName(String version, Integer categoryId, String name);

    @Update("<script>" +
            "UPDATE label_config " +
            "<set> " +
            "<if test=\"null != categoryId and '' != categoryId\">category_id = #{categoryId},</if>\n" +
            "<if test=\"null != categoryName and '' != categoryName\">category_name = #{categoryName},</if>\n" +
            "<if test=\"null != className and '' != className\">class_name = #{className},</if>\n" +
            "<if test=\"null != shortcutKey and '' != shortcutKey\">shortcut_key = #{shortcutKey},</if>\n" +
            "<if test=\"null != abnormalState and '' != abnormalState\">abnormal_state = #{abnormalState},</if>\n" +
            "<if test=\"null != color and '' != color\">color = #{color}, </if>\n" +
            "<if test=\"null != remark and '' != remark\">remark = #{remark}, </if>\n" +
            "<if test=\"null != priority\">priority = #{priority} </if>\n" +
            "</set>\n" +
            "WHERE id=#{id}" +
            "</script>")
    int updateClass(LabelConfig data);


    @Select("select * from label_config where " +
            " version=#{version} and category_id=#{categoryId} and class_name='-'")
    LabelConfig getCategory(String version, Integer categoryId);

    @Select("select * from label_config where id = #{id}")
    LabelConfig getLabelConfigById(Long id);


    @Update("<script> " +
            "update label_config " +
            "<trim prefix='set' suffixOverrides=','> " +
            "<trim prefix='priority=case ' suffix='end '> " +
            "<foreach collection='items' item='item'> " +
            "when id = #{item.id} then #{item.priority} " +
            "</foreach> " +
            "</trim> " +
            "</trim> " +
            "where version= #{version} and " +
            "id in " +
            "<foreach collection='items' item='item' index='index' open='(' separator=',' close=')'> " +
            "#{item.id} " +
            "</foreach> " +
            "</script>")
    int labelResort(@Param("version") String version, @Param("items") List<LabelSortItem> labelSortItems);

    @Update("<script> " +
            "update user_label_config " +
            "<trim prefix='set' suffixOverrides=','> " +
            "<trim prefix='priority=case ' suffix='end '> " +
            "<foreach collection='items' item='item'> " +
            "when id = #{item.id} then #{item.priority} " +
            "</foreach> " +
            "</trim> " +
            "</trim> " +
            "where version= #{version} and uid=#{uid} and " +
            "id in " +
            "<foreach collection='items' item='item' index='index' open='(' separator=',' close=')'> " +
            "#{item.id} " +
            "</foreach> " +
            "</script>")
    int labelUserResort(@Param("uid")Long uid, @Param("version")String version, @Param("items")  List<LabelSortItem> labelSortItems);
}