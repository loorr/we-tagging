package org.scuyang.dao;

import org.apache.ibatis.annotations.*;
import org.scuyang.common.model.UserLabelConfigModel;
import org.scuyang.rest.api.req.label.ResetUserLabelConfigReq;
import org.scuyang.rest.api.req.label.UserLabelConfigGetReq;
import org.scuyang.rest.api.req.label.UserLabelConfigUpdateReq;

import java.util.List;

@Mapper
public interface UserLabelConfigMapper {
    @Select("select * from user_label_config where uid = #{uid} and version = #{version}")
    List<UserLabelConfigModel> getUserLabelConfig(UserLabelConfigGetReq req);

    @Insert("<script>" +
            "insert into user_label_config (uid, `version`, category_id, class_id,`shortcut_key`, `color`, `remark`, `category_name`, `class_name`) values " +
            "<foreach collection='list' item='item' separator=','>" +
            "(#{item.uid}, #{item.version}, #{item.categoryId}, #{item.classId}, #{item.shortcutKey}, #{item.color}, #{item.remark}, #{item.categoryName}, #{item.className})" +
            "</foreach>" +
            "</script>")
    void batchAddUserLabelConfig(List<UserLabelConfigModel> userLabels);

    @Update("update user_label_config set `shortcut_key` = #{shortcutKey}, " +
            "`color` = #{color}, `remark` = #{remark} where uid = #{uid} " +
            "and version = #{version} and category_id = #{categoryId} and class_id = #{classId} " +
            "and priority=#{priority}")
    int updateUserLabelConfig(UserLabelConfigUpdateReq req);

    @Select("select uid from user_label_config where `version` = #{version} group by uid")
    List<Long> getUidsByVersion(String version);


    @Delete("delete from user_label_config where uid = #{uid} and version = #{version}")
    int deleteUserLabelConfig(ResetUserLabelConfigReq req);
}
