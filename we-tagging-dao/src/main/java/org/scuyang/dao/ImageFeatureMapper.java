package org.scuyang.dao;

import org.apache.ibatis.annotations.*;
import org.scuyang.common.model.FeatureConfig;
import org.scuyang.common.model.FeatureData;
import org.scuyang.common.model.GroupFeatureData;
import org.scuyang.rest.api.req.feature.UpdateImageFeatureConfigReq;
import org.scuyang.rest.api.vo.feature.FeatureDataItem;

import java.util.List;

@Mapper
public interface ImageFeatureMapper {
    @Insert("insert into image_feature_config(`image_version`, `feature_name`, `type`, `remark`, `data_format`, `default_value`, `feature_type`) " +
            "values(#{imageVersion}, #{featureName}, #{type}, #{remark}, #{dataFormat}, #{defaultValue}, #{featureType})")
    int insertFeatureConfig(FeatureConfig data);

    @Update("update image_feature_config set `valid` = 0 where `id` = #{id}")
    int deleteFeatureConfig(Long id);

    @Select("select * from image_feature_config where image_version=#{imageVersion} and feature_type=#{featureType} and `valid`=1 order by db_create_time desc")
    List<FeatureConfig> getFeatureConfig(String imageVersion, String featureType);

    @Select("select * from image_feature_data where image_id=#{imageId}")
    List<FeatureData> getFeatureData(Long imageId);

    @Delete("delete from image_feature_data where image_id=#{imageId}")
    int deleteFeatureData(Long imageId);

    @Insert("<script>" +
            "insert into image_feature_data(`feature_id`,`image_id`,`data`) values " +
            "<foreach collection='list' item='item' index='index' separator=','>" +
            "(#{item.featureId}, #{item.imageId}, #{item.data})" +
            "</foreach>" +
            "</script>")
    int batchInsertFeatureData(@Param("list") List<FeatureData> featureData);

    @Deprecated
    @Select("select image_version from image_feature_config where id=#{id}")
    String getImagerVersion(Long id);

    @Select("select * from image_feature_config where id=#{id}")
    FeatureConfig getFeatureConfigById(Long id);

    @Update("<script>" +
            "update image_feature_config " +
            "<set>" +
            "<if test='featureName!=null and featureName!=\"\"'>feature_name = #{featureName}, </if>"+
            "<if test='type!=null and type!=\"\"'>`type` = #{type}, </if>"+
            "<if test='remark!=null and remark!=\"\"'>`remark` = #{remark}, </if>"+
            "<if test='dataFormat!=null and dataFormat!=\"\"'>`data_format` = #{dataFormat}, </if>"+
            "<if test='defaultValue!=null and defaultValue!=\"\"'>`default_value` = #{defaultValue}, </if>"+
            "</set> where id=#{id}" +
            "</script>")
    int updateImageFeatureConfig(UpdateImageFeatureConfigReq req);

    @Select("select * from image_group_feature_data where image_version=#{imageVersion} and `image_group`=#{imageGroup}")
    List<FeatureData> getGroupFeatureData(String imageVersion, String imageGroup);

    @Delete("delete from image_group_feature_data where image_version=#{imageVersion} and `image_group`=#{imageGroup}")
    void deleteGroupFeatureData(String imageVersion, String imageGroup);

    @Insert("<script>" +
            "insert into image_group_feature_data(`feature_id`,`image_group`,`image_version`,`data`) values " +
            "<foreach collection='list' item='item' index='index' separator=','>" +
            "(#{item.featureId}, #{item.imageGroup},#{item.imageVersion}, #{item.data})" +
            "</foreach>" +
            "</script>")
    void batchInsertGroupFeatureData(List<GroupFeatureData> featureDataList);

    @Select("<script>" +
            "select * from image_group_feature_data where image_version=#{imageVersion} and " +
            "<foreach collection='featureFilter' item='item' open='and (' separator=' or ' close=')' >" +
            " (fea.feature_id = #{item.id} and fea.data like concat('%',#{item.value},'%') ) " +
            "</foreach> " +
            "</script>")
    List<String> filterImageGroup(@Param("version") String version, @Param("featureFilter")  List<FeatureDataItem> featureDataList);
}
