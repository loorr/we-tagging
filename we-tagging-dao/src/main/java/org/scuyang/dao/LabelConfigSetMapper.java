package org.scuyang.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.scuyang.common.model.LabelSet;
import org.scuyang.rest.api.req.feature.DeleteImageFeatureConfigReq;
import org.scuyang.rest.api.req.label.AddLabelVersionSetReq;
import org.scuyang.rest.api.req.label.GetAllLabelVersionReq;
import org.scuyang.rest.api.req.label.UpdateLabelVersionSetReq;

import java.util.List;
import java.util.Set;

@Mapper
public interface LabelConfigSetMapper {
    @Insert("insert into label_set (image_version, `name`, `remark`) values (#{imageVersion}, #{version}, #{remark})")
    int addLabelVersionSet(AddLabelVersionSetReq req);

    @Update("update label_set set `valid`=0 where id = #{id}")
    int deleteLabelVersionSet(DeleteImageFeatureConfigReq req);

    @Update("update label_set set `remark`=#{remark} where id = #{id}")
    int updateLabelVersionSet(UpdateLabelVersionSetReq req);

    @Select("select * from label_set where image_version = #{imageVersion} and valid =1")
    List<LabelSet> getAllLabelVersion(GetAllLabelVersionReq req);

    @Select("select `name` from label_set where image_version = #{imageVersion} and valid =1")
    Set<String> getLabelVersionSetByImageVersion(String imageVersion);

}

