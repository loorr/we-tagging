package org.scuyang.dao;

import org.apache.ibatis.annotations.*;
import org.scuyang.common.model.AsynTask;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AsynTaskMapper {
    @Options(useGeneratedKeys=true, keyProperty="id")
    @Insert("insert into `asyn_task`(`task_name`,`task_code`,`task_state`,`task_start_time`,`task_end_time`) " +
            "values(#{taskName}, #{taskCode}, #{taskState}, #{taskStartTime}, #{taskEndTime})")
    int addTask(AsynTask asynTask);

    @Update("<script>" +
            "update `asyn_task`" +
            "<trim prefix='set' suffixOverrides=','>" +
                "<if test='taskName!=null and taskName!=\"\"'> task_name=#{taskName}, </if>"+
                "<if test='taskCode!=null'> task_code=#{taskCode}, </if>"+
                "<if test='taskState!=null'> task_state=#{taskState}, </if>"+
                "<if test='taskStartTime!=null'> task_start_time=#{taskStartTime}, </if>"+
                "<if test='taskEndTime!=null'> task_end_time=#{taskEndTime}, </if>"+
            "</trim>" +
            "where id = #{id}" +
            "</script>")
    int updateTask(AsynTask asynTask);

    @Update("update `asyn_task` set task_state = #{taskState} where id=#{id}")
    int updateTaskState(@Param("id")Integer id, @Param("taskState") Integer taskState);

    @Select("select * from `asyn_task` where id=#{id}")
    AsynTask getTask(@Param("id")Integer id);

    @Select("<script>" +
            "select * from `asyn_task` where task_code=#{taskCode} " +
            "<if test='taskState!=null'> and task_state = #{taskState} </if>" +
            "</script>")
    List<AsynTask> getTaskList(@Param("taskCode")Integer taskCode, @Param("taskState") Integer taskState);
}
