package org.scuyang.dao;

import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.*;
import org.scuyang.common.model.ImageInfo;
import org.scuyang.oss.api.req.GetImagePageAdminReq;
import org.scuyang.oss.api.vo.ImageVo;
import org.scuyang.rest.api.req.image.GetImageGroupsReq;
import org.scuyang.rest.api.req.label.GetImagePageReq;
import org.scuyang.rest.api.vo.tag.ImageInfoVo;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Mapper
@Repository
public interface ImageInfoMapper {
    @Select(value = "select * from image_info where img_group=#{group} AND version=#{version}")
    List<ImageInfo> selectListByVersionAndGroup(@Param("group")String group, @Param("version") String version);

    @Options(useGeneratedKeys=true, keyProperty="id")
    @Insert("insert into image_info" +
            " (md5,version,uri,folder,img_group,img_src_name,img_size,img_length,img_width,label_state)" +
            " values(#{md5},#{version},#{uri},#{folder},#{imgGroup},#{imgSrcName},#{imgSize},#{imgLength},#{imgWidth},#{labelState})")
    int insert(ImageInfo imageInfo);


//    @Delete("")
//    int deleteByVersionAndGroup(@Param("group")String group, @Param("version") String version);

    @Select("<script>" +
            "SELECT img.id, uri as img_url, img_src_name, label_num, label_state, img_length, img_width FROM `image_info` as img left join image_feature_data as fea on img.id = fea.image_id " +
            "WHERE version=#{version} " +
            "<if test='imgGroup!=null and imgGroup!=\"\"'> and img_group = #{imgGroup} </if> " +
            "<if test='labelState!=null'> and label_state = #{labelState} </if>"+
            "<if test='featureFilter!=null and featureFilter.size()>0'>" +
            "<foreach collection='featureFilter' item='item' open='and (' separator=' or ' close=')' >" +
            " (fea.feature_id = #{item.id} and fea.data = #{item.value}) " +
            "</foreach> " +
            "</if>" +
            "<if test='havaLabelFilter!=null and havaLabelFilter==true'> and img.label_num &gt; 0 </if>" +
            "<if test='lastModifyUsernameFilter!=null and lastModifyUsernameFilter!=\"\"'> and img.last_modify_username = #{lastModifyUsernameFilter} </if>" +
            "<if test='minModifyTimeFilter!=null'> and img.db_modify_time &gt;= #{minModifyTimeFilter} </if>" +
            "<if test='maxModifyTimeFilter!=null'> and img.db_modify_time &lt;= #{maxModifyTimeFilter} </if>" +
            "<if test='imgSrcNameFilter!=null and imgSrcNameFilter!=\"\"'> and img.img_src_name like  concat('%',#{imgSrcNameFilter},'%')  </if>" +
            "group by img.id"+
            "</script>")
    Page<ImageInfoVo> selectImageInfoVo(GetImagePageReq req);

    @Select("<script>" +
            "select img_group  from as img left join image_feature_data as fea on img.id = fea.image_id " +
            "WHERE version=#{imageVersion} " +
            "<if test='groupNameFilter!=null and groupNameFilter!=\"\"'> and img_group like concat('%', #{groupNameFilter},'%')  </if> " +
            "<if test='imageGroups!=null and imageGroups.size()>0'>" +
            "<foreach collection='imageGroups' item='item' open='and img_group in (' separator=',' close=')'> #{item} </foreach> " +
            "</if>" +
            "<if test='labelStateFilter!=null'> and label_state = #{labelStateFilter} </if>"+
            "<if test='havaLabelFilter!=null and havaLabelFilter==true'> and img.label_num &gt; 0 </if>" +
            "<if test='lastModifyUsernameFilter!=null and lastModifyUsernameFilter!=\"\"'> and img.last_modify_username = #{lastModifyUsernameFilter} </if>" +
            "<if test='minModifyTimeFilter!=null'> and img.db_modify_time &gt;= #{minModifyTimeFilter} </if>" +
            "<if test='maxModifyTimeFilter!=null'> and img.db_modify_time &lt;= #{maxModifyTimeFilter} </if>" +
            "<if test='imgSrcNameFilter!=null and imgSrcNameFilter!=\"\"'> and img.img_src_name like  concat('%',#{imgSrcNameFilter},'%')  </if>" +
            "<if test='imgFeatureFilter!=null and imgFeatureFilter.size()>0'>" +
            "<foreach collection='imgFeatureFilter' item='item' open='and (' separator=' or ' close=')' >" +
            " (fea.feature_id = #{item.id} and fea.data = #{item.value}) " +
            "</foreach> " +
            "</if>" +
            "group by img_group "+
            "</script>")
    List<String> getImageGroupList(GetImageGroupsReq req);

    @Select("select * from `image_info` WHERE id=#{id}")
    ImageInfo getImageInfo(@Param("id") Long id);

    @Select("select * from `image_info` WHERE md5=#{md5}")
    ImageInfo getImageInfoByMd5(@Param("md5") String md5);

    @Select("SELECT id,version, img_group, label_num,label_state FROM `image_info`")
    List<ImageInfo> getAllImageInfo();

    @Select("<script>" +
            "select id,version, img_group, label_num,label_state from `image_info` where version in "+
            "<foreach item='item' index='index' collection='versions' open='(' separator=',' close=')'>#{item}</foreach> " +
            "</script>")
    List<ImageInfo> getImageInfoByVersion(@Param("versions") Set<String> imageVersion);

    @Select("select count(id) from image_info where version=#{version} and img_group=#{imageGroup}")
    int isExistImageGroup(@Param("version") String version, @Param("imageGroup") String imageGroup);

    @Update("UPDATE image_info SET `label_num`=#{num}, `last_modify_username`=#{username} WHERE `id` = #{id}")
    void updateImageLabelNumAndModifyUid(@Param("username")String username, @Param("num")Integer num, @Param("id") Long imgId);

    @Update("update image_info set `image_ext`=#{imageExt} where `id` = #{id}")
    void updateImageExt(@Param("imageExt")String imageExt, @Param("id") Long imgId);

    @Update("UPDATE image_info SET `label_num`=`label_num`+1 WHERE `id` = #{id}")
    void addOneImageLabelNum(@Param("id") Long imgId);

    @Update("UPDATE image_info SET `label_num`=IF(`label_num`<1, 0, `label_num`-1) WHERE `id` = #{id}")
    void subOneImageLabelNum(@Param("id") Long imgId);

    @Select("select * from image_info where id=#{id}")
    ImageInfo getFeature(@Param("id") Long imgId);

    @Update("update image_info set `img_feature` =#{feature} where `id` = #{id}")
    int updateFeature(@Param("id") Long imgId, @Param("feature") String feature);


    @Select("select * from image_info order by db_modify_time DESC")
    Page<ImageVo> getImagePage(GetImagePageAdminReq req);

    @Update("UPDATE image_info SET label_state=#{imgState} where `id` = #{id}")
    int changeImageLabelState(@Param("id") Long imgId, Integer imgState);

    @Select("select label_state from image_info where id=#{id}")
    boolean getImageLabelState(@Param("id") Long imgId);

    @Select("select version from image_info where version=#{imageVersion} limit 1")
    String isExistImageVersion(@Param("imageVersion") String imageVersion);

    /** just for test */
    @Select("select * from image_info where version=#{imageVersion} and img_src_name=#{imageSrcName} limit 1")
    ImageInfo getImageInfoByVersionAndSrcName(@Param("imageVersion") String imageVersion, @Param("imageSrcName") String imageSrcName);

    @Select("<script>" +
            "select * from image_info where id in " +
            "<foreach item='item' index='index' collection='ids' open='(' separator=',' close=')'>#{item}</foreach> " +
            "</script>")
    List<ImageInfo> getImageByIdList(@Param("ids") List<Long> imageIdList);

    @Select("select id from image_info where version=#{imageVersion} and label_num <= 0 order by id desc")
    List<Long> getAllNoTagImageIdList(String imageVersion);
}
