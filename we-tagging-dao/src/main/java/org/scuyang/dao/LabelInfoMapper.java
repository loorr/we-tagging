package org.scuyang.dao;


import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.*;
import org.scuyang.common.model.ImageLabel;
import org.scuyang.common.model.ImageLabelAlgorithm;
import org.scuyang.common.model.LabelConfig;
import org.scuyang.oss.api.req.GetImageLabelPageAdminReq;
import org.scuyang.oss.api.vo.ImageLabelVo;
import org.scuyang.rest.api.req.GetImageLabelInfoReq;
import org.scuyang.rest.api.req.label.CheckLabelUsedReq;
import org.scuyang.rest.api.vo.LabelUsedVo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface LabelInfoMapper {

    @Insert("<script>" +
            "INSERT INTO `image_label`(img_id, category_id,class_id,coordinate_x,coordinate_y,width,height, label_ext, modify_username) VALUES " +
            "<foreach collection='allTagInfo' item='item' separator=','> " +
                "(#{item.imgId},#{item.categoryId},#{item.classId},#{item.coordinateX},#{item.coordinateY},#{item.width}," +
                    "#{item.height}, #{item.labelExt}, #{item.modifyUsername})" +
            "</foreach>" +
            "</script>")
    int batchInsert(List<ImageLabel> allTagInfo);

    @Deprecated
    @Insert("<script>" +
            "INSERT INTO `image_label_algorithm`(img_id, category_id,class_id,coordinate_x,coordinate_y,width,height) VALUES " +
            "<foreach collection='allTagInfo' item='item' separator=','> " +
            "(#{item.imgId},#{item.categoryId},#{item.classId},#{item.coordinateX},#{item.coordinateY},#{item.width}," +
            "#{item.height})" +
            "</foreach>" +
            "</script>")
    int batchInsertAlgorithm(List<ImageLabel> allTagInfo);

    @Insert("<script>" +
            "INSERT INTO `image_label_algorithm`(img_id, category_id,class_id,coordinate_x,coordinate_y,width,height,model_version, model_code, score) VALUES " +
            "<foreach collection='allTagInfo' item='item' separator=','> " +
            "(#{item.imgId},#{item.categoryId},#{item.classId},#{item.coordinateX},#{item.coordinateY},#{item.width}," +
            "#{item.height}, #{item.modelVersion}, #{item.modelCode}, #{item.score})" +
            "</foreach>" +
            "</script>")
    int batchInsertAlgoLabels(List<ImageLabelAlgorithm> allTagInfo);

    @Insert("<script>" +
            "INSERT INTO `image_label_algorithm`(img_id, category_id,class_id,coordinate_x,coordinate_y,width,height,model_code, model_version, score) VALUES " +
            "<foreach collection='allTagInfo' item='item' separator=','> " +
            "(#{item.imgId},#{item.categoryId},#{item.classId},#{item.coordinateX},#{item.coordinateY},#{item.width}," +
            "#{item.height}, #{item.modelCode}, #{item.modelVersion}, #{item.score})" +
            "</foreach>" +
            "</script>")
    int batchInsertNewAlgorithmLabels(List<ImageLabelAlgorithm> allTagInfo);

    @Delete("delete from `image_label_algorithm` where img_id=#{imgId} and model_code=#{modelCode} and model_version=#{modelVersion}")
    int deleteNewAlgorithmLabel(Long imgId, String modelCode, String modelVersion);
    @Delete("delete from `image_label_algorithm` where img_id=#{imgId}")
    int deleteAlgorithmLabel(Long imgId);

    @Delete("<script>" +
            "delete from `image_label_algorithm` where img_id in " +
            "<foreach collection='imgIdList' item='item' separator=','>#{item}</foreach>" +
            "</script>")
    int deleteAlgorithmLabelByGroup(List<Long> imgIdList);

    @Insert("<script>" +
            "INSERT INTO `image_label_history`(img_id, category_id,class_id,coordinate_x,coordinate_y,width,height, `time`, " +
            "`label_ext`, modify_username) VALUES " +
            "<foreach collection='allTagInfo' item='item' separator=','> " +
            "(#{item.imgId},#{item.categoryId},#{item.classId},#{item.coordinateX},#{item.coordinateY},#{item.width}," +
            "#{item.height}, #{item.time}, #{item.labelExt}, #{item.modifyUsername})" +
            "</foreach>" +
            "</script>")
    int batchInsertHistory(@Param("allTagInfo") List<ImageLabel> allTagInfo, @Param("imageModifyUser") String imageModifyUser);

    @Select("select  * from image_label_history  " +
            "where img_id = #{imgId} and `time` = " +
            "(select max(`time`) from image_label_history where img_id = #{imgId})")
    List<ImageLabel> getLastHistory(Long imgId);


    @Select("select  * from image_label_history where img_id = #{imgId} order by `time` desc")
    List<ImageLabel> getHistoryByImgId(Long imgId);

    @Delete("delete from image_label_history " +
            "where img_id = #{imgId} " +
            "and `time` not in (select  * from ( " +
            "select distinct(`time`) from image_label_history " +
            "where img_id = #{imgId} " +
            "order by `time` desc limit #{maxNum} ) as a)")
    int deleteHistory(Long imgId, Integer maxNum);

    @Delete("DELETE FROM `image_label` WHERE img_id=#{imgId}")
    int deleteLabels(@Param("imgId") Long imgId);

    @Insert("insert into image_label" +
            " (img_id, category_id,class_id,coordinate_x,coordinate_y,width,height)" +
            " values(#{imgId},#{categoryId},#{classId},#{coordinateX},#{coordinateY},#{width},#{height})")
    Boolean insert(ImageLabel imageLabel);

    @Delete("DELETE FROM image_label " +
            "WHERE img_id=#{imgId} AND category_id = #{categoryId} AND class_id = #{classId} AND " +
            "coordinate_x = #{coordinateX} AND coordinate_y = #{coordinateY} AND width = #{width} AND height = #{height}")
    int delete(ImageLabel imageLabel);

    @Select("select * from image_label where img_id = #{imgId}")
    List<ImageLabel> getImageLabelList(GetImageLabelInfoReq req);

    @Select("select * from image_label where img_id = #{imgId}")
    List<ImageLabel> getImageLabelListByImgId(Long imgId);

    @Deprecated
    @Select("select * from image_label where version=#{version} ")
    List<ImageLabel> getImageLabelListByVersion(@Param("version") String version);

    @Select("select\n" +
            "    il.id as id, il.img_id as img_id, il.category_id as category_id, \n" +
            "    lc.category_name as category_name, il.class_id as class_id, lc.class_name as class_name, \n" +
            "    il.coordinate_x as coordinate_x, il.coordinate_y as coordinate_y, il.height as height, \n" +
            "    il.width as width, ii.md5 as `md5` \n" +
            "from\n" +
            "    image_label il\n" +
            "left join label_config lc on\n" +
            "    il.category_id = lc.category_id\n" +
            "    and il.class_id = lc.class_id\n" +
            "left join image_info ii on il.img_id = ii.id "
//            + "where il.category_id != ''"
    )
    Page<ImageLabelVo> getImageLabelPage(GetImageLabelPageAdminReq req);

    @Select("select * from image_label_algorithm where img_id = #{imgId} and model_code = #{modelCode} and model_version = #{modelVersion}")
    List<ImageLabel> getAlgorithmLabels(Long imgId, String modelCode, String modelVersion);

    @Select("select count(id) from image_label_algorithm where img_id = #{imgId} and model_code = #{modelCode} and model_version = #{modelVersion}")
    int getAlgorithmLabelsCount(Long imgId,String modelCode, String modelVersion);

    // find label used in image
    @Select("select count(distinct img_id) as used_images, count(*) as used_labels from image_label  " +
            "where (class_id = #{labelConfig.categoryId} and  class_id = #{labelConfig.classId}) or (class_id = #{labelConfig.categoryId} and class_id is null)")
    LabelUsedVo countCategoryUsed(CheckLabelUsedReq req);

    @Select("select count(distinct img_id) as used_images, count(*) as used_labels from image_label  " +
            "where class_id = #{labelConfig.classId} and category_id = #{labelConfig.categoryId}")
    LabelUsedVo countClassUsed(CheckLabelUsedReq req);

    @Select("select distinct img_id  from image_label as label join image_info as info on label.img_id = info.id " +
            " where info.version=#{version} and  category_id = #{categoryId} and class_id = #{classId}")
    List<Long> getImageIdsByLabelId(String version, Integer categoryId, Integer classId);

    @Delete("delete from image_label where img_id = #{imgId} and category_id = #{oldConfig.categoryId} and class_id = #{oldConfig.classId}")
    int deleteLabelByImgIdAndConfig(@Param("imgId") Long imgId, @Param("old") LabelConfig oldConfig);

    @Update("update image_label set category_id = #{newConfig.categoryId}, class_id = #{newConfig.classId} where img_id = #{imgId} " +
            "and category_id = #{oldConfig.categoryId} and class_id = #{oldConfig.classId}")
    int replaceLabelByImgIdAndConfig(@Param("imgId") Long imgId, @Param("old")  LabelConfig oldConfig, @Param("new")  LabelConfig newConfig);

    @Select("select count(id) from image_label where img_id = #{imgId}")
    int countImageLabelsByImgId(Long imgId);
}
