# 流程定义



# 流程实例



# 流程任务



# 待审批任务
create table user_approve_list (
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `uid` bigint(20) unsigned NOT NULL COMMENT '用户id',
    `approve_id` bigint(20) unsigned NOT NULL COMMENT '审批id',
    primary key (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户待审批任务';


# 用户通知
create table user_notification (
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `uid` bigint(20) unsigned NOT NULL COMMENT '用户id',
    `type` tinyint(3) unsigned NOT NULL COMMENT '通知类型',
    `title` varchar(255) default NULL COMMENT '通知标题',
    `content` varchar(255) default NULL COMMENT '通知内容',
    `url` varchar(255) default NULL COMMENT '通知链接',
    `is_read` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否已读',
    `notify_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '通知时间',
    `read_time` timestamp default NULL COMMENT '阅读时间',
    index `idx_uid` (`uid`),
    primary key (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户通知';


# 角色组
create table user_group(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `name` varchar(255) NOT NULL COMMENT '角色组名称',
    `description` varchar(255) default NULL COMMENT '角色组描述',
    primary key (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户组';

create table relation_user_group_account(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `uid` bigint(20) unsigned NOT NULL COMMENT '用户id',
    `role_group_id` bigint(20) unsigned NOT NULL COMMENT '角色组id',
    index `idx_uid` (`uid`),
    primary key (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角组';

create table relation_user_group_role(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `role_group_id` bigint(20) unsigned NOT NULL COMMENT '角色组id',
    `role_id` bigint(20) unsigned NOT NULL COMMENT '角色id',
    index `idx_role_group_id` (`role_group_id`),
    primary key (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角组';