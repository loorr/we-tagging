create table allocate_image(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `version` varchar(255) NOT NULL COMMENT '图片数据集',
    `image_group` varchar(255) NOT NULL COMMENT '图片组',
    `uid` bigint(20) unsigned NOT NULL COMMENT '用户id',
    index `idx_uid` (`uid`),
    primary key (`id`)
) ENGINE=InnoDB COMMENT='打标签分配';