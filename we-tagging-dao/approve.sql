create table approve_config(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `task_type` varchar(55) NOT NULL COMMENT '类型(图片集, 图片组, 图片)',
    `task_value` varchar(55) NOT NULL COMMENT '审批值',
    `approver_type` varchar(55) NOT NULL COMMENT '审批类型(部门, 角色, 用户)',
    `approver_value` varchar(55) NOT NULL COMMENT '审批者值',
    `require_num` int(11) default 0 COMMENT '需要审批的数量',
    `task_name` varchar(55) NOT NULL COMMENT '审批任务名称',
    `task_desc` varchar(55) NOT NULL COMMENT '审批任务描述',
    `delete_id` bigint(20) unsigned default 0 COMMENT '删除标识',
    unique index (`task_type`, `task_value`, `approver_type`, `approver_value`, `delete_id`),
    primary key (`id`)
)ENGINE=InnoDB  COMMENT='审批配置表';

create table approve_task(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `image_version` varchar(20) NOT NULL COMMENT '图片版本',
    `image_id` bigint(20) unsigned NOT NULL COMMENT '图片id',
    `image_group` varchar(50) NOT NULL COMMENT '图片人物',
    `request_uid` bigint(20) unsigned NOT NULL COMMENT '发起人',
    `approve_num` int(11) default 0 COMMENT '还未审批的数量',
    `start_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '审批开始时间',
    `end_time` datetime(3)  DEFAULT NULL COMMENT '审批结束时间',
    `delete_id` bigint(20) unsigned default 0 COMMENT '删除标识',
    primary key (`id`)
)ENGINE=InnoDB  COMMENT='审批配置表';


create table approve_task_log(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `db_create_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '数据库插入时间',
    `db_modify_time` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '数据库更新时间',
    `task_id` bigint(20) unsigned NOT NULL COMMENT '审批任务id',
    `approver_uid` bigint(20) unsigned NOT NULL COMMENT '审批者id',
    `approve_status` tinyint(4) NOT NULL default 0 COMMENT '审批状态(0:未审批, 2:审批通过, 3:审批不通过)',
    `approve_time` datetime(3)  NULL COMMENT '审批时间',
    `remark` varchar(255) default NULL COMMENT '审批备注',
    `delete_id` bigint(20) unsigned default 0 COMMENT '删除标识',
    primary key (`id`)
)ENGINE=InnoDB  COMMENT='审批日志';
