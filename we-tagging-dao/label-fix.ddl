-- 标签修正
# 1(粒细胞系) -> 2
# 2(红细胞系) -> 1
# 4(单核细胞系) -> 0
update label_config set category_id=22 where category_id=1;
update label_config set category_id=1 where category_id=2;
update label_config set category_id=2 where category_id=22;
update label_config set category_id=0 where category_id=4;


update image_label set category_id=22 where category_id=1;
update image_label  set category_id=1 where category_id=2;
update image_label set category_id=2 where category_id=22;
update image_label set category_id=0 where category_id=4;

update image_label_history  set category_id=22 where category_id=1;
update image_label_history  set category_id=1 where category_id=2;
update image_label_history set category_id=2 where category_id=22;
update image_label_history set category_id=0 where category_id=4;