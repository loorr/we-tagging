rename table account to `220729_account`;
rename table asyn_task to `220729_asyn_task`;
rename table image_group_info to `220729_image_group_info`;
rename table image_info to `220729_image_info`;
rename table image_label to `220729_image_label`;
rename table image_label_algorithm to 220729_image_label_algorithm;
rename table image_label_history to 220729_image_label_history;
rename table image_version_role to 220729_image_version_role;
rename table label_config to 220729_label_config;
rename table role to 220729_role;
rename table user_role to 220729_user_role;

RENAME TABLE account to `220731_account`
    , asyn_task to `220731_asyn_task`
    , image_group_info to `220731_image_group_info`
    , image_info to `220731_image_info`
    , image_label to 220731_image_label
    , image_label_algorithm to 220731_image_label_algorithm
    , image_label_history to 220731_image_label_history
    , image_version_role to 220731_image_version_role
    , label_config to 220731_label_config
    , role to 220731_role
    , user_role to 220731_user_role;


CREATE TABLE `220731_account` LIKE `account`;
INSERT INTO `220731_account` SELECT * FROM `account`;
CREATE TABLE `220731_asyn_task` LIKE `asyn_task`;
INSERT INTO `220731_asyn_task` SELECT * FROM `asyn_task`;
CREATE TABLE `220731_image_group_info` LIKE `image_group_info`;
INSERT INTO `220731_image_group_info` SELECT * FROM `image_group_info`;
CREATE TABLE `220731_image_info` LIKE `image_info`;
INSERT INTO `220731_image_info` SELECT * FROM `image_info`;
CREATE TABLE `220731_image_label` LIKE `image_label`;
INSERT INTO `220731_image_label` SELECT * FROM `image_label`;
CREATE TABLE `220731_image_label_algorithm` LIKE `image_label_algorithm`;
INSERT INTO `220731_image_label_algorithm` SELECT * FROM `image_label_algorithm`;
CREATE TABLE `220731_image_label_history` LIKE `image_label_history`;
INSERT INTO `220731_image_label_history` SELECT * FROM `image_label_history`;
CREATE TABLE `220731_image_version_role` LIKE `image_version_role`;
INSERT INTO `220731_image_version_role` SELECT * FROM `image_version_role`;
CREATE TABLE `220731_version_role` LIKE `version_role`;
INSERT INTO `220731_version_role` SELECT * FROM `version_role`;
CREATE TABLE `220731_label_config` LIKE `label_config`;
INSERT INTO `220731_label_config` SELECT * FROM `label_config`;
CREATE TABLE `220731_role` LIKE `role`;
INSERT INTO `220731_role` SELECT * FROM `role`;
CREATE TABLE `220731_user_role` LIKE `user_role`;
INSERT INTO `220731_user_role` SELECT * FROM `user_role`;

