SELECT COUNT(*) as `有标签的有丝分裂图片` from (
    SELECT img_id from image_label where img_id in (
        SELECT id FROM `image_info` where version in ('有丝分裂数据')
    )
    GROUP BY img_id
) a;


CREATE TEMPORARY TABLE mytabel
with temp as (
    select sj.登记号, 检验子项目中文名称, 检验值, 单位,采样时间, 报告时间,手术日期
    from
        (SELECT 登记号, SUBSTRING_INDEX(手术日期,',',1) as 手术日期 from 手术_就诊) as sj
            join
        (
            SELECT  登记号, 检验子项目中文名称, 检验值, 单位,采样时间, 报告时间
            from 检验结果表
            where 检验子项目中文名称='国际标准化比值'
        ) as jy
        on sj.登记号 = jy.登记号
    where
        (
            CASE `采样时间` WHEN '1900-01-01 00:00:00' THEN TIMESTAMPDIFF(DAY, `报告时间`, 手术日期) > 0
                        ELSE    TIMESTAMPDIFF(DAY, `采样时间`, 手术日期) > 0 END
            )
)
SELECT  * from temp t
WHERE
    CASE `采样时间`
        WHEN '1900-01-01 00:00:00'
            THEN CONVERT (t.`报告时间`, date) = (select MAX(CONVERT (`报告时间`, date)) as max_date from  temp t2  where t2.登记号=t.登记号 group by 登记号)
        ELSE CONVERT (t.`采样时间`, date) = (select MAX(CONVERT (`采样时间`, date)) as max_date from  temp t2  where t2.登记号=t.登记号 group by 登记号)
        END

select 登记号, sum(检验值) from mytabel group by 登记号