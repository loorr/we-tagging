import os
import shutil
from flask import Flask
from flask import request
from flask import jsonify

import datetime
import cv2

app = Flask(__name__)

# constant
LABEL_ROOT_PATH = "/home/labels/"
IMAGE_SHAPE = (2080, 1664)

# common def
def logger(msg, *args):
    now_time = datetime.datetime.now().strftime('%Y.%m.%d %H:%M:%S')
    info = msg.format(*args)
    print(now_time, " ", info)

def get_error_response(message: str):
    return jsonify(message=message)


@app.route("/solo-seg-img", methods=["POST"])
def one_img():
    req = request.get_json()
    if req is None:
        return get_error_response("请求参数不能为空")
    logger("one_img: {}", req)
    img_path = req.get("image_path")
    img_name = req.get("image_name")
    uuid = req.get("uuid")
    env = req.get("env")
    if img_name is None or img_name is None or env is None or uuid is None:
        return get_error_response("请求参数非法")



    # 切割迭代，换模型等等，最初模型训练的数据集格式限制，后面估计数据多了就可以改了
    im = cv2.imread(img_path)
    if im.shape == IMAGE_SHAPE:
        im = cv2.resize(im, IMAGE_SHAPE)
        cv2.imwrite(img_path, im)

    cmd_str = "python /home/yolov5/detect.py --weights /home/yolov5/best.pt --conf 0.4 --source" \
              "  {} --project /home/labels/{} --name {} --save-txt"
    cmd_order = cmd_str.format(img_path, env, uuid)
    os.system(cmd_order)
    label_new_path = "/home/labels/" + env + "/" + uuid + "/labels/" + img_name + ".txt"
    return jsonify(label_file=label_new_path)

@app.route('/batch-seg-img', methods=["POST"])
def batch_seg():
    req = request.get_json()
    if req is None:
        return get_error_response("请求参数不能为空")
    logger("batch_seg: %s", req)
    dir_path = req.get("folder_path")
    # dir_name = req.get("folder_name")
    uuid = req.get("uuid")
    env = req.get("env")
    if dir_path is None or uuid is None or env is None:
        return get_error_response("请求参数非法")

    try:
        img_files_name = os.listdir(dir_path)
    except FileNotFoundError:
        return jsonify(label_file=None)

    for imgs in img_files_name:
        img_path = os.path.join(dir_path, imgs)
        im = cv2.imread(img_path)
        if IMAGE_SHAPE != im.shape:
            im = cv2.resize(im, IMAGE_SHAPE)
            cv2.imwrite(img_path, im)

    label_dir = LABEL_ROOT_PATH + "/{}".format(env)
    cmd_temple = "python /home/yolov5/detect.py --weights /home/yolov5/best.pt --conf 0.4 --source  {}  --project {} --name {} --save-txt"
    cmd_order = cmd_temple.format(dir_path, label_dir, uuid)
    os.system(cmd_order)
    label_dir_new_path = LABEL_ROOT_PATH +env+"/" + uuid + "/labels"
    return jsonify(label_file=label_dir_new_path)

app.run(port=5000)

