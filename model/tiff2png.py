import os
import shutil

import cv2
from numpy import uint8
from numpy.ma.core import fromfile


def tiff_to_png(image_path: str, outFolder):
    img = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
    if img is None:
        print(image_path)
    filename = image_path.split('\\')[-1].split('.')[0]
    save_path = outFolder + '\\' + filename + '.png'
    cv2.imwrite(save_path, img)

def convert_all(rootFolder: str):
    for currFolder, subFolder, subFile in os.walk(rootFolder):
        print(currFolder, subFolder, subFile)
        if len(subFolder) != 0:
            continue
        for name in subFile:
            new_folder = currFolder.replace(rootFolder, "E:\SCU-YANG\gu\organoid-2_out_png")
            if os.path.exists(new_folder) is False:
                os.mkdir(new_folder)
            # new_name = name.replace("图像", "image")
            # os.rename(currFolder + "\\" + name, currFolder + "\\" + new_name)
            if name == "remark.txt":
                shutil.copy(currFolder + "\\" + name, new_folder + "\\" + name)
            else:
                tiff_to_png(currFolder + "\\" + name, new_folder)

if __name__ == '__main__':
    # path = "organoid\\"
    # outFolder = "C:\Project\JavaStudy\we-tagging\model\out"
    # file = os.listdir(path)
    # for item in os.listdir(path):
    #     print("file path:" + item)
    #     tiff_to_png(path + item, outFolder)
    a = cv2.imread("E:\\SCU-YANG\\gu\\organoid-2_out\\az960=12-2=0.75\\image_46854.tif", 1)
    root = "E:\SCU-YANG\gu\organoid-2_out"
    convert_all(root)