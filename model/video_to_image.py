import cv2
import numpy as np
videoPath = "test.avi"
imageFolder = "images"
# 保存时的帧数间隔
interval = 5


def brenner(img):
    '''
    :param img:narray             the clearer the image,the larger the return value
    :return: float
    '''
    shape = np.shape(img)
    out = 0
    for y in range(0, shape[1]):
        for x in range(0, shape[0] - 2):
            out += (int(img[x + 2, y]) - int(img[x, y])) ** 2
    return out

def getImageVar(image):
    img2gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    imageVar = cv2.Laplacian(img2gray, cv2.CV_64F).var()
    return imageVar

if __name__ == '__main__':
    try:
        video_cap = cv2.VideoCapture(videoPath)
        if video_cap.isOpened():
            success = True
            fps = video_cap.get(cv2.CAP_PROP_FPS)
            print("Frames per second using video.get(cv2.CAP_PROP_FPS) : {0}".format(fps))
        else:
            success = False
            print("读取失败!")

        # 保存帧的索引
        frame_count = 0
        # 原视频的帧索引，与 interval*frame_count = frame_index
        frame_index = 0
        while success:
            success, frame = video_cap.read()
            if success is False:
                print("---> 第%d帧读取失败:" % frame_index)
                break
            print("---> 正在读取第%d帧:" % frame_index, success)
            if frame_index % interval == 0:
                v = getImageVar(frame)
                v2 = brenner(cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY))
                out_image_name = "{}/{}-{}-{}.jpg".format(imageFolder, frame_count, int(v), int(v2))
                cv2.imwrite(out_image_name, frame)
                frame_count += 1
            frame_index += 1
    finally:
        video_cap.release()