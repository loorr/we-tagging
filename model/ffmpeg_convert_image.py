import os

def getFolderName(relativePath: str):
    name = relativePath.replace("\\", "=")
    if name[0] == "=":
        name = name[1:]
    if name[len(name) -1] == "=":
        name = name[:-1]
    return "video=" + name


if __name__ == '__main__':
    rootFolder = "E:\SCU-YANG\gu\screen\screen"
    outRootFolder = "E:\SCU-YANG\gu\screen\screen_out"
    for currFolder, subFolder, subFile in os.walk(rootFolder):
        if len(subFolder) != 0:
            continue
        relativePath = currFolder[len(rootFolder) : ]
        outFolder = outRootFolder + "\\" + getFolderName(relativePath)
        if os.path.exists(outFolder) is False:
            os.mkdir(outFolder)
        #print(currFolder, subFolder, subFile)
        sign = False
        for name in subFile:
            if name.find(".avi") != -1:
                sign = True
                # avi = currFolder + "\\" + name
                # outName = name[3:6]
                # command = 'ffmpeg -i {} -vf "select=eq(pict_type\,I)"  -vsync vfr -qscale:v 2 -f image2 {}-%06d.jpg'\
                #     .format(avi, outFolder + "\\" + outName)
                # os.system(command)
                # print(outFolder)
        if sign == False:
            print(currFolder)