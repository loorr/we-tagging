import logging

from logstash import TCPLogstashHandler
from logstash.formatter import LogstashFormatterBase

LOG_STASH_HOST = '110.42.172.122'
LOG_STASH_PORT = 4560
SERVICE = 'model-server'


class MyLogstashFormatter(LogstashFormatterBase):
    def __init__(self, message_type='Logstash', tags=None, fqdn=False, status=None):
        super(MyLogstashFormatter, self).__init__(message_type, tags, fqdn)
        self.status = status if status else {}

    def format(self, record):
        message = {
            '@timestamp': self.format_timestamp(record.created),
            'message': self.format_exception(record.exc_info) if self.format_exception(
                record.exc_info) else record.getMessage(),
            'path': '[line:{}]{}'.format(record.lineno, record.pathname),
            'tags': self.tags,
            'process': record.processName,
            'msgType': self.message_type,
            'service': SERVICE,
            'level': record.levelname,
            'logger_name': self.host
        }
        # Add extra fields
        message.update(self.get_extra_fields(record))

        # delete random item if length of status over 1000
        [self.status.popitem() for i in range(len(self.status) - 1000) if len(self.status) >= 1000]
        return self.serialize(message)


class LogHandler(TCPLogstashHandler):
    def __init__(self, host, port, message_type='flask', tags=None, fqdn=False, version=0):
        super(LogHandler, self).__init__(host, port, message_type, tags, fqdn, version)
        self.formatter = MyLogstashFormatter(message_type, tags, fqdn)


def init(app):
    handler = LogHandler(LOG_STASH_HOST, LOG_STASH_PORT, tags='')
    app.logger.addHandler(handler)
    app.logger.setLevel(logging.INFO)
