import os
import re

import shutil

def out_remark_file(content: str, path: str):
    with open(path, 'w') as f:
        f.write(content)

def custom_make_translation(text, translation):
    regex = re.compile('|'.join(map(re.escape, translation)))
    return regex.sub(lambda match: translation[match.group(0)], text)

def get_new_name(name: str):
    ban_word = {
        "敲除": "",
        " ": "-",
        "对照": "",
        "过表达": "",
        "什么都没编辑": "",
        "双敲":"",
        "图像": "image"
    }
    new_name = custom_make_translation(name, ban_word)
    if new_name[len(new_name) - 1] == '-':
        new_name = new_name[:-1]
    return new_name


def rename():
    folder = "E:/SCU-YANG/骨细胞/organoid-1/QBY"
    for name in os.listdir(folder):
        # out_path = folder + "/{}/remark.txt".format(name)
        # out_remark_file(name, out_path)
        new_name = get_new_name(name)
        if new_name == name:
            continue
        os.rename(folder + "/" + name, folder + "/" + new_name)

def compressed_path(folder):
    """整理路径"""
    new_folder = "E:/SCU-YANG/骨细胞/organoid-2_out"
    for currFolder, subFolder, subFile in os.walk(folder):
        print(currFolder, subFolder, subFile)
        if len(subFolder) == 0:
            new_path = currFolder.replace(folder, "").replace("\\", "=")
            if new_path[0] == "=":
                new_path = new_path[1:]
            if new_path[len(new_path)-1] == "=":
                new_path = new_path[:-1]
            for name in subFile:
                if os.path.exists(new_folder + "/" + new_path) is False:
                    os.mkdir(new_folder + "/" + new_path)
                shutil.copy(currFolder + "/" +name, new_folder + "/" + new_path + "/" + name)



if __name__ == '__main__':
    compressed_path("E:/SCU-YANG/骨细胞/organoid-2")