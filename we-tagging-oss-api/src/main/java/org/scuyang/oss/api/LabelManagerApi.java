package org.scuyang.oss.api;

import com.tove.web.infra.common.PageResult;
import com.tove.web.infra.common.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.scuyang.oss.api.req.AddOrUpdateLabelAdminReq;
import org.scuyang.oss.api.req.DeleteLabelAdminReq;
import org.scuyang.oss.api.req.GetLabelInfoPageAdminReq;
import org.scuyang.oss.api.vo.LabelConfigOssVo;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@Api(value = "标签信息管理接口")
public interface LabelManagerApi {

    @ApiOperation("保存标签信息")
    @PostMapping(value = "/label-manager/add-label", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> addLabelInfo(@RequestBody @Validated AddOrUpdateLabelAdminReq req);

    @ApiOperation("修改标签信息")
    @PostMapping(value = "/label-manager/update-label", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> updateLabelInfo(@RequestBody @Validated AddOrUpdateLabelAdminReq req);

    @ApiOperation("删除标签信息")
    @PostMapping(value = "/label-manager/delete-label", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> deleteLabelInfo(@RequestBody @Validated DeleteLabelAdminReq req);

    @ApiOperation("分页查询标签信息")
    @PostMapping(value = "/label-manager/get-label-page", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<PageResult<LabelConfigOssVo>> getLabelInfoPage(@RequestBody @Validated GetLabelInfoPageAdminReq req);
}
