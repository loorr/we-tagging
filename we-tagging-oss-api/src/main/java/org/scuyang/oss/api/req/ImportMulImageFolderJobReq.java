package org.scuyang.oss.api.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class ImportMulImageFolderJobReq {

    @ApiModelProperty(value = "原始文件夹")
    @NotEmpty
    private String sourcePath;

    @ApiModelProperty(value = "版本号")
    @NotEmpty
    private String version;
}
