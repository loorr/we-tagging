package org.scuyang.oss.api.vo;


import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel
public class ImageAccessVo {

    private String md5;
    /** 图片资源url */
    private String imgUrl;
    private String imgSrcName;
}
