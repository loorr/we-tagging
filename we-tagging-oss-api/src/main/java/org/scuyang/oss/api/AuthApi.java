package org.scuyang.oss.api;

import com.tove.web.infra.common.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.scuyang.oss.api.req.LoginReq;
import org.scuyang.oss.api.vo.LoginVo;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Api(value = "权限验证")
public interface AuthApi {

    @Deprecated
    @ApiOperation("用户登陆")
    @PostMapping(value = "/auth/user-login", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<LoginVo> login(@RequestBody @Validated LoginReq req);

    @Deprecated
    @ApiOperation("获取用户信息")
    @GetMapping(value = "/auth/get-user-info", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<LoginVo> getUserInfo(@RequestBody @Validated String token);

}
