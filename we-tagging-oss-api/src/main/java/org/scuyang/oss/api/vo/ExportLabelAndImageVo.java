package org.scuyang.oss.api.vo;


import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel
public class ExportLabelAndImageVo {
    private String downloadUrl;
    private String localPath;
}
