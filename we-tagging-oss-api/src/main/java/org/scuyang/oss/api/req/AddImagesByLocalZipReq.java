package org.scuyang.oss.api.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@ApiModel
public class AddImagesByLocalZipReq {
    @ApiModelProperty(value = "path")
    @NotEmpty
    private String path;
}
