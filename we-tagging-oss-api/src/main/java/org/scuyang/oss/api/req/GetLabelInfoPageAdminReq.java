package org.scuyang.oss.api.req;

import com.tove.web.infra.common.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class GetLabelInfoPageAdminReq extends Page {

    @ApiModelProperty(value = "标签版本")
    private String version;

    @ApiModelProperty(value = "标签类别id")
    private Integer categoryId;

    @ApiModelProperty(value = "标签名称id")
    private Integer nameId;

    @ApiModelProperty(value = "标签的名字")
    private String name;

    @ApiModelProperty(value = "类的名字")
    private String category;

}
