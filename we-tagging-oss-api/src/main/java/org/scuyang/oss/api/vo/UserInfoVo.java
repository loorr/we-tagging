package org.scuyang.oss.api.vo;

import lombok.Data;

import java.util.List;

@Data
public class UserInfoVo {
    private String avatar = "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif";
    private String introduction = "i am a user.";
    private String name;
    private List<String> roles;
}
