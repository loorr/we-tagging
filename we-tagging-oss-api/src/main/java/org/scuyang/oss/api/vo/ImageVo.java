package org.scuyang.oss.api.vo;

import lombok.Data;

import java.util.Date;

@Data
public class ImageVo {

    private Long id;

    /**
     * 图片版本
     */
    private String version;

    /**
     * 图片人物
     */
    private String imgGroup;

    /**
     * 图片原始名称
     */
    private String imgSrcName;

    private String uri;

    private Integer labelNum;

    private String imgFeature;

    private Date dbModifyTime;
}
