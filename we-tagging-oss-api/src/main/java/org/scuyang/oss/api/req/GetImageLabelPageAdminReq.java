package org.scuyang.oss.api.req;

import com.tove.web.infra.common.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@ApiModel
public class GetImageLabelPageAdminReq extends Page {

    @ApiModelProperty(value = "版本号")
    @NotEmpty
    private String version;

    @ApiModelProperty(value = "是否需要分页")
    @NotNull
    private Boolean needPage;

    @ApiModelProperty(value = "categoryId")
    private Boolean categoryId;

    @ApiModelProperty(value = "classId")
    private Boolean classId;
}
