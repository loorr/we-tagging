package org.scuyang.oss.api.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel
public class ImportImageReq {

    @ApiModelProperty(value = "图片md5值")
    @NotNull
    private String md5;

    @ApiModelProperty(value = "图片分组")
    @NotNull
    private String group;

    @ApiModelProperty(value = "图片人物")
    @NotNull
    private String people;

    @ApiModelProperty(value = "图片原始名称")
    @NotNull
    private String imgSrcName;

    @ApiModelProperty(value = "图片大小， kb")
    @NotNull
    private Long imgSize;

    @ApiModelProperty(value = "图片原始长度 px")
    @NotNull
    private Long imgLength;

    @ApiModelProperty(value = "图片原始宽度 px")
    @NotNull
    private Long imgWidth;

}
