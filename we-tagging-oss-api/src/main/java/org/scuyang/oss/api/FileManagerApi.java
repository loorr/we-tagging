package org.scuyang.oss.api;

import com.tove.web.infra.common.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.scuyang.oss.api.req.AddFileReq;
import org.scuyang.oss.api.req.AddImagesByLocalZipReq;
import org.scuyang.oss.api.req.ExportLabelAndImageReq;
import org.scuyang.oss.api.vo.ExportLabelAndImageVo;
import org.scuyang.oss.api.vo.ImageAccessVo;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

@Api(value = "文件上传下载，信息导出")
public interface FileManagerApi {

    @ApiOperation("文件上传")
    @PostMapping(value = "/file-manager/add-file", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<ImageAccessVo> addFile(MultipartFile file, @Validated AddFileReq req);

    // TODO
    @ApiOperation("腾讯云OSS文件上传")
    @PostMapping(value = "/file-manager/add-cos-file", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> addCosFile(@RequestBody @Validated AddFileReq req);

    @ApiOperation("标签文件和图片下载")
    @PostMapping(value = "/file-manager/export-label-and-image-info", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<ExportLabelAndImageVo> exportLabelAndImage(@RequestBody @Validated ExportLabelAndImageReq req);

    @ApiOperation("压缩包文件批量图片上传, 二级目录, zip文件名为version, 子目录名为group")
    @PostMapping(value = "/file-manager/add-batch-images-by-zip", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> addBatchImagesByZip(@RequestBody @Validated MultipartFile file);

    @ApiOperation("本地文件批量图片上传, 二级目录, zip文件名为version, 子目录名为group")
    @PostMapping(value = "/file-manager/add-batch-images-by-local-zip", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> addBatchImagesByLocalZip(@RequestBody @Validated AddImagesByLocalZipReq req);

}
