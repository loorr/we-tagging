package org.scuyang.oss.api.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.scuyang.common.enums.ModelEnum;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@ApiModel
public class SingleImageRecognizeReq {

    @ApiModelProperty(value = "请求的uri", hidden = true)
    @NotEmpty
    private String uri = "/solo-seg-img";

    @ApiModelProperty(value = "图片完整路径, 包含后缀名",hidden = true)
    private String ImagePath;

    @ApiModelProperty(value = "图片名md5, 不包含后缀名",hidden = true)
    private String ImageName;

    @ApiModelProperty(value = "imgId")
    @NotNull
    private Long imgId;

    @Deprecated
    @ApiModelProperty(value = "需要调用的模型类型")
    private ModelEnum modelEnum = ModelEnum.BLOOD;

    @ApiModelProperty(value = "需要调用的模型类型")
    private String modelCode = "HEMOCYTE-yolo";

    @ApiModelProperty(value = "x")
    private BigDecimal x = BigDecimal.ONE;

    @ApiModelProperty(value = "y")
    private BigDecimal y = BigDecimal.ONE;

    @ApiModelProperty(value = "w")
    private BigDecimal width = BigDecimal.ONE;

    @ApiModelProperty(value = "h")
    private BigDecimal height = BigDecimal.ONE;
}


