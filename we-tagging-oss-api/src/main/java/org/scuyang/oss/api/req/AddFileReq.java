package org.scuyang.oss.api.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@ApiModel
public class AddFileReq {
    @ApiModelProperty(value = "标签分组")
    @NotEmpty
    private String group;

    @ApiModelProperty(value = "版本号")
    @NotEmpty
    private String version;
}
