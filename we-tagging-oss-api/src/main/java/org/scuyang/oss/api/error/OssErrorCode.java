package org.scuyang.oss.api.error;

import com.tove.web.infra.common.BaseError;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OssErrorCode implements BaseError {

    NO_FILES_IN_TARGET_FOLDER("502", "目标文件夹没有文件."),
    NO_TARGET_FOLDER("500", "没有目标文件夹."),
    NO_TARGET_IMAGE("501", "没有目标图片信息."),
    OPERATION_INVALID("502", "没有任何操作"),
    FILE_ERROE("1003", "文件错误"),
    CAN_NOT_OPEN_ZIP("1004", "打不开ZIP文件"),
    DUPLICATE_KET("1005", "信息重复"),
    USER_PASSWORD_ERROR("1006", "用户名或密码错误"),
    ;
    private final String code;
    private final String msg;

}
