package org.scuyang.oss.api.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ImageLabelVo {
    private Long id;

    /** 图片ID */
    private Long imgId;

    private Integer categoryId;
    private Integer classId;
    private String categoryName;
    private String className;

    /** x */
    private BigDecimal coordinateX;

    /** y */
    private BigDecimal coordinateY;

    /** w */
    private BigDecimal width;

    /** h */
    private BigDecimal height;

    private String md5;

}
