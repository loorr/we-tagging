package org.scuyang.oss.api.error;


import com.tove.web.infra.common.BaseError;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OssException extends RuntimeException implements BaseError {
    private String code;
    private String msg;

    public OssException(BaseError baseError){
        this.code = baseError.getCode();
        this.msg = baseError.getMsg();
    }
}
