package org.scuyang.oss.api.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@ApiModel
public class AddOrUpdateLabelAdminReq {

    @ApiModelProperty
    private Long id;

    @ApiModelProperty
    @NotEmpty
    private String version;

    @ApiModelProperty
    @NotNull
    private Integer categoryId;

    @ApiModelProperty
    @NotEmpty
    private String categoryName;

    @ApiModelProperty
    @NotNull
    private Integer classId;

    @ApiModelProperty
    @NotEmpty
    private String className;

    @ApiModelProperty
    private String shortcutKey;

    @ApiModelProperty
    private String abnormalState;

    @ApiModelProperty
    private String color;

    @ApiModelProperty
    private String remark;

}
