package org.scuyang.oss.api;


import com.tove.web.infra.common.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.scuyang.oss.api.req.*;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Api(value = "执行批处理任务的api")
public interface JobApi {

    @ApiOperation("调用算法接口，批量识别图片")
    @PostMapping(value = "/job/batch-image-recognize", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> batchImageRecognize(@RequestBody @Validated BatchImageRecognizeReq req);

    @ApiOperation("调用算法接口，识别单张图片")
    @PostMapping(value = "/job/single-image-recognize", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> singleImageRecognize(@RequestBody @Validated SingleImageRecognizeReq req);

//    @ApiOperation("ASYN 调用算法接口，识别单张图片")
//    @PostMapping(value = "/job/single-image-recognize-asyn", produces = MediaType.APPLICATION_JSON_VALUE)
//    Response<Boolean> singleImageRecognizeAsyn(@RequestBody @Validated SingleImageRecognizeReq req);


    @ApiOperation("文件夹导入图片,一个组")
    @PostMapping(value = "/job/import-image-folder", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> importImageFolder(@RequestBody @Validated ImportImageFolderJobReq req);

    @ApiOperation("文件夹导入图片,一个组")
    @PostMapping(value = "/job/import-mul-image-folder", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> importMulImageFolder(@RequestBody @Validated ImportMulImageFolderJobReq req);
//
//    @ApiOperation("文件夹导入服务器本地图片")
//    @PostMapping(value = "/job/import-local-image-folder", produces = MediaType.APPLICATION_JSON_VALUE)
//    Response<Boolean> importLocalImageFolder(@RequestBody @Validated ImportLocalFolderReq req);

    @ApiOperation("导出已打标签的图片信息")
    @PostMapping(value = "/job/export-image-label-info", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> exportImageLabelInfo(@RequestBody @Validated ImportImageFolderJobReq req);
}
