package org.scuyang.oss.api;

import com.tove.web.infra.common.PageResult;
import com.tove.web.infra.common.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.scuyang.oss.api.req.GetImageLabelPageAdminReq;
import org.scuyang.oss.api.vo.ImageLabelVo;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Api(value = "已标注信息管理接口")
public interface ImageLabelApi {

    @ApiOperation("分页查询标签信息")
    @PostMapping(value = "/image-label/get-image-label-page", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<PageResult<ImageLabelVo>> getImageLabelPage(@RequestBody @Validated GetImageLabelPageAdminReq req);

}
