package org.scuyang.oss.api.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel
public class LabelConfigOssVo {
    /**
     * 自增id
     */
    private Long id;

    /**
     * 标签分组，标签版本
     */
    private String version;

    /**
     * 标签类别编号
     */
    private Integer categoryId;

    /**
     * 标签类别描述
     */
    private String categoryName;

    /**
     * 标签类编号
     */
    private Integer classId;

    /**
     * 标签描述
     */
    private String className;

    private String shortcutKey;

    /**
     * 是否异常
     */
    private String abnormalState;

    private String color;

    /**
     * 数据库更新时间
     */
    private Date dbModifyTime;
}
