package org.scuyang.oss.api.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@ApiModel
public class BatchImportImageReq {

    @ApiModelProperty(value = "图片信息列表")
    @NotNull
    private List<ImportImageReq> imageReqList;

}
