package org.scuyang.oss.api.vo;

import lombok.Data;

@Data
public class LoginVo {
    private String token;
}
