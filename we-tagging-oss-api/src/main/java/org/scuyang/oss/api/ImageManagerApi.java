package org.scuyang.oss.api;

import com.tove.web.infra.common.PageResult;
import com.tove.web.infra.common.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.scuyang.oss.api.req.GetImagePageAdminReq;
import org.scuyang.oss.api.req.ImportImageReq;
import org.scuyang.oss.api.vo.ImageVo;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Api(value = "打标签接口")
public interface ImageManagerApi {

    @Deprecated
    @ApiOperation("保存图片信息")
    @PostMapping(value = "/image-manager/import-image", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> importImage(@RequestBody @Validated ImportImageReq req);

    @Deprecated
    @ApiOperation("批量保存图片信息")
    @PostMapping(value = "/image-manager/batch-import-images", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Boolean> batchImportImages(@RequestBody @Validated ImportImageReq req);

    @ApiOperation("分页查询图片信息")
    @PostMapping(value = "/image-manager/get-image-page", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<PageResult<ImageVo>> getImagePage(@RequestBody @Validated GetImagePageAdminReq req);

}
