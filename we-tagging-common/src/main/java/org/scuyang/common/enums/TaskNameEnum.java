package org.scuyang.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TaskNameEnum {
    IMPORT_IMAGE_BY_UPLOAD_ZIP(0, "通过上传压缩包导入图片", ExpirationTimeEnum.EX_60_SECONDS.getSecond()),
    IMPORT_IMAGE_BY_LOCAL_ZIP(1, "通过本地压缩包导入图片",  ExpirationTimeEnum.EX_60_SECONDS.getSecond()),
    EXPORT_IMAGE_TO_LOCAL_ZIP(2, "导出图片和标签信息到本地压缩文件", ExpirationTimeEnum.EX_60_SECONDS.getSecond()),
    EXPORT_IMAGE_TO_DOWNLOAD_ZIP(3, "导出图片和标签信息压缩包以供下载",  ExpirationTimeEnum.EX_60_SECONDS.getSecond()),

    BATCH_IMAGE_RECOGNIZE(4,"批量图片识别", ExpirationTimeEnum.EX_5_MINUTES.getSecond()),
    SINGLE_IMAGE_RECOGNIZE(5, "单张图片识别", ExpirationTimeEnum.EX_30_SECONDS.getSecond()),
    ;

    private Integer code;
    private String taskName;
    private Long maxExpirationTime;

    public static TaskNameEnum getTaskNameEnum(Integer code){
        if (code == null || code < 0){
            return null;
        }
        for(TaskNameEnum taskNameEnum: TaskNameEnum.values()){
            if (code.equals(taskNameEnum.getCode())){
                return taskNameEnum;
            }
        }
        return null;
    }


    @Getter
    @AllArgsConstructor
    public enum ExpirationTimeEnum{
        EX_10_MINUTES(10 * 60 * 1000L),
        EX_5_MINUTES(5 * 60 * 1000L),
        EX_60_SECONDS(60 * 1000L),
        EX_30_SECONDS(30 * 1000L),
        EX_10_SECONDS(30 * 1000L),
        ;
        private Long second;
    }
}
