package org.scuyang.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ImageLabelStateEnum {
    NOT_TAG(0),
    HAD_TAG(1),
    WAIT_APPROVE(2)
    ;

    public static ImageLabelStateEnum of(int state) {
        for (ImageLabelStateEnum e : ImageLabelStateEnum.values()) {
            if (e.getCode() == state) {
                return e;
            }
        }
        return null;
    }

    public static ImageLabelStateEnum of(Boolean state){
        if (state == null) {
            return null;
        }
        return state ? HAD_TAG : NOT_TAG;
    }


    private Integer code;
}
