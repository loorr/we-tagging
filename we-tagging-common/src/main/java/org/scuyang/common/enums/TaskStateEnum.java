package org.scuyang.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TaskStateEnum {
    /** 新建任务态 */
    CREATE(0),
    /** 发送异常终止 */
    EXCEPTION_END(1),
    /** 手动终止 */
    ABORT_END(2),
    /** 运行完成 */
    COMPLETE(3),
    ;

    private Integer code;
}
