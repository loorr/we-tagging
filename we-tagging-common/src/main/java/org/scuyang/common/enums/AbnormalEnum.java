package org.scuyang.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AbnormalEnum {
    NONE(0),
    NORMAL(1),
    SUSPICIOUS(2),
    ABNORMAL(3),
    ;
    private Integer code;
}
