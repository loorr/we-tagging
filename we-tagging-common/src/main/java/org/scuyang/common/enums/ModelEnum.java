package org.scuyang.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ModelEnum {
    /** 成都中医药 */
    BLOOD,

    /** 荧光细胞模型 */
    FLUORECYTE,

    /** 骨细胞 */
    ORGANOID
    ;
}
