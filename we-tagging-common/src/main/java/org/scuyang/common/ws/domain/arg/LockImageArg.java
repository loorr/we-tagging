package org.scuyang.common.ws.domain.arg;

import lombok.Data;

@Data
public class LockImageArg{
    private Long imageId;
}
