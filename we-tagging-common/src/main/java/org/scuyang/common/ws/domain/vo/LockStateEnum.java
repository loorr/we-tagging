package org.scuyang.common.ws.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LockStateEnum {
    FREE,
    LOCKED,
    OWNED,
    ;
}
