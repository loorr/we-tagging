package org.scuyang.common.ws;

import lombok.Data;

import java.util.Set;

@Data
public class WsMsgDO<T> {
    private final static String OK_MSG = "OK";
    private final static String OK_CODE = "200";
    private WsMsgType type;
    private Set<String> uids;
    private T data;

    /** 发生错误时 */
    private String code;
    private String msg;

    public WsMsgDO(String code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public WsMsgDO(WsMsgType type, T data){
        this.msg=OK_MSG;
        this.type=type;
        this.code=OK_CODE;
        this.data = data;
    }

}
