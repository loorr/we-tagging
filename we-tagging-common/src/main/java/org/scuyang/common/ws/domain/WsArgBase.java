package org.scuyang.common.ws.domain;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tove.web.infra.common.BaseErrorCode;
import com.tove.web.infra.common.BaseException;
import lombok.Data;
import org.scuyang.common.ws.WsMsgType;
import org.scuyang.common.ws.domain.arg.GetLockStateArg;
import org.scuyang.common.ws.domain.arg.LockImageArg;
import org.scuyang.common.ws.domain.arg.UnLockImageArg;


@Data
public class WsArgBase<T> {
    private WsMsgType type;
    private Long uid;
    private T data;

    public static WsArgBase getMsgType(String msg){ ;
        JSONObject jsonObject = JSONObject.parseObject(msg);
        String type = (String) jsonObject.get("type");
        WsMsgType wsMsgType = WsMsgType.of(type);
        if(wsMsgType == null) {
            throw new BaseException(BaseErrorCode.ILLEGAL_PARAMETERS);
        }
        String data = jsonObject.get("data").toString();
        switch (wsMsgType){
            case LOCK_IMAGE:
                WsArgBase<LockImageArg> arg = new WsArgBase();
                arg.setType(wsMsgType);
                arg.setData(JSON.parseObject(data, LockImageArg.class));
                return arg;
            case UNLOCK_IMAGE:
                WsArgBase<UnLockImageArg> unLockImageArgWsArg = new WsArgBase();
                unLockImageArgWsArg.setType(wsMsgType);
                unLockImageArgWsArg.setData(JSON.parseObject(data, UnLockImageArg.class));
                return unLockImageArgWsArg;
            case LOCK_STATE:
                WsArgBase<GetLockStateArg> lockStateWsArg = new WsArgBase();
                lockStateWsArg.setType(wsMsgType);
                lockStateWsArg.setData(JSON.parseObject(data, GetLockStateArg.class));
                return lockStateWsArg;
            default:
                throw new BaseException(BaseErrorCode.ILLEGAL_PARAMETERS);
        }
    }
}
