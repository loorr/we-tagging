package org.scuyang.common.ws.domain.arg;

import lombok.Data;

@Data
public class UnLockImageArg{
    private Long imageId;
}
