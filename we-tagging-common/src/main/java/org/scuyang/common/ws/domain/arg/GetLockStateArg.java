package org.scuyang.common.ws.domain.arg;

import lombok.Data;

@Data
public class GetLockStateArg {
    private Long imageId;
}
