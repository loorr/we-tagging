package org.scuyang.common.ws;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.scuyang.common.ws.domain.arg.GetLockStateArg;
import org.scuyang.common.ws.domain.arg.LockImageArg;
import org.scuyang.common.ws.domain.arg.UnLockImageArg;

@Getter
@AllArgsConstructor
public enum WsMsgType {
    SINGLE_IMAGE(String.class,String.class),
    IMAGE_GROUP(String.class,String.class),

    /** 图片锁 */
    LOCK_IMAGE(String.class, LockImageArg.class),
    UNLOCK_IMAGE(String.class, UnLockImageArg.class),
    LOCK_STATE(String.class, GetLockStateArg.class),

    NOTHING(null,null),
    ;

    private final Class<?> voClazz;
    private final Class<?> argClazz;

    public static WsMsgType of(String name) {
        if(name == null || name.length() == 0){
            return null;
        }
        for (WsMsgType type : WsMsgType.values()) {
            if (type.name().equals(name)) {
                return type;
            }
        }
        return null;
    }
}
