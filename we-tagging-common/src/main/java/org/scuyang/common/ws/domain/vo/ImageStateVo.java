package org.scuyang.common.ws.domain.vo;

import lombok.Data;

@Data
public class ImageStateVo {
    private Long imageId;
    private LockStateEnum lockState;
}
