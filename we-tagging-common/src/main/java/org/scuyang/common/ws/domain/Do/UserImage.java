package org.scuyang.common.ws.domain.Do;

import com.alibaba.fastjson.JSON;
import lombok.Data;

import java.util.Date;

@Data
public class UserImage {
    private Long uid;
    private Date date;
    private Long imageId;


    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
