package org.scuyang.common.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FileClassType {
    PICS(1),
    DOCS(2),
    VIDEOS(3),
    TOTTENTS(4),
    AUDIOS(5),
    OTHERS(6);

    private Integer value;
}
