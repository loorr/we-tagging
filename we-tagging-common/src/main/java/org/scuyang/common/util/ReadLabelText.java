package org.scuyang.common.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ReadLabelText {

    /**
     * 功能：Java读取txt文件的内容
     * 步骤：
     * 1：先获得文件句柄
     * 2：获得文件句柄当做是输入一个字节码流，需要对这个输入流进行读取
     * 3：读取到输入流后，需要读取生成字节流 4：一行一行的输出。readline()。 备注：需要考虑的是异常情况
     * @param filePath 文件路径[到达文件:如： D:\aa.txt]
     * @return 将这个文件按照每一行切割成数组存放到list中。
     */
    public static List<String> readTxtFileIntoStringArrList(String filePath) {
        List<String> list = new ArrayList();
        try {
            String encoding = "GBK";
            File file = new File(filePath);
            if (file.isFile() && file.exists()) { // 判断文件是否存在
                InputStreamReader read = new InputStreamReader(
                        new FileInputStream(file), encoding);// 考虑到编码格式
                BufferedReader bufferedReader = new BufferedReader(read);
                String lineTxt = null;

                while ((lineTxt = bufferedReader.readLine()) != null) {
                    list.add(lineTxt);
                }
                bufferedReader.close();
                read.close();
            } else {
                throw new RuntimeException("找不到指定的文件");
            }
        } catch (Exception e) {
            throw new RuntimeException("读取文件内容出错");
        }
        return list;
    }

    public static void main(String[] args) {
        List<String> list = readTxtFileIntoStringArrList("C:\\Users\\zjianfa\\Pictures\\ossRoot\\temp\\3g21m3tl83u4jb9tn9mpmr24lh.txt");
        for(String s: list){
            String[] arr = s.split("\\s+");

        }
        System.out.println(list.size());
    }
}
