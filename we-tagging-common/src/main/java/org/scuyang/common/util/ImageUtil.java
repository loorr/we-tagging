package org.scuyang.common.util;

import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;

@Log4j2
public class ImageUtil {

    @SneakyThrows
    public static void getImageInfo(File file){
        BufferedImage sourceImg =ImageIO.read(new FileInputStream(file));
        System.out.println(String.format("%.1f", file.length()/1024.0));// 源图大小
        System.out.println(sourceImg.getWidth()); // 源图宽度
        System.out.println(sourceImg.getHeight()); // 源图高度
        log.info("1212");
    }


}
