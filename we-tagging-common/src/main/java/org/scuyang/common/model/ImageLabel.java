package org.scuyang.common.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class ImageLabel extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 图片ID */
    private Long imgId;

    /** 标签类编号 */
    private Integer categoryId;

    /** 标签编号 */
    private Integer classId;

    /** x */
    private BigDecimal coordinateX;

    /** y */
    private BigDecimal coordinateY;

    /** w */
    private BigDecimal width;

    /** h */
    private BigDecimal height;

    /** history time */
    private Date time;
    private String labelExt;

    private String modifyUsername;
    private String imgModifyUsername;


    /**
     * 将标签文件转化为对象
     * @param line
     * @param imageId
     * @return
     */
    public static List<ImageLabel> transformLabelList(List<String> line, Long imageId){
        return line.stream().map((item)->{
            ImageLabel imageLabel = new ImageLabel();
            imageLabel.setImgId(imageId);
            if (item == null || "".equals(item)){
                return null;
            }
            String[] arr = item.split("\\s+");
            if (arr.length != 5){
                return null;
            }
            imageLabel.setCategoryId(new Integer(arr[0]));
            imageLabel.setCoordinateX(new BigDecimal(arr[1]));
            imageLabel.setCoordinateY(new BigDecimal(arr[2]));
            imageLabel.setWidth(new BigDecimal(arr[3]));
            imageLabel.setHeight(new BigDecimal(arr[4]));
            return imageLabel;
        }).collect(Collectors.toList());
    }

    public static List<ImageLabelAlgorithm> transformAlgoLabelList(List<String> line, Long imageId){
        return line.stream().map((item)->{
            ImageLabelAlgorithm imageLabel = new ImageLabelAlgorithm();
            imageLabel.setImgId(imageId);
            if (item == null || "".equals(item)){
                return null;
            }
            String[] arr = item.split("\\s+");
            if (arr.length != 5){
                return null;
            }
            imageLabel.setCategoryId(new Integer(arr[0]));
            imageLabel.setCoordinateX(new BigDecimal(arr[1]));
            imageLabel.setCoordinateY(new BigDecimal(arr[2]));
            imageLabel.setWidth(new BigDecimal(arr[3]));
            imageLabel.setHeight(new BigDecimal(arr[4]));
            return imageLabel;
        }).collect(Collectors.toList());
    }

    public boolean isEqualContent(ImageLabel newItem){
        return imgId.equals(newItem.imgId)
            && ((categoryId == null &&  newItem.categoryId == null) || categoryId.equals(newItem.categoryId))
            && ((classId == null &&  newItem.classId == null) || classId.equals(newItem.classId))
            && coordinateX.setScale(15, RoundingMode.HALF_UP).compareTo(newItem.coordinateX.setScale(15, RoundingMode.HALF_UP)) == 0
            && coordinateY.setScale(15, RoundingMode.HALF_UP).compareTo(newItem.coordinateY.setScale(15, RoundingMode.HALF_UP)) == 0
            && width.setScale(15, RoundingMode.HALF_UP).compareTo(newItem.width.setScale(15, RoundingMode.HALF_UP)) == 0
            && height.setScale(15, RoundingMode.HALF_UP).compareTo(newItem.height.setScale(15, RoundingMode.HALF_UP)) == 0;
    }
}
