package org.scuyang.common.model;

import lombok.Getter;

@Getter
public enum ModelTypeEnum {
    BACKEND,
    FONTEND
}
