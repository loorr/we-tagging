package org.scuyang.common.model;

import lombok.Data;

@Deprecated
@Data
public class FeatureSet extends BaseModel{
    private String setName;
    private Boolean valid;
    private String remark;
    private String imageVersion;
}
