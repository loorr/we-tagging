package org.scuyang.common.model;

import lombok.Data;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;

@Data
public class ApproveConfig extends BaseModel {
    private String taskType;
    private String taskValue;
    private String taskName;
    private String taskDesc;
    private String approverType;
    private String approverValue;
    private int requireNum;
    private List<String> approverValueList = new ArrayList<>();
    private Set<Long> approverUidList = new HashSet<>();


    public void setApproverValue(String approverValue) {
        this.approverValue = approverValue;
        if (!StringUtils.hasLength(approverValue)){
            return;
        }
        String[] approverValueArray = approverValue.split(",");
        this.approverValueList = Arrays.asList(approverValueArray);
    }

    public void transferApproverValueListToString() {
        if (CollectionUtils.isEmpty(approverValueList)){
            return;
        }
        StringBuilder sb = new StringBuilder();
        for (String value : this.approverValueList) {
            sb.append(value).append(",");
        }
        this.approverValue = sb.toString();
    }

}
