package org.scuyang.common.model;

import lombok.Data;

@Data
public class FeatureData extends BaseModel{
    private Long featureId;

    /**
     * 图片id
     */
    private Long imageId;

    /**
     * 数据
     */
    private String data;
}
