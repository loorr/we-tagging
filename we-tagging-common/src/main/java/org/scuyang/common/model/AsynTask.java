package org.scuyang.common.model;

import lombok.Builder;
import lombok.Data;
import org.scuyang.common.enums.TaskNameEnum;
import org.scuyang.common.enums.TaskStateEnum;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
public class AsynTask extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1121212L;

    /**
     * 自增id
     */
    private Long id;

    /**
     * 任务类型
     */
    private String taskName;

    private Integer taskCode;

    /**
     * 任务状态
     */
    private TaskStateEnum taskState;

    /**
     * 任务开始时间
     */
    private Date taskStartTime;

    /**
     * 任务开始时间
     */
    private Date taskEndTime;

    /**
     * 返回任务已经执行了多久
     * 如果没有结束,则返回当前时间差
     * @return
     */
    public Long getDiffSecond(){
        long nowDate;
        if (getTaskEndTime() == null){
            nowDate = new Date().getTime();
        }else {
            nowDate  = getTaskEndTime().getTime();
        }
        long startDateTime = getTaskStartTime().getTime();
        return (nowDate - startDateTime) / 1000;
    }

    public boolean isExpired(){
        TaskNameEnum taskNameEnum = TaskNameEnum.getTaskNameEnum(this.taskCode);
        if (taskNameEnum == null){
            return true;
        }
        return taskNameEnum.getMaxExpirationTime() <= getDiffSecond();
    }

    public static AsynTask getTaskStartInstance(TaskNameEnum taskNameEnum){
        AsynTask asynTask = AsynTask.builder()
                .taskName(taskNameEnum.getTaskName())
                .taskCode(taskNameEnum.getCode())
                .taskState(TaskStateEnum.CREATE)
                .taskStartTime(new Date())
                .build();
        return asynTask;
    }
}
