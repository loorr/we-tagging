package org.scuyang.common.model;

import lombok.Data;

@Data
public class RoleImageVersion extends BaseModel {
    private String role;
    private String version;
}
