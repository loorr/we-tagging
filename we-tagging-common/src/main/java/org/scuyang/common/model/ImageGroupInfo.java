package org.scuyang.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ImageGroupInfo extends BaseModel implements Serializable {
    private static final long serialVersionUID = 112121L;

    private String version;
    private String imgGroup;
    private String describe;
}
