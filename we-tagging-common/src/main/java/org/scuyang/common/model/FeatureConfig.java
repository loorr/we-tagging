package org.scuyang.common.model;

import lombok.Data;

@Data
public class FeatureConfig extends BaseModel{
    private String imageVersion;
    private String featureName;
    private String type;
    private Boolean valid;
    private String remark;
    private String dataFormat;
    private String defaultValue;
    private String featureType = "IMAGE";
}
