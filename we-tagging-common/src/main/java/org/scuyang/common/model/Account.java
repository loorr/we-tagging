package org.scuyang.common.model;

import lombok.Data;

import java.util.Date;

@Data
public class Account extends BaseModel{
    private String password;
    private Long uid;
    private String email;
    private Integer loginNum;
    private Date lastLoginTime;
    private String username;
}
