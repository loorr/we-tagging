package org.scuyang.common.model;

import lombok.Data;

@Data
public class ModelConfig extends BaseModel{
    private String version;
    private String modelName;
    private String modelCode;
    private String modelVersion;
    private String labelMapping;
    private String modelUrl;
    private String modelType;
}
