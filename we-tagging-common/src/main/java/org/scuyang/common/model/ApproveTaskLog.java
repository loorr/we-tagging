package org.scuyang.common.model;

import lombok.Data;

import java.util.Date;

@Data
public class ApproveTaskLog extends BaseModel{
    private Long taskId;
    private Long approverUid;
    private Integer approveStatus;
    private Date approveTime;
    private String remark;
    private Long deleteId;
}
