package org.scuyang.common.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.*;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ImageInfo extends BaseModel implements Serializable {

    private static final long serialVersionUID = 11111232323L;
    private static final String[] IMAGE_FEATURE = new String[]{"适合看红细胞", "适合看白细胞", "低倍镜", "油镜"};

    /**
     * 图片md5值
     */
    private String md5;

    /**
     * 图片版本
     */
    private String version;

    /**
     * 图片uri，url = oss.url.prefix uri
     */
    private String uri;

    /**
     * 图片存储的文件夹
     */
    private String folder;

    /**
     * 图片人物
     */
    private String imgGroup;

    /**
     * 图片原始名称
     */
    private String imgSrcName;

    /**
     * 图片大小， kb
     */
    private Long imgSize;

    /**
     * 图片原始长度 kb
     */
    private Long imgLength;

    /**
     * 图片原始宽度， kb
     */
    private Long imgWidth;

    /**
     * 是否打标签label_state(no[0]， ing[1]， had[2])
     */
    private Boolean labelState;

    private Integer labelNum;

    private String imgFeature;

    private String imageExt;
    private String lastModifyUsername;

    public static Map<String,Boolean> getEmptyFeatureMap(){
        List<String> allFeatureList = getAllImageFeatures();
        final Map<String, Boolean> result = new HashMap<>();
        allFeatureList.forEach(o->result.put(o,false));
        return result;
    }

    public Map<String, Boolean> getFeatureMap(){
        List<String> featureList = getImageFeatures();
        List<String> allFeatureList = getAllImageFeatures();
        final Map<String, Boolean> result = new HashMap<>();
        allFeatureList.forEach(o-> result.put(o,false));
        featureList.forEach(o->{
            if (result.containsKey(o)){
                result.put(o, true);
            }
        });
        return result;
    }

    public List<String> getImageFeatures(){
        List<String> result = new ArrayList<>();
        if (imgFeature == null || "".equals(imgFeature)){
            return result;
        }

        String[] strings = imgFeature.split(",");
        return Arrays.asList(strings);
    }

    public static List<String> getAllImageFeatures(){
        return Arrays.asList(IMAGE_FEATURE);
    }

    public static String generateImageFeatureString(List<String> feature){
        checkImageFeature(feature);
        StringBuilder sb = new StringBuilder(feature.size()*2);
        for (int i = 0; i < feature.size(); i++) {
            sb.append(feature.get(i));
            sb.append(",");
        }
        if (sb.length()>=2){
            sb.deleteCharAt(sb.length()-1);
        }
        return sb.toString();
    }

    private static void checkImageFeature(List<String> feature){
        Set<String> featuresSet = new HashSet<>(getAllImageFeatures());
        for(String item: feature){
            if (!featuresSet.contains(item)){
                throw new RuntimeException("image feature check not pass");
            }
        }
    }
}

