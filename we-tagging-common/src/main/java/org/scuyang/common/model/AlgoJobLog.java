package org.scuyang.common.model;

import lombok.Data;

@Data
public class AlgoJobLog extends BaseModel{
    private Long imgId;
    private String modelVersion;
    private String modelCode;
    private Boolean state;
}
