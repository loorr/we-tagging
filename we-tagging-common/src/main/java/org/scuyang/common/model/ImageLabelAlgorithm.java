package org.scuyang.common.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ImageLabelAlgorithm extends ImageLabel{
    private String modelCode;
    private String modelVersion;
    private BigDecimal score;
}
