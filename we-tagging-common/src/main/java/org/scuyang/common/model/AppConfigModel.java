package org.scuyang.common.model;

import lombok.Data;

@Data
public class AppConfigModel extends BaseModel{
    private String key;
    private String value;
    private String type;
    private Boolean valid;
}
