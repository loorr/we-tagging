package org.scuyang.common.model;

import lombok.Data;

@Data
public class UserRole extends BaseModel{

    private Long uid;
    private String role;

}
