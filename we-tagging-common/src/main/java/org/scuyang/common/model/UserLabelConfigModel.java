package org.scuyang.common.model;

import lombok.Data;


@Data
public class UserLabelConfigModel extends LabelConfig {
    private Long uid;
}
