package org.scuyang.common.model;

import lombok.Data;

@Data
public class GroupFeatureData {
    private String imageGroup;
    private String imageVersion;
    private Long featureId;
    private String data;
}
