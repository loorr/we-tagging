package org.scuyang.common.model;

import lombok.Data;

@Data
public class Role extends BaseModel{
    private String name;
    private String remark;
}
