package org.scuyang.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
public class ApproveCommon {

    @Getter
    @AllArgsConstructor
    public static enum ApproveTaskType {
        IMAGE_VERSION,
        IMAGE_GROUP,
        IMAGE,
        ;

        public static ApproveTaskType of(String name) {
            for (ApproveTaskType type : values()) {
                if (type.name().equals(name)) {
                    return type;
                }
            }
            return null;
        }
    }

    @Getter
    @AllArgsConstructor
    public static enum ApproveType {
        USER_GROUP,
        ROLE,
        USER,
        ;
    }

    @Getter
    @AllArgsConstructor
    public static enum ApproveStatus {
        WAITING(0),
        SUCCESS(1),
        FAIL(2),
        ;
        private int code;
    }

}
