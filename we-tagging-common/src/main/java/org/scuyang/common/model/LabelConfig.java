package org.scuyang.common.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.scuyang.common.enums.AbnormalEnum;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class LabelConfig extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1234567890L;

    /**
     * 标签分组，标签版本
     */
    private String version;

    /**
     * 标签类别编号
     */
    private Integer categoryId;

    /**
     * 标签类别描述
     */
    private String categoryName;

    /**
     * 标签类编号
     */
    private Integer classId;

    /**
     * 标签描述
     */
    private String className;

    private String shortcutKey;

    /**
     * 是否异常
     */
    private AbnormalEnum abnormalState;

    private String color;

    private String remark;
    private Integer priority;
}