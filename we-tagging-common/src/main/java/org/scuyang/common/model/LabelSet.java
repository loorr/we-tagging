package org.scuyang.common.model;

import lombok.Data;

@Data
public class LabelSet extends BaseModel{

    /**
     * 标签集名称
     */
    private String name;

    /**
     * 图片数据集版本
     */
    private String imageVersion;

    /**
     * 是否有效
     */
    private int valid;

    /**
     * 标签集备注
     */
    private String remark;
}
