package org.scuyang.common.model;

import lombok.Data;

import java.util.Date;

@Data
public class ApproveTask extends BaseModel {
    private String imageVersion;
    private String imageGroup;
    private Long imageId;

    private Long requestUid;
    private Integer approveNum;
    private Date startTime;
    private Date endTime;

    private Long deleteId;
}
