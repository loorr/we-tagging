package org.scuyang.common.model;

import lombok.Data;

import java.util.Date;

@Data
public class BaseModel {
    /**
     * 自增id
     */
    private Long id;
    /**
     * 数据库插入时间
     */
    private Date dbCreateTime;

    /**
     * 数据库更新时间
     */
    private Date dbModifyTime;
}
