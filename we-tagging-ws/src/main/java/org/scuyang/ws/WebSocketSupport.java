package org.scuyang.ws;

import com.alibaba.fastjson.JSON;
import com.tove.web.infra.common.Response;
import com.tove.web.infra.common.WsResponse;
import lombok.extern.slf4j.Slf4j;
import org.scuyang.ws.common.QueryStringUtil;
import org.scuyang.ws.common.WsErrorCode;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.websocket.RemoteEndpoint;
import javax.websocket.Session;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class WebSocketSupport {

    private static final WsSessionManager sessionManager = new WsSessionManager();

    private WebSocketSupport() {}


    public static void pushByUid(String uid, String jsonStr) {
        if (!StringUtils.hasLength(uid)){
            System.out.println("uid is null " + jsonStr);
        }
        Session session = getSession(uid);
        if (session == null){
            System.out.println("session is null");
            log.error("session is null uid: {}", uid);
            return;
        }
        log.info("pushByUid uid: {}, session: {}", uid, session.getId());
        try {
            String message = jsonStr;
            RemoteEndpoint.Basic basic = session.getBasicRemote();
            if (basic != null){
                session.getBasicRemote().sendText(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <T> void pushToAll(WsResponse<T> response, List<Long> excludeUids) {
        for(Session session: sessionManager.getAllSession()){
            String uid = sessionManager.getUid(session);
            if (!CollectionUtils.isEmpty(excludeUids) && excludeUids.contains(Long.valueOf(uid))){
                continue;
            }
            if (session.isOpen()){
                push(session, response);
            }else{
                sessionManager.releaseSession(uid);
            }
        }
    }

    public static <T> void pushByUid(String uid, Response<T> response) {
        if (!StringUtils.hasLength(uid)){
            System.out.println("uid is null " + response.toString());
        }
        Session session = getSession(uid);
        if (session == null){
            System.out.println("session is null");
            return;
        }
        try {
            String message = JSON.toJSONString(response);
            RemoteEndpoint.Basic basic = session.getBasicRemote();
            if (basic != null){
                session.getBasicRemote().sendText(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <T> void pushByUid(String uid, WsResponse<T> response) {
        if (!StringUtils.hasLength(uid)){
            System.out.println("uid is null " + response.toString());
        }
        Session session = getSession(uid);
        if (session == null){
            System.out.println("session is null");
            return;
        }
        try {
            String message = JSON.toJSONString(response);
            RemoteEndpoint.Basic basic = session.getBasicRemote();
            if (basic != null){
                session.getBasicRemote().sendText(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <T> void push(Session session, WsResponse<T> response) {
        if (session == null){
            System.out.println("session is null");
        }
        try {
            String message = JSON.toJSONString(response);
            session.getBasicRemote().sendText(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <T> void pushPong(Session session) {
        try {
            session.getBasicRemote().sendText("pong");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void storageSession(Long uid, Session session) {
        String querystring = session.getQueryString();

        if (!StringUtils.hasText(querystring)){
            releaseSession(session);
        }
        sessionManager.save(String.valueOf(uid),session);
    }

    public static Session getSession(String id) {
        return sessionManager.get(id);
    }

    public static Long getUid(Session session) {
        return Long.valueOf(sessionManager.getUid(session));
    }

    public static void releaseSession(String id) {
        sessionManager.releaseSession(id);
    }

    public static void releaseSession(Session session) {
        String querystring = session.getQueryString();

        if (!StringUtils.isEmpty(querystring)) {
            Map<String, String> param = QueryStringUtil.parse(querystring);
            String key = param.get("userId");
            sessionManager.releaseSession(key);
        }
    }

    public static void closeSession(String message, Session session){
        try {
            if (session.isOpen()) {
                push(session, WsResponse.getFail(WsErrorCode.TOKEN_ILLEGAL));
                session.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class WsSessionManager {

        final ConcurrentHashMap<String, Session> sessionPool = new ConcurrentHashMap<>();
        final ConcurrentHashMap<String, String>  sessionIdToUid = new ConcurrentHashMap<>();

        public List<Session> getAllSession() {
            return new ArrayList<>(sessionPool.values());
        }

        void save(String key, Session session) {
            sessionPool.put(key, session);
            sessionIdToUid.put(session.getId(), key);
        }

        String getUid(Session session){
            String uid = sessionIdToUid.get(session.getId());
            return uid;
        }

        Session get(String key) {
            return sessionPool.get(key);
        }

        boolean haveSession(String key) {
            return sessionPool.containsKey(key);
        }

        void releaseSession(String key) {
            if (!sessionPool.containsKey(key)){
                return;
            }
            Session session = sessionPool.remove(key);
            sessionIdToUid.remove(session.getId());
            try {
                if (session.isOpen()) {
                    session.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
