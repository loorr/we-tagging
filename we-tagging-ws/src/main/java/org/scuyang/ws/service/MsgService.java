package org.scuyang.ws.service;

import com.tove.web.infra.common.BaseException;
import com.tove.web.infra.common.WsResponse;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.scuyang.common.ws.WsMsgType;
import org.scuyang.common.ws.domain.WsArgBase;
import org.scuyang.common.ws.domain.arg.GetLockStateArg;
import org.scuyang.common.ws.domain.arg.LockImageArg;
import org.scuyang.common.ws.domain.arg.UnLockImageArg;
import org.scuyang.common.ws.domain.vo.ImageStateVo;
import org.scuyang.core.RedisService;
import org.scuyang.core.constant.RedisConstant;
import org.scuyang.common.ws.domain.vo.LockStateEnum;
import org.scuyang.ws.WebSocketSupport;
import org.scuyang.ws.common.WsErrorCode;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class MsgService {

    @Resource
    private RedisService redisService;

    @Resource
    private RedissonClient redisClient;

    public void MsgHandler(WsArgBase argBase) {
        System.out.println("argBase: " + argBase.toString());
        if (argBase.getType() == WsMsgType.LOCK_IMAGE) {
            LockImageArg arg = (LockImageArg) argBase.getData();
            lockImage(argBase.getUid(), arg);
            return;
        }
        if (argBase.getType() == WsMsgType.UNLOCK_IMAGE) {
            UnLockImageArg arg = (UnLockImageArg) argBase.getData();
            unLockImage(argBase.getUid(), arg);
            return;
        }
        if (argBase.getType() == WsMsgType.LOCK_STATE){
            GetLockStateArg arg = (GetLockStateArg) argBase.getData();
            getLockState(argBase.getUid(), arg);
            return;
        }
    }

    private void getLockState(Long uid, GetLockStateArg arg) {
        RBucket<String> rLock = redisClient.getBucket(String.format(RedisConstant.IMAGE_LOCK, arg.getImageId()));
        boolean exists = rLock.isExists();
        LockStateEnum result;
        if (exists){
            if (rLock.get().equals(uid.toString())){
                result = LockStateEnum.OWNED;
            }else{
                result = LockStateEnum.LOCKED;
            }
        }else {
            result = LockStateEnum.FREE;
        }
        ImageStateVo vo = new ImageStateVo();
        vo.setLockState(result);
        vo.setImageId(arg.getImageId());
        WebSocketSupport.pushByUid(String.valueOf(uid), WsResponse.getOk(vo, WsMsgType.LOCK_STATE.name()));
    }

    private void lockImage(Long uid, LockImageArg arg) {
        if (arg == null || arg.getImageId() == null){
            throw new BaseException(WsErrorCode.PARAM_ILLEGAL);
        }
        ImageStateVo vo = new ImageStateVo();
        vo.setImageId(arg.getImageId());
        RBucket<String> rLock = redisClient.getBucket(String.format(RedisConstant.IMAGE_LOCK, arg.getImageId()));
        if (rLock.isExists()){
            if (rLock.get().equals(uid)){
                vo.setLockState(LockStateEnum.OWNED);
                WebSocketSupport.pushByUid(String.valueOf(uid), WsResponse.getOk(vo, WsMsgType.LOCK_IMAGE.name()));
                return;
            }
            throw new BaseException(WsErrorCode.LOCK_IMAGE_FAIL);
        }else{
            rLock.set(String.valueOf(uid), 10, TimeUnit.SECONDS);
            redisService.addUserImage(arg.getImageId(), uid);

            // set lock state to owned
            vo.setLockState(LockStateEnum.OWNED);
            WebSocketSupport.pushByUid(String.valueOf(uid), WsResponse.getOk(vo, WsMsgType.LOCK_IMAGE.name()));
            List<Long> uids =  Collections.singletonList(uid);

            // seed to other users
            vo.setLockState(LockStateEnum.LOCKED);
            WebSocketSupport.pushToAll(WsResponse.getOk(vo, WsMsgType.LOCK_IMAGE.name()), uids);
        }
    }

    private void unLockImage(Long uid, UnLockImageArg arg){
        RBucket<String> lock = redisClient.getBucket(String.format(RedisConstant.IMAGE_LOCK, arg.getImageId()));
        if (lock.isExists() && lock.get().equals(String.valueOf(uid))){
            lock.delete();
            redisService.removeUserImage(arg.getImageId());
            ImageStateVo vo = new ImageStateVo();
            vo.setLockState(LockStateEnum.FREE);
            vo.setImageId(arg.getImageId());
            WebSocketSupport.pushToAll(WsResponse.getOk(vo, WsMsgType.LOCK_IMAGE.name()), null);
        }else{
            GetLockStateArg arg1 = new GetLockStateArg();
            BeanUtils.copyProperties(arg, arg1);
            getLockState(uid, arg1);
        }
    }
}
