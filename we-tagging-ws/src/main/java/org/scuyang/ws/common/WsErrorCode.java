package org.scuyang.ws.common;

import com.tove.web.infra.common.BaseError;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum WsErrorCode implements BaseError {

    TOKEN_ILLEGAL("30001", "token error"),
    LOCK_IMAGE_FAIL("30002", "lock image fail"),
    UNLOCK_IMAGE_FAIL("30003", "unlock image fail"),
    // 参数错误
    PARAM_ILLEGAL("30004", "param error"),
    ;

    private final String code;
    private final String msg;

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }
}
