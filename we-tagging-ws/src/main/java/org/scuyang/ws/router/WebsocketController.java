package org.scuyang.ws.router;

import com.tove.web.infra.common.BaseErrorCode;
import com.tove.web.infra.common.BaseException;
import com.tove.web.infra.common.WsResponse;
import lombok.extern.slf4j.Slf4j;
import org.scuyang.common.ws.domain.WsArgBase;
import org.scuyang.core.RedisService;
import org.scuyang.rest.api.req.auth.UserInfo;
import org.scuyang.ws.WebSocketSupport;
import org.scuyang.ws.service.MsgService;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

@Slf4j
@ServerEndpoint(value = "/we-tagging-ws/{uid}")
@Component
public class WebsocketController {

    private static RedisService redisService;
    private static MsgService msgService;

    @Resource
    public void setRedisService(RedisService redisService) {
        WebsocketController.redisService = redisService;
    }

    @Resource
    public void setMsgService(MsgService msgService) {
        WebsocketController.msgService = msgService;
    }

    @OnOpen
    public void onOpen(Session session, @PathParam("uid") Long uid) {
        if (uid == null){
            log.error("uid not null");
        }
        log.info("onOpen uid: {}", uid);
        UserInfo userInfo = redisService.getOnlineUser(uid);
        if (userInfo == null){
            log.error("uid not null");
        }
        WebSocketSupport.storageSession(uid, session);
        log.info("ws open session: {}, uid:{}", session.getId(), uid);

        Session session1 = WebSocketSupport.getSession(String.valueOf(uid));
        log.info("ws open session: {}, uid:{}", session1.getId(), uid);
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session) {
        System.out.println("session close. ID:" + session.getId());

        WebSocketSupport.releaseSession(session);
        // redisService.clearUserAllImage(WebSocketSupport.getUid(session));
    }

    /**
     * 收到客户端消息后调用的方法
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        if("ping".equals(message.toLowerCase())) {
            WebSocketSupport.pushPong(session);
            return;
        }
        if (!StringUtils.hasLength(message)){
            log.info("message is empty");
            WebSocketSupport.push(session, WsResponse.getFail(BaseErrorCode.ILLEGAL_PARAMETERS));
            return;
        }
        System.out.println("receive message: " + message);
        try {
            WsArgBase arg = WsArgBase.getMsgType(message);
            arg.setUid(WebSocketSupport.getUid(session));
            msgService.MsgHandler(arg);
        }catch (BaseException e){
            log.warn("error: {}", e.getMsg());
            WebSocketSupport.push(session, WsResponse.getFail(e));
        }
    }

    /**
     * Throwable error: 不能改变异常的类型，否则会导致服务器无法捕获异常
     * 发生错误时调用
     */
    @OnError
    public void onError(Session session, Throwable error) {
        error.printStackTrace();
        if (error instanceof BaseException){
            BaseException baseException = (BaseException) error;
            WebSocketSupport.push(session, WsResponse.getFail(baseException));
        } else {
            WebSocketSupport.push(session, WsResponse.getFail(BaseErrorCode.SYSTEM_BUSY));
        }
    }

}
