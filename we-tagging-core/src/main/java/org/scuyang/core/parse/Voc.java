package org.scuyang.core.parse;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;

@Data
@JacksonXmlRootElement(localName = "annotation")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Voc {
    private String folder;
    private String filename;
    private Source source;
    private Size size;

    @JacksonXmlElementWrapper(useWrapping = false, localName = "object")
    private List<Annotation> object;

    public static void main(String[] args) {
        try {
            InputStream input = new FileInputStream("C:\\Users\\zjianfa\\Desktop\\Annotations\\image3.xml");
            JacksonXmlModule module = new JacksonXmlModule();
            XmlMapper mapper = new XmlMapper(module);
            Voc book = mapper.readValue(input, Voc.class);
            System.out.println(book);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Data
    static class Source{
        private String database;
        private String annotation;
        private String image;
    }


    @Data
    public static class Size{
        private BigDecimal width;
        private BigDecimal height;
        private BigDecimal depth;
    }


    @Data
    public static class Annotation{
        private String name;
        private String pose;
        private String truncated;
        private String difficult;
        private Bndbox bndbox;
    }


    @Data
    public static class Bndbox{
        private BigDecimal xmin;
        private BigDecimal ymin;
        private BigDecimal xmax;
        private BigDecimal ymax;
    }
}
