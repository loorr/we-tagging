package org.scuyang.core.parse;

import lombok.Builder;
import lombok.Data;
import org.w3c.dom.Element;

import java.math.BigDecimal;

public class VocUtil {

    public static Element convertToElement(String vocName, String vocValue) {
        //Element element = new Element(vocName);
        //element.setTextContent(vocValue);
       // return element;
        return null;
    }

    @Builder
    @Data
    static class Annotation{
        private String name;
        private String pose;
        private String truncated;
        private String difficult;
        private Bndbox bndbox;
    }

    @Builder
    @Data
    static class Bndbox{
        private BigDecimal xmin;
        private BigDecimal ymin;
        private BigDecimal xmax;
        private BigDecimal ymax;
    }
}
