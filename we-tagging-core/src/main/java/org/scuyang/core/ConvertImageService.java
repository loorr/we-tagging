package org.scuyang.core;

public interface ConvertImageService {

    /**
     * 批量存储图片信息，从currFolder 拷贝到 rootFolder+imageFolder
     * @param rootFolder oss根目录
     * @param imageFolder 图片相对于根目录位置
     * @param currFolder 图片文件夹现在的位置
     */
    void convertImage(String rootFolder, String imageFolder, String currFolder);

}
