package org.scuyang.core.oss;

import java.io.IOException;

public interface FileManageService {
    void addBatchImagesByZip(String tempFilePath, String outPath) throws IOException;
}
