package org.scuyang.core.oss;

import org.scuyang.oss.api.req.*;
import org.scuyang.oss.api.vo.ExportLabelAndImageVo;
import org.scuyang.oss.api.vo.ImageAccessVo;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.IOException;

public interface JobService {

    Boolean importLocalImageFolder(ImportLocalFolderReq req);

    Boolean importImageFolder(ImportImageFolderJobReq req) throws IOException;

    ImageAccessVo addNewImage(File file, String version, String group) throws IOException;

    ExportLabelAndImageVo exportLabelAndImage(ExportLabelAndImageReq req);

    void batchImageRecognize(BatchImageRecognizeReq req);

    void singleImageRecognize(SingleImageRecognizeReq req);

    void imageRecognize(SingleImageRecognizeReq req);
}
