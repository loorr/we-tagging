package org.scuyang.core.oss;

import org.scuyang.oss.api.req.SingleImageRecognizeReq;

public interface ImportService {
    /** 文件夹 */
    void importImageLabelFromTxt(String path);

    void importSignleLabelTxt(SingleImageRecognizeReq req, String path);
}
