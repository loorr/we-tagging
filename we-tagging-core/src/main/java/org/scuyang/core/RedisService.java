package org.scuyang.core;

import com.alibaba.fastjson.JSON;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.time.DateUtils;
import org.redisson.api.*;
import org.scuyang.common.ws.domain.Do.UserImage;
import org.scuyang.core.constant.RedisConstant;
import org.scuyang.rest.api.req.auth.UserInfo;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Service
public class RedisService {

    @Resource
    private RedissonClient redissonClient;

    public void saveOnlineUser(UserInfo userInfo){
        RBucket rBucket = redissonClient.getBucket(String.format(RedisConstant.ONLINE_USER_UID, userInfo.getUid()));
        rBucket.set(JSON.toJSONString(userInfo));
    }

    public UserInfo getOnlineUser(Long uid){
        RBucket rBucket = redissonClient.getBucket(String.format(RedisConstant.ONLINE_USER_UID, uid));
        String data = (String) rBucket.get();
        // System.out.println(data);
        return JSON.parseObject(data, UserInfo.class);
    }

    public void removeOnlineUser(Long uid){
        RBucket rBucket = redissonClient.getBucket(String.format(RedisConstant.ONLINE_USER_UID, uid));
        rBucket.delete();
    }

    public Set<String> popAllSingleImageUser(String imgId){
        RSet<String> rSet = redissonClient.getSet(String.format(RedisConstant.IMAGE_ID_SET, imgId));
        if (!rSet.isExists()){
            return new HashSet<>(0);
        }
        Set<String> uids = rSet.readAll();
        rSet.delete();
        return uids;
    }

    public void addUserImage(Long imgId, Long uid){
        RMap<Long, String> map = redissonClient.getMap(RedisConstant.USER_LOCK_IMAGES);
        UserImage userImage = new UserImage();
        userImage.setDate(new Date());
        userImage.setUid(uid);
        userImage.setImageId(imgId);
        map.put(imgId, userImage.toString());
    }

    public void removeUserImage(Long imgId){
       RMap<Long, String> map = redissonClient.getMap(RedisConstant.USER_LOCK_IMAGES);
       map.remove(imgId);
    }

    public void clearUserAllImage(Long uid){
        RMap<Long,String> map = redissonClient.getMap(RedisConstant.USER_LOCK_IMAGES);
        for (String data : map.values()){
            if (!StringUtils.hasLength(data)){
                continue;
            }
            UserImage userImage = JSON.parseObject(data, UserImage.class);
            if (userImage.getUid().equals(uid)){
                map.remove(userImage.getImageId());
            }
            if (new Date().getTime() - userImage.getDate().getTime() > 1000 * 60 * 20){
                map.remove(userImage.getImageId());
            }
        }
    }

    public HashMap<Long, UserImage> getAllLockedImageIds(){
        RMap<Long,String> map = redissonClient.getMap(RedisConstant.USER_LOCK_IMAGES);
        HashMap<Long, String> data = (HashMap<Long, String>) map.readAllMap();
        HashMap<Long, UserImage> result = new HashMap<>(data.size());
        for (String value : data.values()){
            if (!StringUtils.hasLength(value)){
                continue;
            }
            UserImage userImage = JSON.parseObject(value, UserImage.class);
            result.put(userImage.getImageId(), userImage);
        }

        return result;
    }
}
