package org.scuyang.core.config;

import org.scuyang.common.model.AppConfigModel;
import org.scuyang.dao.AppConfigMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

@Configuration
public class AppConfig {

    @Value("${tagging.env:dev}")
    public String env;

    @Value("${tagging.auto-refresh-app-config:false}")
    private String autoRefreshAppConfig;
    @Resource
    private AppConfigMapper appConfigMapper;

    private HashMap<String, AppConfigModel> appConfigMap = new HashMap<>();

    @Scheduled(fixedDelay = 1000 * 60)
    public void refreshAppConfig() {
        if (!autoRefreshAppConfig.equals("true")) {
            return;
        }
        List<AppConfigModel> appConfigModels = appConfigMapper.selectAll();
        synchronized (appConfigMap) {
            appConfigMap.clear();
            for (AppConfigModel appConfigModel : appConfigModels) {
                appConfigMap.put(appConfigModel.getKey(), appConfigModel);
            }
        }
    }

    public AppConfigModel getAppConfig(String key) {
        if (appConfigMap == null || appConfigMap.isEmpty()) {
            return null;
        }
        return appConfigMap.get(key);
    }
}
