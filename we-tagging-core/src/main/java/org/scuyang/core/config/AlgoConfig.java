package org.scuyang.core.config;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RList;
import org.redisson.api.RedissonClient;
import org.scu.yang.algo.AlgoContext;
import org.scu.yang.algo.LabelItemVo;
import org.scu.yang.algo.Reason;
import org.scu.yang.algo.YoloV5Model;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.File;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;

@Slf4j
@Component
public class AlgoConfig {
    @Value("${we-tagging.algo-config.model-config:}")
    private String modelFolder;
    @Value("${we-tagging.algo-config.enableAlgo:false}")
    private String enableAlgo;

    @Resource
    private RedissonClient redissonClient;

    private final HashMap<String, Reason> reasonMap = new HashMap<>();
    private final HashMap<String, AlgoContext> algoContextMap = new HashMap<>();

    public AlgoConfig() {
    }

    @PostConstruct
    public void init() {
        log.info("AlgoConfig init start");
        loadConfig();
        log.info("AlgoConfig init end");
    }

    public void loadConfig() {
        if(!enableAlgo.equals("true")){
            log.warn("Algo is not enabled, skip loading model config");
            return;
        }
        List<AlgoContext> algoContext = readAlgoContextFromFile();

        // List<AlgoContext> algoContext = readAlgoContextFromRedis();
        if (CollectionUtils.isEmpty(algoContext)) {
            log.warn("No model config is loaded");
            return;
        }
        log.info("AlgoConfig load model config success: ", algoContext.size());
        loadModels(algoContext);
    }

    private List<AlgoContext> readAlgoContextFromRedis(){
        RList<AlgoContext> rList = redissonClient.getList("we-tagging:algo-config:model-config");
        List<AlgoContext> contexts = rList.readAll();
        return contexts;
    }

    private List<AlgoContext> readAlgoContextFromFile(){
        if (modelFolder == null || modelFolder.isEmpty()) {
            log.warn("No model folder is configured, skip loading model config");
            return null;
        }
        File file = new File(modelFolder);
        byte[] bytes = new byte[]{};
        try {
            bytes = Files.readAllBytes(file.toPath());
        } catch (Exception e) {
            log.error("AlgoConfig read model config error", e);
        }
        String jsonString = new String(bytes);
        List<AlgoContext> algoContext = JSON.parseArray(jsonString, AlgoContext.class);
        return algoContext;
    }


    private void loadModels(List<AlgoContext> contexts){
        RList<AlgoContext> rList = redissonClient.getList("we-tagging:algo-config:model-config");

        for (AlgoContext context : contexts) {
            if (!context.isEnable()){
                continue;
            }
            String mappingPath = context.getClassMappingFile();
            context.setClassMapping(readJson(mappingPath));
            rList.add(context);
            Reason reason = new YoloV5Model(context);
            synchronized (reasonMap) {
                reasonMap.put(context.getModelCode(), reason);
            }
            synchronized (algoContextMap) {
                algoContextMap.put(context.getModelCode(), context);
            }
        }
    }

    private HashMap<String, LabelItemVo> readJson(String path){
        byte[] bytes = new byte[]{};
        try {
            bytes = Files.readAllBytes(new File(path).toPath());
        } catch (Exception e) {
            log.error("AlgoConfig read model config error", e);
        }
        String jsonString = new String(bytes);
        return JSON.parseObject(jsonString, new TypeReference<HashMap<String, LabelItemVo>>(){});
    }

    public Reason getAlgoContext(String modelCode) {
        return reasonMap.get(modelCode);
    }

    public AlgoContext getAlgoContextByModelCode(String modelCode) {
        return algoContextMap.get(modelCode);
    }
}
