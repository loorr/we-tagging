package org.scuyang.core;

import org.scuyang.common.model.RoleImageVersion;
import org.scuyang.dao.RoleMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class RoleService {

    @Resource
    private RoleMapper roleMapper;

    public Set<String> getRoleImageVersion(List<String> roles) {
        List<RoleImageVersion> roleImageVersions = roleMapper.getRoleImageVersion(roles);
        Set<String> imageVersions = new HashSet<>();
        for (RoleImageVersion roleImageVersion : roleImageVersions) {
            imageVersions.add(roleImageVersion.getVersion());
        }
        return imageVersions;
    }

}
