package org.scuyang.core;

import com.tove.web.infra.common.PageHelperUtil;
import com.tove.web.infra.common.PageResult;
import org.scuyang.oss.api.req.GetImageLabelPageAdminReq;
import org.scuyang.oss.api.vo.ImageLabelVo;
import org.scuyang.rest.api.req.*;
import org.scuyang.rest.api.req.label.GetImagePageReq;
import org.scuyang.rest.api.vo.ImageGroupNoteVo;
import org.scuyang.rest.api.vo.tag.ImageInfoVo;
import org.scuyang.rest.api.vo.ImageLabelInfoVo;

public interface TagService {

    Boolean saveTagInfo(SaveTagInfoReq req);

    Boolean saveAllTagInfo(SaveAllTageInfoReq req);

    Boolean deleteTagInfo(DeleteTagInfoReq req);

    PageHelperUtil<ImageInfoVo> getImageInfoPage(GetImagePageReq req);

    ImageLabelInfoVo getImageLabelInfo(GetImageLabelInfoReq req);

    Boolean confirmImageHaveTag(ConfirmImageHaveTagReq req);

    Boolean addImageGroupNote(AddImageGroupNoteReq req);

    ImageGroupNoteVo getImageGroupNote(GetImageGroupNoteReq req);

    PageResult<ImageLabelVo> getImageLabelPage(GetImageLabelPageAdminReq req);
}
