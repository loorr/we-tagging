package org.scuyang.core;

public interface CosService {
    com.tencent.cloud.Response getTmpSecretToken();

    void downloadFile(String outputFilePath, String fileKey);
}
