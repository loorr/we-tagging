package org.scuyang.core.impl;

import org.scuyang.common.enums.TaskNameEnum;
import org.scuyang.common.enums.TaskStateEnum;
import org.scuyang.common.model.AsynTask;
import org.scuyang.core.AsynTaskService;
import org.scuyang.dao.AsynTaskMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class AsynTaskServiceImpl implements AsynTaskService {

    @Resource
    private AsynTaskMapper asynTaskMapper;

    @Override
    public void addTask(AsynTask asynTask) {
        asynTaskMapper.addTask(asynTask);
    }

    @Override
    public void updateTask(AsynTask asynTask) {
        asynTask.setTaskEndTime(new Date());
        asynTaskMapper.updateTask(asynTask);
    }

    @Override
    public void startNewTask(TaskNameEnum taskNameEnum) {
        AsynTask task = AsynTask.getTaskStartInstance(taskNameEnum);
        asynTaskMapper.addTask(task);
    }

    @Override
    public AsynTask getTask(Integer id) {
        return asynTaskMapper.getTask(id);
    }

    @Override
    public List<AsynTask> getTaskList(TaskNameEnum taskNameEnum, TaskStateEnum taskStateEnum) {
        return asynTaskMapper.getTaskList(taskNameEnum.getCode(), taskStateEnum.getCode());
    }

    @Override
    public void getTaskState(Integer id) {
        return;
    }

}
