package org.scuyang.core.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IORuntimeException;
import cn.hutool.core.util.ZipUtil;
import lombok.extern.slf4j.Slf4j;
import org.scuyang.core.oss.FileManageService;
import org.scuyang.core.oss.JobService;
import org.scuyang.oss.api.error.OssErrorCode;
import org.scuyang.oss.api.req.ImportImageFolderJobReq;
import org.scuyang.rest.api.error.WeTagErrorCode;
import org.scuyang.rest.api.error.WeTagException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;

@Slf4j
@Service
public class FileManageServiceImpl implements FileManageService {

    @Resource
    private JobService jobService;


    @Override
    public void addBatchImagesByZip(String tempFilePath, String outPath) throws IOException {
        try {
            ZipUtil.unzip(tempFilePath, outPath);
        }catch (IORuntimeException e){
            throw new WeTagException(OssErrorCode.CAN_NOT_OPEN_ZIP);
        }
        String unZipPath = outPath  + getFileName(tempFilePath);
        File[] groupDirs = FileUtil.ls(unZipPath);
        String imgVersion = getFileName(tempFilePath);
        try{
            for (File file: groupDirs){
                ImportImageFolderJobReq req = new ImportImageFolderJobReq();
                req.setSourcePath(file.getPath());
                req.setVersion(imgVersion);
                req.setGroup(file.getName());
                jobService.importImageFolder(req);
                log.info(imgVersion + " "+ file.getName());
            }
        }catch (DuplicateKeyException e){
            throw new WeTagException(WeTagErrorCode.DUPLICATE_KEY);
        }finally {
            FileUtil.del(tempFilePath);
            FileUtil.del(unZipPath);
        }
    }

    /**
     * 从文件路径中，获取不带后缀的文件名
     * @param fileName 文件路径
     * @return
     */
    private String getFileName(String fileName){
        // TODO
        String[] arr = new String[10]; //FileUtil.getName(fileName).split("\\.");
        return arr==null&&arr.length<2 ? null : arr[0];
    }
}
