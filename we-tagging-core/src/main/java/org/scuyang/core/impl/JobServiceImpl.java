package org.scuyang.core.impl;


import ai.djl.modality.Classifications;
import ai.djl.modality.cv.output.BoundingBox;
import ai.djl.modality.cv.output.DetectedObjects;
import ai.djl.modality.cv.output.Rectangle;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.tove.web.infra.common.BaseErrorCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.scu.yang.algo.AlgoContext;
import org.scu.yang.algo.LabelItemVo;
import org.scu.yang.algo.Reason;
import org.scuyang.common.model.*;
import org.scuyang.common.util.FileClassType;
import org.scuyang.common.util.FileTypeJudge;
import org.scuyang.common.util.FileUtil;
import org.scuyang.common.ws.WsMsgDO;
import org.scuyang.common.ws.WsMsgType;
import org.scuyang.core.ModelConfigService;
import org.scuyang.core.RedisService;
import org.scuyang.core.config.AlgoConfig;
import org.scuyang.core.mq.RedisQueueMsgProducer;
import org.scuyang.core.oss.ImportService;
import org.scuyang.core.oss.JobService;
import org.scuyang.dao.ImageGroupInfoMapper;
import org.scuyang.dao.ImageInfoMapper;
import org.scuyang.dao.LabelInfoMapper;
import org.scuyang.oss.api.error.OssErrorCode;
import org.scuyang.oss.api.error.OssException;
import org.scuyang.oss.api.req.*;
import org.scuyang.oss.api.vo.ExportLabelAndImageVo;

import org.scuyang.oss.api.vo.ImageAccessVo;

import org.scuyang.rest.api.error.WeTagErrorCode;
import org.scuyang.rest.api.error.WeTagException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.lang.Thread.sleep;

@Log4j2
@Service
public class JobServiceImpl implements JobService {
    private static String BASE_URL = "http://127.0.0.1:5000";


    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    private static AtomicBoolean atomicBoolean;

    @Value("${oss.root.folder}")
    private String rootFolder;

    @Value("${oss.img.url}")
    private String imgUrlPrefix;

    @Value("${app-env:dev}")
    private String appEnv;

    @Resource
    private RootManager rootManager;

    @Resource
    private ImageInfoMapper imageInfoMapper;

    @Resource
    private LabelInfoMapper labelInfoMapper;

    @Resource
    private ImportService importService;

    @Resource
    private ImageGroupInfoMapper imageGroupInfoMapper;

    @Resource
    private ModelConfigService modelConfigService;

    @Resource
    private AlgoConfig algoConfig;

    @Resource
    private RedisService redisService;

    @Resource
    private RedisQueueMsgProducer redisQueueMsgProducer;



    @Override
    public Boolean importLocalImageFolder(ImportLocalFolderReq req) {
        File file = new File(req.getSourcePath());
        if (!file.exists() || !file.isDirectory()){
            throw new OssException(OssErrorCode.NO_TARGET_FOLDER);
        }
        List<File> fileList = (List<File>) FileUtils.listFiles(new File(req.getSourcePath()), null, false);
        if (CollectionUtils.isEmpty(fileList)){
            throw new OssException(OssErrorCode.NO_TARGET_IMAGE);
        }

        return null;
    }

    @Override
    public Boolean importImageFolder(ImportImageFolderJobReq req) throws IOException {
        if (!FileUtil.isDir(req.getSourcePath())){
            throw new OssException(OssErrorCode.NO_TARGET_FOLDER);
        }
        List<File> fileList = (List<File>) FileUtils.listFiles(new File(req.getSourcePath()), null, false);
        if (CollectionUtils.isEmpty(fileList)){
            throw new OssException(OssErrorCode.NO_FILES_IN_TARGET_FOLDER);
        }
        for(File file: fileList){
            log.info("import file: {}", file.getAbsolutePath());
            if (file.getName().equals("remark.txt")){
                addImageGroupRemark(file, req.getVersion(), req.getGroup());
                continue;
            }
            addNewImage(file, req.getVersion(), req.getGroup());
        }
        return true;
    }

    private void addImageGroupRemark(File file, String version, String group){
        try {
            String remark = FileUtils.readFileToString(file, "UTF-8");
            if (!StringUtils.hasLength(remark)){
                return;
            }
            ImageGroupInfo imageGroupInfo = new ImageGroupInfo();
            imageGroupInfo.setImgGroup(group);
            imageGroupInfo.setDescribe(remark);
            imageGroupInfo.setVersion(version);
            imageGroupInfoMapper.insertNote(imageGroupInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ImageAccessVo addNewImage(File file, String version, String group) throws IOException {
        if (FileClassType.PICS != FileTypeJudge.getClassType(file)){
            log.info("{} The file is not a picture file, not managed by the system.",file.getName());
            throw new OssException(BaseErrorCode.ILLEGAL_PARAMETERS);
        }
        ImageAccessVo imageAccessVo = new ImageAccessVo();
        String md5 = FileUtil.getFileMD5(file);
        String postfix = getFilePostfix(file);
        Long imgSize = file.length();
        BufferedImage sourceImg = ImageIO.read(new FileInputStream(file));
        Long imgLength = Integer.toUnsignedLong(sourceImg.getHeight());
        Long imgWidth =  Integer.toUnsignedLong(sourceImg.getWidth());

        String uri = getImageUrl(version, group, md5, postfix);
        ImageInfo imageInfo = ImageInfo.builder()
                .md5(md5)
                .version(version)
                .uri(uri)
                .imgGroup(group)
                .imgSrcName(file.getName())
                .imgSize(imgSize)
                .imgLength(imgLength)
                .imgWidth(imgWidth)
                .labelState(false)
                .folder("/test")
                .build();

        try {
            imageInfoMapper.insert(imageInfo);
        }catch (DuplicateKeyException e){
            log.error("{} The image is already exist.",file.getName());
            throw new OssException(WeTagErrorCode.DUPLICATE_KEY);
        }
        File newFile = new File(rootFolder, uri);
        if (!newFile.exists()){
            FileUtil.copyFile(file, new File(rootFolder, uri));
        }
        log.info("copy file: {} ", file.getPath());
        log.info("Successfully imported image. {}", imageInfo);
        imageAccessVo.setImgSrcName(file.getName());
        imageAccessVo.setMd5(md5);
        imageAccessVo.setImgUrl(imageInfo.getUri());
        return imageAccessVo;
    }

    @Override
    public ExportLabelAndImageVo exportLabelAndImage(ExportLabelAndImageReq req) {
        List<ImageLabel> imageLabels = labelInfoMapper.getImageLabelListByVersion(req.getVersion());

        return null;
    }

    @Override
    public void batchImageRecognize(BatchImageRecognizeReq req) {
        String uuid = UUID.randomUUID().toString();
        List<BatchImageRecognizeReq> recognizeReqList = new ArrayList<>();
        if (StringUtils.hasLength(req.getVersionFolderPath())){
            List<File> fileList = FileUtil.listFolderFiles(req.getVersionFolderPath(),true);
            for (File file: fileList){
                String groupName = file.getName();
                String groupFolderPath = file.getAbsolutePath();
                log.info("绝对路径: {} ", groupFolderPath);
                BatchImageRecognizeReq reqItem = new BatchImageRecognizeReq();
                reqItem.setGroupName(groupName);
                reqItem.setGroupFolderPath(groupFolderPath);
                recognizeReqList.add(reqItem);
                log.info(reqItem.toString());
            }
        }else{
            recognizeReqList.add(req);
        }
        sendBatchRecognizeReq(recognizeReqList);
    }

    @SneakyThrows
    @Async(value = "taskExecutor2")
    public void sendBatchRecognizeReq(List<BatchImageRecognizeReq> recognizeReqList){
        atomicBoolean = new AtomicBoolean(true);
        int index = 0;
        while (true){
            if (!atomicBoolean.get()){
                sleep(1*1000);
                continue;
            }
            if (index >= recognizeReqList.size()) {
                return;
            }
            atomicBoolean.getAndSet(false);
            log.info("批量识别: {}", recognizeReqList.get(index).toString());
            sendBatchRecognizeReq(recognizeReqList.get(index));
            index ++;
        }
    }

    public void sendBatchRecognizeReq(BatchImageRecognizeReq req){
        String uuid = UUID.randomUUID().toString();
        BatchSegImgReq batchSegImgReq = new BatchSegImgReq();
        batchSegImgReq.setUuid(uuid);
        batchSegImgReq.setFolder_path(req.getGroupFolderPath());
        batchSegImgReq.setEnv(appEnv);

        WebClient client = WebClient.create();
        Flux<Map> mapFlux = client
                .post()
                .uri(BASE_URL + req.getUri())
                .contentType(MediaType.APPLICATION_JSON)
                 .body(BodyInserters.fromObject(JSONUtil.toJsonStr(batchSegImgReq)))
                .retrieve() // 获取响应体
                .bodyToFlux(Map.class); // 响应数据类型转换
        // 异步执行的方法，在里面可以执行http响应后需要执行的后续操作
        mapFlux.subscribe(map -> {
            // 文件夹
            String labelFile = (String) map.get("label_file");
            if (labelFile == null){
                log.warn("is null.");
            }
            log.info("标签文件夹: {}", labelFile);
            atomicBoolean.getAndSet(true);
            importService.importImageLabelFromTxt(labelFile);
        });
    }


    @Override
    public void singleImageRecognize(SingleImageRecognizeReq req) {
        log.info("singleImageRecognize: {}", req);
        String uuid = UUID.randomUUID().toString();

        ImageInfo imageInfo = imageInfoMapper.getImageInfo(req.getImgId());
        if (imageInfo == null){
            throw new WeTagException(WeTagErrorCode.NO_IMAGE);
        }
        String newUri = imageInfo.getUri().replace("//","/");
        String root = "/home/image_root/ossRoot";
        req.setImageName(imageInfo.getMd5());
        req.setImagePath(root + newUri);

        SoloSegImgReq soloSegImgReq = new SoloSegImgReq(req.getImagePath(), req.getImageName(),uuid, appEnv);
        log.info("SoloSegImgReq: {}", soloSegImgReq);

        WebClient client = WebClient.create();
        Flux<Map> mapFlux = client
                .post()
                .uri(BASE_URL + req.getUri())
                .contentType(MediaType.APPLICATION_JSON)
                // TODO
                .body(BodyInserters.fromObject(JSON.toJSON(soloSegImgReq)))
                .retrieve() // 获取响应体
                .bodyToFlux(Map.class); // 响应数据类型转换

        // 异步执行的方法，在里面可以执行http响应后需要执行的后续操作
        mapFlux.subscribe(map -> {
            String labelFile = (String) map.get("label_file");
            log.info(labelFile);
            importService.importSignleLabelTxt(req, labelFile);
        });
    }


    @Override
    public void imageRecognize(SingleImageRecognizeReq req) {
        ImageInfo imageInfo = imageInfoMapper.getImageInfo(req.getImgId());
        if (imageInfo == null){
            throw new WeTagException(WeTagErrorCode.NO_IMAGE);
        }
        ModelConfig modelConfigs = modelConfigService.getModelConfigByCode(imageInfo.getVersion(), req.getModelCode());
        if (modelConfigs == null){
            throw new WeTagException(WeTagErrorCode.NO_MODEL_CONFIG);
        }
        String newUri = imageInfo.getUri().replace("//","/");
        String root = "/home/image_root/ossRoot";

        try {
            Reason model = algoConfig.getAlgoContext(req.getModelCode());
            if (model == null){
                sendLabelResult(req.getImgId(), null);
                log.warn("model is null. {}", req.getModelCode());
            }

            DetectedObjects result = model.detectObjects(root + newUri);
            if (result == null){
                sendLabelResult(req.getImgId(), null);
                throw new WeTagException(WeTagErrorCode.NO_MODEL_CONFIG);
            }
            List<ImageLabelAlgorithm> labels = transformAlgoLabelList(req.getImgId(), result, modelConfigs);
            if (!CollectionUtils.isEmpty(labels)){
                executorService.submit(()->{
                    labelInfoMapper.deleteNewAlgorithmLabel(req.getImgId(), req.getModelCode(), modelConfigs.getModelVersion());
                    labelInfoMapper.batchInsertAlgoLabels(labels);
                });
                log.info("导入标签完成 数量: {} ", labels.size());
            }
            sendLabelResult(req.getImgId(), labels);
        }catch (Exception e){
            sendLabelResult(req.getImgId(), null);
            log.error("识别异常: {}", e);
        }
    }

    private void sendLabelResult(Long imageId, List<ImageLabelAlgorithm> labels){
        Set<String> uids = redisService.popAllSingleImageUser(String.valueOf(imageId));
        if (CollectionUtils.isEmpty(uids)){
            log.info("没有可发送的对象");
            return;
        }
        WsMsgDO<String> wsMsgDO;
        if (CollectionUtils.isEmpty(labels)){
            wsMsgDO =new WsMsgDO<>(WsMsgType.SINGLE_IMAGE, "");
        }else{
            wsMsgDO =new WsMsgDO<>(WsMsgType.SINGLE_IMAGE, JSON.toJSONString(labels));
        }
        wsMsgDO.setUids(uids);
        String data = JSON.toJSONString(wsMsgDO);
        redisQueueMsgProducer.sendData(data);
    }

    private List<ImageLabelAlgorithm> transformAlgoLabelList(Long imageId,DetectedObjects data, ModelConfig modelConfig){
        List<ImageLabelAlgorithm> imageLabelList = new ArrayList<>(data.items().size());
        AlgoContext context = algoConfig.getAlgoContextByModelCode(modelConfig.getModelCode());
        HashMap<String, LabelItemVo> classMapping  = context.getClassMapping();
        Date now = new Date();
        for (Classifications.Classification item :data.items()) {
            ImageLabelAlgorithm label = new ImageLabelAlgorithm();

            DetectedObjects.DetectedObject obj = (DetectedObjects.DetectedObject) item;
            BoundingBox box = obj.getBoundingBox();
            Rectangle rec = box.getBounds();
            label.setImgId(imageId);
            label.setModelVersion(modelConfig.getModelVersion());
            label.setModelCode(modelConfig.getModelCode());
            label.setScore(new BigDecimal(String.valueOf(item.getProbability())));
            label.setHeight(BigDecimal.valueOf(rec.getHeight()));
            label.setWidth(BigDecimal.valueOf(rec.getWidth()));
            label.setCoordinateX(BigDecimal.valueOf(rec.getX()));
            label.setCoordinateY(BigDecimal.valueOf(rec.getY()));
            LabelItemVo labelItemVo = classMapping.get(item.getClassName());
            if (labelItemVo != null){
                label.setCategoryId(labelItemVo.getCategoryId());
                label.setClassId(labelItemVo.getClassId());
            }
            label.setImgModifyUsername("admin");
            label.setTime(now);
            imageLabelList.add(label);
        }
        return imageLabelList;
    }

    public Boolean removeImageByVersionAndGroup(String version, String group){
        List<ImageInfo> imageInfoList = imageInfoMapper.selectListByVersionAndGroup(group,version);
        if (CollectionUtils.isEmpty(imageInfoList)){
            throw new OssException(OssErrorCode.NO_TARGET_FOLDER);
        }
        //int rows = imageInfoMapper.deleteByVersionAndGroup(group,version);
//        if (rows == 0){
//            throw new OssException(OssErrorCode.OPERATION_INVALID);
//        }
        return true;
    }

    private String getImageUrl(String version, String group, String md5, String postfix){
        return String.format("/%s/%s/%s.%s", version, group, md5, postfix);
    }

    private String getFilePostfix(File file){
        String fileName = file.getName();
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }
}

@Data
@AllArgsConstructor
class SoloSegImgReq{
    private String image_path;
    private String image_name;
    private String uuid;
    private String env = "dev";
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class BatchSegImgReq{
    private String folder_path;

    @Deprecated
    private String folder_name;
    private String uuid;
    private String env = "dev";
}
