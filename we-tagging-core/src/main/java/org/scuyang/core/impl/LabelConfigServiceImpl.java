package org.scuyang.core.impl;

import cn.dev33.satoken.stp.StpUtil;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.tove.web.infra.common.BaseErrorCode;
import com.tove.web.infra.common.PageResult;
import org.scuyang.common.model.*;
import org.scuyang.core.LabelConfigService;
import org.scuyang.core.RoleService;
import org.scuyang.core.constant.CommonConstant;
import org.scuyang.dao.*;
import org.scuyang.oss.api.req.AddOrUpdateLabelAdminReq;
import org.scuyang.oss.api.req.DeleteLabelAdminReq;
import org.scuyang.oss.api.req.GetLabelInfoPageAdminReq;
import org.scuyang.oss.api.vo.LabelConfigOssVo;
import org.scuyang.rest.api.error.WeTagErrorCode;
import org.scuyang.rest.api.error.WeTagException;
import org.scuyang.rest.api.req.AddOrUpdateLabelReq;
import org.scuyang.rest.api.req.DeleteLabelReq;
import org.scuyang.rest.api.req.GetLabelConfigReq;
import org.scuyang.rest.api.req.config.AddOrUpdateCategoryLabelReq;
import org.scuyang.rest.api.req.feature.DeleteImageFeatureConfigReq;
import org.scuyang.rest.api.req.image.GetImageGroupsReq;
import org.scuyang.rest.api.req.label.*;
import org.scuyang.rest.api.vo.*;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class LabelConfigServiceImpl implements LabelConfigService {

    @Resource
    private LabelConfigMapper labelConfigMapper;

    @Resource
    private ImageInfoMapper imageInfoMapper;

    @Resource
    private LabelConfigSetMapper labelConfigSetMapper;

    @Resource
    private UserLabelConfigMapper userLabelConfigMapper;

    @Resource
    private RoleService roleService;

    @Resource
    private ImageFeatureMapper imageFeatureMapper;

    @Resource
    private LabelInfoMapper labelInfoMapper;

    // 缓存线程池
    private final ExecutorService executorService = Executors.newFixedThreadPool(4);

    private LoadingCache<String, List<ImageInfo>> imageInfoCache = Caffeine.newBuilder()
            .maximumSize(20)
            .expireAfterWrite(4, TimeUnit.HOURS)
            .build(key -> imageInfoMapper.getImageInfoByVersion(Collections.singleton(key)));

    @Override
    public LabelConfigVo getLabelConfig(GetLabelConfigReq req) {
        List<LabelConfig> labelConfigList = labelConfigMapper.getLabelConfigListByVersion(req.getVersion());
        if (CollectionUtils.isEmpty(labelConfigList)){
            throw new WeTagException(WeTagErrorCode.NO_THIS_VERSION);
        }

        LabelConfigVo labelConfigVo = new LabelConfigVo();
        labelConfigVo.setVersion(req.getVersion());

        HashMap<Integer, CategoryItemVo> categoryItemVoMap = new HashMap<>();

        for(LabelConfig item: labelConfigList){
            if (!categoryItemVoMap.containsKey(item.getCategoryId())){
                CategoryItemVo categoryItemVo = new CategoryItemVo();
                categoryItemVo.setId(item.getId());
                categoryItemVo.setCategoryId(item.getCategoryId());
                categoryItemVo.setCategoryName(item.getCategoryName());
                categoryItemVo.setPriority(item.getPriority()== null?0:item.getPriority());
                categoryItemVoMap.put(item.getCategoryId(), categoryItemVo);
            }
            CategoryItemVo categoryItemVo = categoryItemVoMap.get(item.getCategoryId());
            List<ClassItemVo> classItemVoList = categoryItemVo.getClassItemVoList();
            if (CollectionUtils.isEmpty(classItemVoList)){
                classItemVoList = new ArrayList<>();
            }
            if (!CommonConstant.CONFIG_SIGN.equals(item.getClassName())){
                ClassItemVo classItemVo = ClassItemVo.builder()
                        .id(item.getId())
                        .classId(item.getClassId())
                        .className(item.getClassName())
                        .abnormalEnum(item.getAbnormalState().name())
                        .shortcutKey(item.getShortcutKey())
                        .color(item.getColor())
                        .remark(item.getRemark())
                        .priority(item.getPriority()== null?0:item.getPriority())
                        .build();
                classItemVoList.add(classItemVo);
            }else{
                // 大类的 color 和快捷键
                categoryItemVo.setColor(item.getColor());
                categoryItemVo.setShortcutKey(item.getShortcutKey());
                categoryItemVo.setRemark(item.getRemark());
                categoryItemVo.setPriority(item.getPriority()== null?0:item.getPriority());
            }
            categoryItemVo.setClassItemVoList(classItemVoList);
            categoryItemVoMap.put(item.getCategoryId(), categoryItemVo);
        }
        List<CategoryItemVo> categoryItemVoList = new ArrayList<>(categoryItemVoMap.values());
        labelConfigVo.setCategoryItemVoList(categoryItemVoList);
        return labelConfigVo;
    }

    @Override
    public List<LabelVersionVo> getAllLabelVersion(GetAllLabelVersionReq req) {
        if(StringUtils.hasLength(req.getImageVersion())){
            List<LabelSet> labelSetList = labelConfigSetMapper.getAllLabelVersion(req);
            if (CollectionUtils.isEmpty(labelSetList)){
                return new ArrayList<>();
            }
            List<LabelVersionVo> result = labelSetList.stream().map(o->{
                LabelVersionVo labelVersionVo = new LabelVersionVo();
                labelVersionVo.setVersion(o.getName());
                labelVersionVo.setRemark(o.getRemark());
                labelVersionVo.setId(o.getId());
                return labelVersionVo;
            }).collect(Collectors.toList());
            return result;
        }else{
            return labelConfigMapper.getAllVersion();
        }
    }

    @Override
    public List<ImageVersionVo> getAllImageVersion() {
        List<String> userRoles = StpUtil.getRoleList();
        if (CollectionUtils.isEmpty(userRoles)){
            throw new WeTagException(WeTagErrorCode.NO_IMAGES);
        }
        Set<String> userImageVersions = roleService.getRoleImageVersion(userRoles);
        if (CollectionUtils.isEmpty(userImageVersions)){
            throw new WeTagException(WeTagErrorCode.NO_IMAGES);
        }
        List<ImageInfo> imageInfoList = new ArrayList<>();
        for(String version: userImageVersions){
            List<ImageInfo> data = imageInfoCache.get(version);
            imageInfoList.addAll(data);
        }
        // List<ImageInfo> imageInfoList = imageInfoMapper.getImageInfoByVersion(userImageVersions);
        Map<String, ImageVersionVo> versionMap = new HashMap<>();
        Map<String, ImageGroupVo> groupVoMap = new HashMap<>();
        for(ImageInfo imageInfoItem: imageInfoList){
            if (!versionMap.containsKey(imageInfoItem.getVersion())){
                ImageVersionVo imageVersionVo = new ImageVersionVo();
                imageVersionVo.setVersion(imageInfoItem.getVersion());
                versionMap.put(imageInfoItem.getVersion(), imageVersionVo);
            }
            ImageVersionVo imageVersionVo = versionMap.get(imageInfoItem.getVersion());
            imageVersionVo.addTagNum(imageInfoItem.getLabelState()== null?false:imageInfoItem.getLabelState());

            String groupMapKey = imageInfoItem.getVersion() + imageInfoItem.getImgGroup();
            if (!groupVoMap.containsKey(groupMapKey)){
                ImageGroupVo imageGroupVo = new ImageGroupVo();
                imageGroupVo.setGroupName(imageInfoItem.getImgGroup());
                imageVersionVo.getImageGroupVoList().add(imageGroupVo);

                groupVoMap.put(groupMapKey, imageGroupVo);
            }
            ImageGroupVo imageGroupVo = groupVoMap.get(groupMapKey);
            imageGroupVo.addTagNum(imageInfoItem.getLabelState()== null?false:imageInfoItem.getLabelState());
        }
        return new ArrayList<>(versionMap.values());
    }


    @Override
    public List<ImageGroupVo> getImageGroups(GetImageGroupsReq req) {
        checkImageVersionAuth(req.getImageVersion());
        if (!CollectionUtils.isEmpty(req.getGroupFeatureFilter())){
            List<String> imageGroups = imageFeatureMapper.filterImageGroup(req.getImageVersion(), req.getGroupFeatureFilter());
            req.setImageGroups(imageGroups);
        }

        List<String> imageGroups = imageInfoMapper.getImageGroupList(req);
        if (CollectionUtils.isEmpty(imageGroups)){
            return new ArrayList<>();
        }
        List<ImageGroupVo> result = new ArrayList<>();
        for(String imageGroup: imageGroups){
            ImageGroupVo imageGroupVo = new ImageGroupVo();
            imageGroupVo.setGroupName(imageGroup);
            result.add(imageGroupVo);
        }
        return result;
    }

    private void checkImageVersionAuth(String imageVersion){
        List<String> userRoles = StpUtil.getRoleList();
        if (CollectionUtils.isEmpty(userRoles)){
            throw new WeTagException(WeTagErrorCode.NO_IMAGES);
        }
        Set<String> userImageVersions = roleService.getRoleImageVersion(userRoles);
        if (CollectionUtils.isEmpty(userImageVersions) && !userImageVersions.contains(imageVersion)){
            throw new WeTagException(WeTagErrorCode.NO_IMAGES);
        }
    }

    @Transactional
    @Override
    public boolean addLabel(AddOrUpdateLabelReq req) {
        Integer categoryId= labelConfigMapper.existCategoryName(req.getCategoryName(), req.getVersion());
        if (categoryId == null){
            throw new WeTagException(WeTagErrorCode.NO_CATEGORY_LABEL);
        }
        String className = labelConfigMapper.existClassName(req.getClassName(), req.getVersion(), req.getCategoryName());
        if (StringUtils.hasLength(className)){
            throw new WeTagException(WeTagErrorCode.DUPLICATE_KEY);
        }
        req.setCategoryId(categoryId);
        Integer classId = labelConfigMapper.getMaxClassId(req.getVersion()) + 1;
        classId = classId == null?1:classId + 1;
        req.setClassId(classId);
        LabelConfig config = new LabelConfig();
        BeanUtils.copyProperties(req, config);
        int rows = labelConfigMapper.addLabelInfoByModel(config);
        // 给每个用户都添加一个记录
        addUserLabelConfig(config);
        return rows > 0;
    }

    @Transactional
    @Override
    public boolean updateLabel(AddOrUpdateLabelReq req) {
        if (req.getId() == null){
            throw new WeTagException(BaseErrorCode.ILLEGAL_PARAMETERS);
        }
        // 如果要更新大类
        if (StringUtils.hasLength(req.getCategoryName())){
            Integer categoryId= labelConfigMapper.existCategoryName(req.getCategoryName(), req.getVersion());
            if (categoryId == null){
                throw new WeTagException(WeTagErrorCode.NO_RECORD_MATCH);
            }
            req.setCategoryId(categoryId);
        }
        try {
            LabelConfig config = new LabelConfig();
            BeanUtils.copyProperties(req, config);
            int rows = labelConfigMapper.updateClass(config);
            return rows > 0;
        }catch (DuplicateKeyException e){
            throw new WeTagException(WeTagErrorCode.DUPLICATE_KEY);
        }
    }

    @Override
    public boolean deleteLabel(DeleteLabelReq req) {
        if (req.getCategoryId() == null && req.getClassId() == null){
            throw new WeTagException(BaseErrorCode.ILLEGAL_PARAMETERS);
        }
        if (req.getCategoryId() != null && req.getClassId() != null){
            // 删除一个小类
            int rows = labelConfigMapper.deleteClass(req.getVersion(), req.getCategoryId(), req.getClassId());
            return rows > 0;
        }
        if (req.getCategoryId() != null){
            // 删除大类
            int rows = labelConfigMapper.deleteCategory(req.getVersion(), req.getCategoryId());
            return rows > 0;
        }
        if (req.getClassId() != null){
            throw new WeTagException(BaseErrorCode.ILLEGAL_PARAMETERS);
        }
        return false;
    }

    @Override
    public boolean updateCategoryLabel(AddOrUpdateCategoryLabelReq req) {
        if (req.getCategoryId() == null){
            throw new WeTagException(BaseErrorCode.ILLEGAL_PARAMETERS);
        }
        req.setClassId(null);
        req.setClassName(null);
        if (StringUtils.hasLength(req.getCategoryName())){
            // 更新所有大类名称
            labelConfigMapper.updateCategoryName(req.getVersion(), req.getCategoryId(), req.getCategoryName());
        }

        // 更新大类配置
        LabelConfig labelConfig = new LabelConfig();
        BeanUtils.copyProperties(req, labelConfig);
        labelConfigMapper.updateCategory(labelConfig);
        return true;
    }

    @Override
    public boolean addCategoryLabel(AddOrUpdateCategoryLabelReq req) {
        if (!StringUtils.hasLength(req.getCategoryName())){
            throw new WeTagException(BaseErrorCode.ILLEGAL_PARAMETERS);
        }
        // 判断有无此大类
        Integer categoryId= labelConfigMapper.existCategoryName(req.getCategoryName(), req.getVersion());
        if (categoryId != null){
            throw new WeTagException(WeTagErrorCode.DUPLICATE_KEY);
        }else{
            Integer maxCategoryId = labelConfigMapper.getMaxCategoryId(req.getVersion());
            maxCategoryId = maxCategoryId == null?1:maxCategoryId + 1;
            req.setCategoryId(maxCategoryId);
        }

        Integer classId = labelConfigMapper.getMaxClassId(req.getVersion());
        classId = classId == null?1:classId + 1;
        req.setClassId(classId);
        try {
            LabelConfig config = new LabelConfig();
            BeanUtils.copyProperties(req, config);
            int rows = labelConfigMapper.addLabelInfoByModel(config);
            return rows>0;
        }catch (DuplicateKeyException e){
            throw new WeTagException(WeTagErrorCode.DUPLICATE_KEY);
        }
    }

    @Override
    public boolean addLabelInfo(AddOrUpdateLabelAdminReq req) {
        return labelConfigMapper.addLabelInfo(req) > 0;
    }

    @Override
    public boolean updateLabelInfo(AddOrUpdateLabelAdminReq req) {
        return labelConfigMapper.updateLabelInfo(req) > 0;
    }

    @Override
    public boolean deleteLabelInfo(DeleteLabelAdminReq req) {
        return labelConfigMapper.deleteLabelInfo(req) > 0;
    }

    @Override
    public PageResult<LabelConfigOssVo> getLabelInfoPage(GetLabelInfoPageAdminReq req) {
        PageHelper.startPage(req.getPage(), req.getRows());
        Page<LabelConfigOssVo> page = labelConfigMapper.getLabelConfigPage(req);
        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public boolean addLabelVersionSet(AddLabelVersionSetReq req) {
        String version = imageInfoMapper.isExistImageVersion(req.getImageVersion());
        if (!StringUtils.hasLength(version)) {
            throw new WeTagException(WeTagErrorCode.IMAGE_VERSION_NOT_EXIST);
        }
        try {
            int rows =labelConfigSetMapper.addLabelVersionSet(req);
            return rows > 0;
        }catch (DuplicateKeyException e){
            throw new WeTagException(WeTagErrorCode.DUPLICATE_KEY);
        }
    }

    @Override
    public boolean deleteLabelVersionSet(DeleteImageFeatureConfigReq req) {
        int rows = labelConfigSetMapper.deleteLabelVersionSet(req);
        return rows > 0;
    }

    @Override
    public boolean updateLabelVersionSet(UpdateLabelVersionSetReq req) {
        int rows = labelConfigSetMapper.updateLabelVersionSet(req);
        return rows > 0;
    }

    @Override
    public Boolean userLabelConfigUpdate(UserLabelConfigUpdateReq req) {
        int rows = userLabelConfigMapper.updateUserLabelConfig(req);
        return rows> 0;
    }

    @Override
    public LabelConfigVo  userLabelConfigGet(UserLabelConfigGetReq req) {
        LabelConfigVo result = new LabelConfigVo();
        result.setVersion(req.getVersion());

        // checkLabelVersionExist(req.getVersion(), req.getImageVersion());
        List<UserLabelConfigModel> userLabels = userLabelConfigMapper.getUserLabelConfig(req);
        if (CollectionUtils.isEmpty(userLabels)){
            List<LabelConfig> labelConfigList = labelConfigMapper.getLabelConfigListByVersion(req.getVersion());
            if (CollectionUtils.isEmpty(labelConfigList)){
                return result;
            }
            userLabels = labelConfigList.stream().map(o->{
                UserLabelConfigModel model = new UserLabelConfigModel();
                BeanUtils.copyProperties(o, model);
                model.setUid(req.getUid());
                return model;
            }).collect(Collectors.toList());
            userLabelConfigMapper.batchAddUserLabelConfig(userLabels);
        }

        HashMap<Integer, CategoryItemVo> categoryItemVoMap = new HashMap<>();

        for(LabelConfig item: userLabels){
            if (!categoryItemVoMap.containsKey(item.getCategoryId())){
                CategoryItemVo categoryItemVo = new CategoryItemVo();
                categoryItemVo.setId(item.getId());
                categoryItemVo.setCategoryId(item.getCategoryId());
                categoryItemVo.setCategoryName(item.getCategoryName());
                categoryItemVo.setPriority(item.getPriority()== null?0:item.getPriority());
                categoryItemVoMap.put(item.getCategoryId(), categoryItemVo);
            }
            CategoryItemVo categoryItemVo = categoryItemVoMap.get(item.getCategoryId());
            List<ClassItemVo> classItemVoList = categoryItemVo.getClassItemVoList();
            if (CollectionUtils.isEmpty(classItemVoList)){
                classItemVoList = new ArrayList<>();
            }
            if (!CommonConstant.CONFIG_SIGN.equals(item.getClassName())){
                ClassItemVo classItemVo = ClassItemVo.builder()
                        .id(item.getId())
                        .classId(item.getClassId())
                        .className(item.getClassName())
                        .shortcutKey(item.getShortcutKey())
                        .color(item.getColor())
                        .remark(item.getRemark())
                        .priority(item.getPriority()== null?0:item.getPriority())
                        .build();
                classItemVoList.add(classItemVo);
            }else{
                // 大类的 color 和快捷键
                categoryItemVo.setColor(item.getColor());
                categoryItemVo.setShortcutKey(item.getShortcutKey());
                categoryItemVo.setRemark(item.getRemark());
                categoryItemVo.setPriority(item.getPriority() == null?0:item.getPriority());
            }
            categoryItemVo.setClassItemVoList(classItemVoList);
            categoryItemVoMap.put(item.getCategoryId(), categoryItemVo);
        }
        List<CategoryItemVo> categoryItemVoList = new ArrayList<>(categoryItemVoMap.values());
        result.setCategoryItemVoList(categoryItemVoList);

        return result;
    }

    @Transactional
    @Override
    public Boolean resetUserLabelConfig(ResetUserLabelConfigReq req) {
        userLabelConfigMapper.deleteUserLabelConfig(req);
        List<LabelConfig> labelConfigList = labelConfigMapper.getLabelConfigListByVersion(req.getVersion());
        if (CollectionUtils.isEmpty(labelConfigList)){
            return true;
        }
        List<UserLabelConfigModel> userLabels = labelConfigList.stream().map(o->{
            UserLabelConfigModel model = new UserLabelConfigModel();
            BeanUtils.copyProperties(o, model);
            model.setUid(req.getUid());
            return model;
        }).collect(Collectors.toList());
        userLabelConfigMapper.batchAddUserLabelConfig(userLabels);
        return true;
    }

    @Override
    public LabelUsedVo checkLabelUsed(CheckLabelUsedReq req) {
        LabelConfig labelConfig = labelConfigMapper.getLabelConfigById(req.getId());
        if (labelConfig == null){
            throw new WeTagException(WeTagErrorCode.NO_RECORD_MATCH);
        }
        req.setLabelConfig(labelConfig);
        if (!req.getVersion().equals(labelConfig.getVersion())){
            throw new WeTagException(WeTagErrorCode.NO_RECORD_MATCH);
        }
        LabelUsedVo result;
        if (CommonConstant.CONFIG_SIGN.equals(labelConfig.getClassName())){
            // 统计大类
            result = labelInfoMapper.countCategoryUsed(req);
        }else{
            // 统计小类
            result = labelInfoMapper.countClassUsed(req);
        }
        return result;
    }

    @Override
    public Boolean replaceLabel(ReplaceLabelReq req) {
        LabelConfig oldLabelConfig = labelConfigMapper.getLabelConfigById(req.getOldLabelId());
        if (oldLabelConfig == null){
            throw new WeTagException(WeTagErrorCode.NO_RECORD_MATCH);
        }
        if (oldLabelConfig.getClassId().equals(CommonConstant.CONFIG_SIGN)){
            throw new WeTagException(WeTagErrorCode.CANNOT_REPLACE_CATEGORY);
        }

        List<Long> imgIds = labelInfoMapper.getImageIdsByLabelId(req.getVersion(), oldLabelConfig.getCategoryId(), oldLabelConfig.getClassId());
        if (CollectionUtils.isEmpty(imgIds)){
            return true;
        }

        if (req.getNewLabelId() == null){
            // 删除旧标签
            executorService.submit(()->{
              for(Long id: imgIds){
                   labelInfoMapper.deleteLabelByImgIdAndConfig(id, oldLabelConfig);
                   updateImageStatics(req.getLastModifyUsername(), id);
                   saveImageLabelHistory(id, req.getLastModifyUsername());
               };
            });
            return true;
        }
        // 替换标签逻辑
        LabelConfig newLabelConfig = labelConfigMapper.getLabelConfigById(req.getNewLabelId());
        if (newLabelConfig == null){
            throw new WeTagException(WeTagErrorCode.NO_RECORD_MATCH);
        }
        if (req.getOldLabelId().equals(req.getNewLabelId())){
            throw new WeTagException(WeTagErrorCode.CANNOT_SAME_LABEL);
        }
        if (newLabelConfig.getClassId().equals(CommonConstant.CONFIG_SIGN)){
            throw new WeTagException(WeTagErrorCode.CANNOT_REPLACE_CATEGORY);
        }
        executorService.submit(()->{
            for(Long id: imgIds){
                labelInfoMapper.replaceLabelByImgIdAndConfig(id, oldLabelConfig, newLabelConfig);
                updateImageStatics(req.getLastModifyUsername(), id);
                saveImageLabelHistory(id, req.getLastModifyUsername());
            }
        });
        return true;
    }

    private void updateImageStatics(String username, Long imgId){
        int count = labelInfoMapper.countImageLabelsByImgId(imgId);
        imageInfoMapper.updateImageLabelNumAndModifyUid(username, count, imgId);
    }

    private void saveImageLabelHistory(Long imgId, String username){
        List<ImageLabel> infoList = labelInfoMapper.getImageLabelListByImgId(imgId);

        List<ImageLabel> history =  labelInfoMapper.getLastHistory(imgId);
        //  比较是否有改变
        if (history.size() == infoList.size()){
            Map<String, ImageLabel> newMap = new HashMap<>(infoList.size());
            for (ImageLabel item: infoList){
                newMap.put(getKey(item), item);
            }
            for (ImageLabel item: history){
                ImageLabel newItem = newMap.get(getKey(item));
                if (newItem==null || !newItem.isEqualContent(item)){
                    labelInfoMapper.batchInsertHistory(infoList, username);
                    break;
                }
            }
        }else{
            labelInfoMapper.batchInsertHistory(infoList, username);
        }
    }

    private String getKey(ImageLabel item){
        return String.format("%s_%s_%s_%s_%s_%s", item.getCategoryId(), item.getClassId(),
                item.getCoordinateX().setScale(15, RoundingMode.HALF_UP),
                item.getCoordinateY().setScale(15, RoundingMode.HALF_UP),
                item.getWidth().setScale(15, RoundingMode.HALF_UP),
                item.getHeight().setScale(15, RoundingMode.HALF_UP)
        );
    }

    @Override
    public Boolean labelResort(LabelResortReq req) {
        int rows;
        if (req.getIsUserLabel()){
            rows = labelConfigMapper.labelUserResort(req.getUid(), req.getVersion(), req.getLabelSortItems());
        }else{
            rows = labelConfigMapper.labelResort(req.getVersion(), req.getLabelSortItems());
        }
        return rows > 0;
    }


    private void addUserLabelConfig(LabelConfig config){
        List<Long> uids = userLabelConfigMapper.getUidsByVersion(config.getVersion());
        if (CollectionUtils.isEmpty(uids)){
            return;
        }
        List<UserLabelConfigModel> userLabelConfigModels = uids.stream().map(o->{
            UserLabelConfigModel model = new UserLabelConfigModel();
            BeanUtils.copyProperties(config, model);
            model.setUid(o);
            return model;
        }).collect(Collectors.toList());
        userLabelConfigMapper.batchAddUserLabelConfig(userLabelConfigModels);
    }

    private void checkLabelVersionExist(String labelversion, String imageVersion){
        Set<String> labelVersions = labelConfigSetMapper.getLabelVersionSetByImageVersion(imageVersion);
        if (!labelVersions.contains(labelversion)){
            throw new WeTagException(WeTagErrorCode.LABEL_VERSION_NOT_EXIST);
        }
    }
}
