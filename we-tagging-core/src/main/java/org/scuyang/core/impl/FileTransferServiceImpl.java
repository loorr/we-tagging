package org.scuyang.core.impl;

import lombok.extern.log4j.Log4j2;
import org.scuyang.core.oss.FileTransferService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Log4j2
@Service
public class FileTransferServiceImpl implements FileTransferService {
//    @Resource
//    private RestTemplate restTemplate;

    private static final String api = "";

    public void upload(MultipartFile imageMulti) {

        //restTemple不能传输File和MulipartFile,转为FileSystemResource后传输
        //构建FileSystemResource需要文件类型为File，因此先将MulipartFile转为File
        //convertTemporaryPath转换临时地址
//        File image = ConvertUtil.multipartFileToFile(imageMulti, convertTemporaryPath);
//
//        try {
//            FileSystemResource resource = new FileSystemResource(image);
//            MultiValueMap<String, Object> param = new LinkedMultiValueMap<>();
//            param.add("faceimg", resource);
//            param.add("time", time);
//            //使用HttpEntity构建需要传递的参数
//            HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(param);
//            restTemplate.postForEntity(api, httpEntity, CustomerDetectInfo.class).getBody();
//        } catch (RestClientException e) {
//            e.printStackTrace();
//        } finally {
//            image.delete();
//        }
    }

    public static File multipartFileToFile(MultipartFile file, String convertBasePath) {
        if (file != null) {
            File base = new File(convertBasePath);
            if (!base.exists()) {
                base.mkdirs();
            }
            String filePath = convertBasePath + file.getName() + String.valueOf((int) (100000 * Math.random())) + ".jpeg";

            File conventFile = new File(filePath);
            try {
                boolean isCreateSuccess = conventFile.createNewFile();
                if (isCreateSuccess) {
                    file.transferTo(conventFile);
                    return conventFile;
                }
            } catch (IOException e) {
                log.warn("multipartFile convert to File failed  -> " + e.getMessage());
            }
        }
        return null;
    }


}
