package org.scuyang.core.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.tove.web.infra.common.PageResult;
import org.scuyang.common.model.ImageInfo;
import org.scuyang.core.ImageService;
import org.scuyang.dao.ImageInfoMapper;
import org.scuyang.oss.api.req.GetImagePageAdminReq;
import org.scuyang.oss.api.vo.ImageVo;
import org.scuyang.rest.api.error.WeTagErrorCode;
import org.scuyang.rest.api.error.WeTagException;
import org.scuyang.rest.api.req.GetImageFeatureReq;
import org.scuyang.rest.api.req.UpdateImageFeatureReq;
import org.scuyang.rest.api.vo.ImageFeatureVo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ImageServiceImpl implements ImageService {

    @Value("${oss.img.url}")
    private String imgUrlPrefix;

    @Resource
    private ImageInfoMapper imageInfoMapper;

    @Override
    public ImageFeatureVo getImageFeature(GetImageFeatureReq req) {
        ImageInfo imageInfo = imageInfoMapper.getFeature(req.getImgId());
        ImageFeatureVo imageFeatureVo = new ImageFeatureVo();
        if (imageInfo == null || imageInfo.getImageFeatures() == null){
            imageFeatureVo.setFeatureMap(ImageInfo.getEmptyFeatureMap());
            return imageFeatureVo;
        }
        imageFeatureVo.setFeatureMap(imageInfo.getFeatureMap());
        return imageFeatureVo;
    }

    @Override
    public Boolean updateImageFeature(UpdateImageFeatureReq req) {
        String feature = "";
        try {
            feature = ImageInfo.generateImageFeatureString(req.getImageFeatures());
        }catch (Exception e){
            throw new WeTagException(WeTagErrorCode.NO_IMAGE_FEATURES);
        }
        ImageInfo imageInfo = imageInfoMapper.getFeature(req.getImgId());
        if (imageInfo == null || imageInfo.getId() == null){
            throw new WeTagException(WeTagErrorCode.NO_IMAGE);
        }
        imageInfoMapper.updateFeature(req.getImgId(), feature);
        return true;
    }

    @Override
    public PageResult<ImageVo> getImagePage(GetImagePageAdminReq req) {
        PageHelper.startPage(req.getPage(), req.getRows());
        Page<ImageVo> page = imageInfoMapper.getImagePage(req);
        List<ImageVo> list = page.getResult().stream().map(o->{
            o.setUri(imgUrlPrefix + o.getUri());
            return o;
        }).collect(Collectors.toList());
        PageResult<ImageVo> result = new PageResult(page.getTotal(), list);
        return result;
    }
}
