package org.scuyang.core.impl;

import cn.hutool.json.JSONUtil;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.exception.CosServiceException;
import com.qcloud.cos.exception.FileLockException;
import com.qcloud.cos.http.HttpProtocol;
import com.qcloud.cos.model.GetObjectRequest;
import com.qcloud.cos.model.ObjectMetadata;
import com.qcloud.cos.region.Region;
import com.tencent.cloud.CosStsClient;
import com.tencent.cloud.Response;
import com.tencent.cloud.cos.util.Jackson;
import com.tove.web.infra.common.BaseErrorCode;
import lombok.extern.log4j.Log4j2;
import org.scuyang.core.CosService;
import org.scuyang.rest.api.error.WeTagErrorCode;
import org.scuyang.rest.api.error.WeTagException;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.TreeMap;

@Log4j2
@Service
public class CosServiceImpl implements CosService {

    private static final String secretId = "AKIDoQnukC5KzNWP1M79dhaEfqlSt3wYw2xk";
    private static final String secretKey = "kHkzyiBft08T9aGl4A1bxuc4RgpiKhtt";
    private static final String bucket = "scu-yang-1256251417";
    private static final String region = "ap-chengdu";

    @Override
    public Response getTmpSecretToken() {
        TreeMap<String, Object> config = new TreeMap();
        config.put("SecretId", secretId);
        config.put("SecretKey", secretKey);
        config.put("durationSeconds", 60*120);
        config.put("bucket", bucket);
        config.put("region", region);
        // 这里改成允许的路径前缀，可以根据自己网站的用户登录态判断允许上传的具体路径，例子：a.jpg 或者 a/* 或者 * 。
        // 如果填写了“*”，将允许用户访问所有资源；除非业务需要，否则请按照最小权限原则授予用户相应的访问权限范围。
        config.put("allowPrefixes", new String[] {"*"});
        // 密钥的权限列表。必须在这里指定本次临时密钥所需要的权限。简单上传、表单上传和分片上传需要以下的权限，
        // 其他权限列表请看 https://cloud.tencent.com/document/product/436/31923
        String[] allowActions = new String[] {
                // 简单上传
                "name/cos:PutObject",
                // 表单上传、小程序上传
                "name/cos:PostObject",
                // 分块上传
                "name/cos:InitiateMultipartUpload",
                "name/cos:ListMultipartUploads",
                "name/cos:ListParts",
                "name/cos:UploadPart",
                "name/cos:CompleteMultipartUpload"
        };
        config.put("allowActions", allowActions);
        try {
            Response response = CosStsClient.getCredential(config);
            return response;
        }catch (Exception e){
            throw new WeTagException(BaseErrorCode.SYSTEM_BUSY);
        }
    }

    public static void main(String[] args) {
        Response response = new  CosServiceImpl().getTmpSecretToken();
        System.out.println(JSONUtil.parseObj(Jackson.toJsonPrettyString(response), false));
    }
    @Override
    public void downloadFile(String outputFilePath, String fileKey){
        File downFile = new File(outputFilePath);
        GetObjectRequest getObjectRequest = new GetObjectRequest(bucket, fileKey);
        try {
            ObjectMetadata downObjectMeta = getCOSClient().getObject(getObjectRequest, downFile);
            System.out.println(downObjectMeta);
        }catch (FileLockException e){
            e.printStackTrace();
        }catch (CosServiceException e){
            log.warn("文件不存在");
            e.printStackTrace();
        }
    }

    private COSClient getCOSClient(){
        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
        Region region1 = new Region(region);
        ClientConfig clientConfig = new ClientConfig(region1);
        clientConfig.setHttpProtocol(HttpProtocol.https);
        return new COSClient(cred, clientConfig);
    }
}
