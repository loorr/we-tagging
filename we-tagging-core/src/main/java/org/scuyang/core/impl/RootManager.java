package org.scuyang.core.impl;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

@Slf4j
@Service
public class RootManager {
    @Value("${oss.root.folder}")
    private String rootFolder;

    @Value("${oss.root.temp.folder}")
    private String rootTempFolder;

    @EventListener(ApplicationReadyEvent.class)
    public void AfterStart(){
        checkRootFolder(rootFolder);
        checkRootFolder(rootTempFolder);
    }

    private void checkRootFolder(String path){
        File file = new File(path);
        if (!file.exists() || !file.isDirectory()) {
            file.mkdir();
        }
    }

    /** 拷贝文件 */
    public static void copyFile(String source, String target) {
        long start = System.currentTimeMillis();
        try {
            FileUtils.copyFile(new File(source), new File(target));
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("FileUtils file copy cost {} msc",  System.currentTimeMillis() - start);
    }

    /** 拷贝文件 */
    public static void copyDirectory(String source, String target) {
        long start = System.currentTimeMillis();
        try {
            FileUtils.copyDirectory(new File(source), new File(target));
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("FileUtils file copy cost {} msc",  System.currentTimeMillis() - start);
    }
}
