package org.scuyang.core.impl;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.tove.web.infra.common.BaseErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.scuyang.common.model.FeatureConfig;
import org.scuyang.common.model.FeatureData;
import org.scuyang.common.model.GroupFeatureData;
import org.scuyang.core.FeatureService;
import org.scuyang.dao.ImageFeatureMapper;
import org.scuyang.dao.ImageInfoMapper;
import org.scuyang.rest.api.error.WeTagErrorCode;
import org.scuyang.rest.api.error.WeTagException;
import org.scuyang.rest.api.req.GetImageFeatureReq;
import org.scuyang.rest.api.req.feature.*;
import org.scuyang.rest.api.vo.feature.FeatureDataItem;
import org.scuyang.rest.api.vo.feature.FeatureItem;
import org.scuyang.rest.api.vo.feature.FeatureVo;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
@Service
public class FeatureServiceImpl implements FeatureService {

    @Resource
    private ImageFeatureMapper imageFeatureMapper;

    @Resource
    private ImageInfoMapper imageInfoMapper;

    // 线程池
    @Resource
    private Executor taskExecutor;

    private final LoadingCache<String, List<FeatureConfig>> cache = Caffeine.newBuilder()
            .maximumSize(100)
            .expireAfterWrite(10, TimeUnit.MINUTES)
            .build(o -> {
                String[] arr = o.split("::");
                if (arr.length != 2){
                    return null;
                }
                return imageFeatureMapper.getFeatureConfig(arr[0], arr[1]);
            });

    private String getCacheKey(String imageVersion, String featureType) {
        return String.format("%s::%s", imageVersion, featureType);
    }

    @Override
    public FeatureVo getImageFeature(GetImageFeatureReq req) {
        FeatureVo featureVo = new FeatureVo();
        String key = getCacheKey(req.getImageVersion(), FeatureType.IMAGE.name());
        List<FeatureConfig> list = cache.get(key);
        if (CollectionUtils.isEmpty(list)) {
            return featureVo;
        }
        Map<Long, FeatureItem> featureItemMap = list.stream().map(o->{
            FeatureItem item = new FeatureItem();
            BeanUtils.copyProperties(o, item);
            return item;
        }).collect(Collectors.toMap(FeatureItem::getId, o->o));

        List<FeatureData> imageFeatData = imageFeatureMapper.getFeatureData(req.getImgId());

        for(FeatureData data : imageFeatData) {
            if (featureItemMap.containsKey(data.getFeatureId())) {
                FeatureItem item = featureItemMap.get(data.getFeatureId());
                item.setValue(data.getData());
            }
        }

        List<FeatureItem> featureItems = featureItemMap.values().stream().map(o->{
            o.setValue(o.getValue() == null ? o.getDefaultValue() : o.getValue());
            return o;
        }).collect(Collectors.toList());
        featureVo.setFeatureList(featureItems);
        return featureVo;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean saveImageFeatureData(SaveImageFeatureReq req) {
        if (CollectionUtils.isEmpty(req.getFeatureList())){
            return false;
        }
        List<FeatureData> featureDataList = new ArrayList<>();
        for (FeatureDataItem item : req.getFeatureList()) {
            FeatureData data = new FeatureData();
            data.setImageId(req.getImageId());
            data.setFeatureId(item.getId());
            data.setData(item.getValue());
            featureDataList.add(data);
        }
        try {
            if (CollectionUtils.isEmpty(featureDataList)) {
                return false;
            }
            taskExecutor.execute(() -> {
                imageFeatureMapper.deleteFeatureData(req.getImageId());
                imageFeatureMapper.batchInsertFeatureData(featureDataList);
            });
        } catch (DuplicateKeyException e) {
            throw new WeTagException(WeTagErrorCode.DUPLICATE_KEY);
        }catch (Exception e) {
            log.error("saveImageFeatureData error", e);
            throw new WeTagException(BaseErrorCode.SYSTEM_BUSY);
        }
        return true;
    }

    @Override
    public Boolean addImageFeatureConfig(AddImageFeatureConfigReq req) {
        try {
            String version = imageInfoMapper.isExistImageVersion(req.getImageVersion());
            if (!StringUtils.hasLength(version)) {
                throw new WeTagException(WeTagErrorCode.IMAGE_VERSION_NOT_EXIST);
            }

            FeatureConfig featureConfig = new FeatureConfig();
            BeanUtils.copyProperties(req, featureConfig);
            featureConfig.setFeatureType(req.getFeatureType().name());
            int rows = imageFeatureMapper.insertFeatureConfig(featureConfig);

            String key = getCacheKey(req.getImageVersion(),req.getFeatureType().name());
            cache.invalidate(key);
            return rows>0;
        }catch (DuplicateKeyException e){
            throw new WeTagException(WeTagErrorCode.DUPLICATE_KEY);
        }
    }

    @Override
    public Boolean deleteImageFeatureConfig(DeleteImageFeatureConfigReq req) {
        int rows = imageFeatureMapper.deleteFeatureConfig(req.getId());
        FeatureConfig config = imageFeatureMapper.getFeatureConfigById(req.getId());
        cache.invalidate(getCacheKey(config.getImageVersion(), config.getFeatureType()));
        return rows>0;
    }

    @Override
    public List<FeatureItem> getImageFeatureConfig(GetImageFeatureConfigReq req) {
        String key = getCacheKey(req.getImageVersion(),req.getFeatureType().name());
        List<FeatureConfig> list = cache.get(key);
        if (CollectionUtils.isEmpty(list)) {
            return new ArrayList<>(0);
        }
        return list.stream().map(item -> {
            FeatureItem featureItem = new FeatureItem();
            featureItem.setId(item.getId());
            featureItem.setFeatureName(item.getFeatureName());
            featureItem.setType(item.getType());
            featureItem.setRemark(item.getRemark());
            featureItem.setValid(item.getValid());
            featureItem.setDataFormat(item.getDataFormat());
            featureItem.setDefaultValue(item.getDefaultValue());
            return featureItem;
        }).collect(Collectors.toList());
    }

    @Override
    public Boolean updateImageFeatureConfig(UpdateImageFeatureConfigReq req) {
        int rows = imageFeatureMapper.updateImageFeatureConfig(req);
        if (rows>0) {
            FeatureConfig config = imageFeatureMapper.getFeatureConfigById(req.getId());
            cache.invalidate(getCacheKey(config.getImageVersion(), config.getFeatureType()));
        }
        return rows > 0;
    }

    @Override
    public FeatureVo getImageGroupFeature(GetImageGroupFeatureReq req) {
        FeatureVo featureVo = new FeatureVo();
        String key = getCacheKey(req.getImageVersion(), FeatureType.IMAGE_GROUP.name());
        List<FeatureConfig> list = cache.get(key);
        if (CollectionUtils.isEmpty(list)) {
            return featureVo;
        }
        Map<Long, FeatureItem> featureItemMap = list.stream().map(o->{
            FeatureItem item = new FeatureItem();
            BeanUtils.copyProperties(o, item);
            return item;
        }).collect(Collectors.toMap(FeatureItem::getId, o->o));

        List<FeatureData> groupFeatData = imageFeatureMapper.getGroupFeatureData(req.getImageVersion(), req.getImageGroup());
        for(FeatureData data : groupFeatData) {
            if (featureItemMap.containsKey(data.getFeatureId())) {
                FeatureItem item = featureItemMap.get(data.getFeatureId());
                item.setValue(data.getData());
            }
        }

        List<FeatureItem> featureItems = featureItemMap.values().stream().map(o->{
            o.setValue(o.getValue() == null ? o.getDefaultValue() : o.getValue());
            return o;
        }).collect(Collectors.toList());
        featureVo.setFeatureList(featureItems);
        return featureVo;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean saveImageGroupFeature(SaveImageGroupFeatureReq req) {
        if (CollectionUtils.isEmpty(req.getFeatureList())){
            return false;
        }
        List<GroupFeatureData> featureDataList = new ArrayList<>();
        for (FeatureDataItem item : req.getFeatureList()) {
            GroupFeatureData data = new GroupFeatureData();
            data.setFeatureId(item.getId());
            data.setData(item.getValue());
            data.setImageGroup(req.getImageGroup());
            data.setImageVersion(req.getImageVersion());
            featureDataList.add(data);
        }
        try {
            if (CollectionUtils.isEmpty(featureDataList)) {
                return false;
            }
            taskExecutor.execute(()->{
                imageFeatureMapper.deleteGroupFeatureData(req.getImageVersion(), req.getImageGroup());
                imageFeatureMapper.batchInsertGroupFeatureData(featureDataList);
            });
        } catch (DuplicateKeyException e) {
            throw new WeTagException(WeTagErrorCode.DUPLICATE_KEY);
        }catch (Exception e) {
            log.error("saveImageGroupFeature error", e);
            throw new WeTagException(BaseErrorCode.SYSTEM_BUSY);
        }
        return true;
    }
}
