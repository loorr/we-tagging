package org.scuyang.core.impl;

import org.redisson.api.RAtomicDouble;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class RedisServiceImpl {
    private static final String key = "kkkk11";

    @Resource(name = "weTaggingRedissonClient")
    private RedissonClient redissonClient;

    @EventListener(ApplicationReadyEvent.class)
    public void AfterStart(){
        RAtomicDouble atomicDouble = redissonClient.getAtomicDouble(key);
        atomicDouble.set(1000.00);
    }

    public double sub(){
        RAtomicDouble atomicDouble = redissonClient.getAtomicDouble(key);
        return atomicDouble.decrementAndGet();
    }

    public double get(){
        RAtomicDouble atomicDouble = redissonClient.getAtomicDouble(key);
        return atomicDouble.get();
    }

    public void getLock(){
        RLock rLock = redissonClient.getLock(key);
        rLock.lock();
    }



}
