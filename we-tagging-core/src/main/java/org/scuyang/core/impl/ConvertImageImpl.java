package org.scuyang.core.impl;

import lombok.extern.slf4j.Slf4j;
import org.scuyang.core.ConvertImageService;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class ConvertImageImpl implements ConvertImageService {
    private final List<String> imageNameList = new ArrayList<>();

    @Override
    public void convertImage(String rootFolder, String imageFolder, String currFolder) {

    }

    private List<String> getAllImageName(String folderPath){
        File folder = new File(folderPath);
        if (folder.exists()) {
            File[] files = folder.listFiles();
            for (File file2:files){
                if (file2.getName().indexOf("jpg")!=-1){
                    System.out.println(file2.getName());
                }
            }
        }
        return null;
    }
}
