package org.scuyang.core.impl;

import cn.dev33.satoken.stp.StpUtil;
import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.tove.web.infra.common.BaseErrorCode;
import com.tove.web.infra.common.BaseException;
import com.tove.web.infra.common.Response;
import lombok.extern.slf4j.Slf4j;
import org.example.account.center.api.AccountApi;
import org.example.account.center.api.entity.AccountVo;
import org.example.account.center.api.entity.req.GetAccountInfoReq;
import org.scuyang.common.model.Account;
import org.scuyang.common.model.Role;
import org.scuyang.common.model.UserRole;
import org.scuyang.core.AuthService;
import org.scuyang.core.RedisService;
import org.scuyang.dao.AccountMapper;
import org.scuyang.rest.api.error.WeTagErrorCode;
import org.scuyang.rest.api.error.WeTagException;
import org.scuyang.rest.api.req.LoginReq;
import org.scuyang.rest.api.req.SignReq;
import org.scuyang.rest.api.req.auth.ModifyUserInfoReq;
import org.scuyang.rest.api.req.auth.UserInfo;
import org.scuyang.rest.api.vo.user.UserGroupVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AuthServiceImpl implements AuthService {

    private static Map<String,String> roleMap = new HashMap<>();

    @Resource
    private AccountMapper accountMapper;

    private final SecureRandom random = new SecureRandom();

    @Resource
    private RedisService redisService;

    @Resource
    private AccountApi accountApi;
    @Value("${account-center.tenantCode:c5779512-fd63-4f20-bffe-e34c66612640}")
    private String tenantCode;

    public final LoadingCache<Long, AccountVo> accountCache = Caffeine.newBuilder()
            .maximumSize(1000)
            .expireAfterWrite(10, TimeUnit.MINUTES)
            .build(key->{
                GetAccountInfoReq req = new GetAccountInfoReq();
                req.setUid(key);
                Response<AccountVo> accountVo = accountApi.getAccountInfo(req, tenantCode);
                return accountVo.getData();
            });

    @Override
    public UserInfo login(LoginReq req) {
        org.example.account.center.api.entity.req.LoginReq arg = new org.example.account.center.api.entity.req.LoginReq();
        BeanUtils.copyProperties(req, arg);

        UserInfo userInfo = new UserInfo();
        Response<AccountVo> accountVo = accountApi.login(arg, tenantCode);
        if (accountVo == null || !BaseErrorCode.OPERATION_SUCCESS.getCode().equals(accountVo.getCode())) {
            throw new BaseException(WeTagErrorCode.PWD_MISTAKE);
        }
        AccountVo account = accountVo.getData();
        if (account == null) {
            throw new BaseException(WeTagErrorCode.USER_NOT_EXIST);
        }
        StpUtil.login(account.getUid());
        log.info("user:{} role:{}", account.getUid(), StpUtil.getRoleList());

        userInfo.setEmail(account.getEmail());
        userInfo.setUid(account.getUid());
        userInfo.setUsername(account.getUsername());
        userInfo.setToken(StpUtil.getTokenValue());
        userInfo.setPermissions(account.getDefaultPermissions());
        userInfo.setRoles(account.getDefaultRoles());
        userInfo.setUserGroupList(account.getUserGroups().stream().map(item->{
            UserGroupVo userGroupVo = new UserGroupVo();
            userGroupVo.setGroupName(item.getName());
            userGroupVo.setRoles(item.getRoles());
            userGroupVo.setPermissions(item.getPermissions());
            return userGroupVo;
        }).collect(Collectors.toList()));

        redisService.saveOnlineUser(userInfo);
        return userInfo;
    }

    private <T> void checkResult(Response<T> result) {
        if (result == null || !BaseErrorCode.OPERATION_SUCCESS.getCode().equals(result.getCode())) {
            throw new RuntimeException("result is null");
        }
    }

    @Override
    public UserInfo getUserInfo() {
        String uid = String.valueOf(StpUtil.getLoginId());
        UserInfo userInfo = redisService.getOnlineUser(Long.valueOf(uid));
        return userInfo;
    }

    @Transactional
    @Override
    public Long sign(SignReq req) {
        Long uid;
        while (true){
            uid = nextUid();
            Account account = accountMapper.getUserByUid(uid);
            if (account == null){
                break;
            }
        }
        Account account = new Account();
        account.setEmail(req.getEmail());
        account.setPassword(req.getPassword());
        account.setUid(uid);
        try {
            int i = accountMapper.insertUser(account);
            List<UserRole> roleList = req.getRoles().stream().map(o->{
                UserRole userRole = new UserRole();
                userRole.setRole(o);
                userRole.setUid(account.getUid());
                return userRole;
            }).collect(Collectors.toList());
            int k = accountMapper.batchInsertRole(roleList);
            return uid;
        }catch (DuplicateKeyException e){
            throw new WeTagException(WeTagErrorCode.DUPLICATE_KEY);
        }
    }

    @Override
    public boolean modifyUserInfo(ModifyUserInfoReq req) {
        Long uid = Long.valueOf(String.valueOf(StpUtil.getLoginId()));
        Account account = accountMapper.getUserByUid(uid);
        if (!account.getPassword().equals(req.getOldPassword())){
            throw new WeTagException(WeTagErrorCode.PWD_MISTAKE);
        }
        int rows = accountMapper.updatePassword(uid, req.getNewPassword());
        return rows > 0;
    }

    @Override
    public boolean checkRoles(List<String> roles) {
        for (String role : roles){
            if (!roleMap.containsKey(role)){
                return false;
            }
        }
        return true;
    }

    @Override
    public AccountVo getAccountInfo(Long uid) {
         return accountCache.get(uid);
    }

    /** 返回6位uid */
    private Long nextUid(){
        return (long) ((random.nextFloat()*9+1)* 100000);
    }

    /** 每分钟运行一次  */
    @Scheduled(cron = "0 0/1 * * * ?")
    public void syncRoleMap(){
        List<Role> roleList = accountMapper.getAllRole();
        roleList.forEach(o->{
            roleMap.put(o.getName(), o.getRemark());
        });
    }
}
