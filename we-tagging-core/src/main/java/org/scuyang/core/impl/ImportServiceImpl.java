package org.scuyang.core.impl;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.scuyang.common.model.ImageInfo;
import org.scuyang.common.model.ImageLabel;
import org.scuyang.common.model.ImageLabelAlgorithm;
import org.scuyang.common.model.ModelConfig;
import org.scuyang.common.util.ReadLabelText;
import org.scuyang.common.ws.WsMsgDO;
import org.scuyang.common.ws.WsMsgType;
import org.scuyang.core.ModelConfigService;
import org.scuyang.core.oss.ImportService;
import org.scuyang.core.RedisService;
import org.scuyang.core.mq.RedisQueueMsgProducer;
import org.scuyang.dao.ImageInfoMapper;
import org.scuyang.dao.LabelInfoMapper;
import org.scuyang.oss.api.error.OssErrorCode;
import org.scuyang.oss.api.error.OssException;
import org.scuyang.oss.api.req.SingleImageRecognizeReq;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ImportServiceImpl implements ImportService {

    @Resource
    private ImageInfoMapper imageInfoMapper;

    @Resource
    private LabelInfoMapper labelInfoMapper;

    @Resource
    private RedisQueueMsgProducer redisQueueMsgProducer;

    @Resource
    private RedisService redisService;

    @Resource
    private ModelConfigService modelConfigService;

    @Async("taskExecutor2")
    @Override
    public void importImageLabelFromTxt(String path) {
        log.info("开始导入标签: {} ", path);
        File file = new File(path);
        if (file == null){
            log.info("{} 路径下文件不存在", path);
        }
        List<File> singleLableFileList = new ArrayList<>();
        if (file.isDirectory()){
            for (File subFile: file.listFiles()){
                singleLableFileList.add(subFile);
            }
        }else{
            return;
        }

        for (File fileItem: singleLableFileList){
            String fileName = fileItem.getName();
            String md5 = fileName.substring(0, fileName.lastIndexOf("."));
            String filePath = fileItem.getPath();

            ImageInfo imageInfo = imageInfoMapper.getImageInfoByMd5(md5);
            if (imageInfo == null){
                throw new OssException(OssErrorCode.NO_TARGET_IMAGE);
            }
            try {
                log.info(fileName, filePath);
                List<String> labelList =  ReadLabelText.readTxtFileIntoStringArrList(filePath);
                List<ImageLabel> imageLabelList = ImageLabel.transformLabelList(labelList, imageInfo.getId());
                List<Long> imageIdList = imageLabelList.stream().map(ImageLabel::getImgId).collect(Collectors.toList());
                labelInfoMapper.deleteAlgorithmLabelByGroup(imageIdList);
                labelInfoMapper.batchInsertAlgorithm(imageLabelList);
            }catch (DuplicateKeyException e){
                throw new OssException(OssErrorCode.DUPLICATE_KET);
            }catch (Exception e){
                e.printStackTrace();
                throw new OssException(OssErrorCode.NO_TARGET_FOLDER);
            }
        }
        log.info("导入标签完成: {} ", path);
    }

    @Override
    public void importSignleLabelTxt(SingleImageRecognizeReq req, String path){
        log.info("开始导入标签: {} ", path);
        if (path == null){
            sendNoneData(req.getImgId());
            return;
        }
        File file = new File(path);
        if (file == null){
            log.info("{} 路径下文件不存在", path);
            sendNoneData(req.getImgId());
            return;
        }

        ImageInfo imageInfo = imageInfoMapper.getImageInfo(req.getImgId());
        if (imageInfo == null){
            sendNoneData(req.getImgId());
            throw new OssException(OssErrorCode.NO_TARGET_IMAGE);
        }
        try {
            List<String> labelList =  ReadLabelText.readTxtFileIntoStringArrList(path);
            List<ImageLabelAlgorithm> imageLabelList = ImageLabel.transformAlgoLabelList(labelList, imageInfo.getId());
            log.info("imageLabelList: {} {}", labelList.size(), imageLabelList.size());
            ModelConfig modelConfig = getModel(req.getModelCode(), imageInfo);
            if (imageLabelList == null || imageLabelList.size() == 0){
                sendNoneData(req.getImgId());
                return;
            }
            imageLabelList.forEach(o->{
                o.setModelCode(modelConfig.getModelCode());
                o.setModelVersion(modelConfig.getModelVersion());
                o.setScore(BigDecimal.ONE);
            });
            labelInfoMapper.deleteAlgorithmLabel(req.getImgId());
            labelInfoMapper.batchInsertAlgoLabels(imageLabelList);
            log.info("导入标签完成 数量: {} ", imageLabelList.size());

            Set<String> uids = redisService.popAllSingleImageUser(String.valueOf(req.getImgId()));
            if (CollectionUtils.isEmpty(uids)){
                log.info("没有可发送的对象");
                return;
            }
            WsMsgDO<String> wsMsgDO =new WsMsgDO<>(WsMsgType.SINGLE_IMAGE, JSON.toJSONString(imageLabelList));
            wsMsgDO.setUids(uids);
            String data = JSON.toJSONString(wsMsgDO);
            redisQueueMsgProducer.sendData(data);
        }catch (DuplicateKeyException e){
            sendNoneData(req.getImgId());
            throw new OssException(OssErrorCode.DUPLICATE_KET);
        }catch (Exception e){
            sendNoneData(req.getImgId());
            e.printStackTrace();
            // throw new OssException(OssErrorCode.NO_TARGET_FOLDER);
        }
    }

    private ModelConfig getModel(String modelCode, ImageInfo imageInfo){
        List<ModelConfig> models = modelConfigService.getModelConfig(imageInfo.getVersion());
        if (CollectionUtils.isEmpty(models)){
            return null;
        }
        for(ModelConfig model : models){
            if(model.getModelCode().equals(modelCode)){
                return model;
            }
        }
        return null;
    }

    private void sendNoneData(Long imgId){
        WsMsgDO<String> wsMsgDO =new WsMsgDO<>(WsMsgType.SINGLE_IMAGE, "");
        Set<String> uids = redisService.popAllSingleImageUser(String.valueOf(imgId));
        if (CollectionUtils.isEmpty(uids)){
            log.info("没有可发送的对象");
            return;
        }
        wsMsgDO.setUids(uids);
        String data = JSON.toJSONString(wsMsgDO);
        redisQueueMsgProducer.sendData(data);
    }

}
