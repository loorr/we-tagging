package org.scuyang.core.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.tove.web.infra.common.BaseException;
import com.tove.web.infra.common.PageHelperUtil;
import com.tove.web.infra.common.Response;
import lombok.extern.log4j.Log4j2;
import org.example.account.center.api.AccountApi;
import org.example.account.center.api.entity.req.GetUserIdListByRoleReq;
import org.scuyang.common.enums.ImageLabelStateEnum;
import org.scuyang.common.model.ApproveConfig;
import org.scuyang.common.model.ApproveTask;
import org.scuyang.common.model.ApproveTaskLog;
import org.scuyang.common.model.ImageInfo;
import org.scuyang.common.ws.domain.Do.UserImage;
import org.scuyang.common.ws.domain.vo.LockStateEnum;
import org.scuyang.core.ApproveService;
import org.scuyang.core.RedisService;
import org.scuyang.dao.ApproveMapper;
import org.scuyang.dao.ImageInfoMapper;
import org.scuyang.rest.api.error.WeTagErrorCode;
import org.scuyang.rest.api.req.image.GetImageGroupsReq;
import org.scuyang.rest.api.req.label.GetImagePageReq;
import org.scuyang.rest.api.req.approve.ApproveImageReq;
import org.scuyang.rest.api.req.approve.RequestApproveReq;
import org.scuyang.rest.api.req.image.GetAllImageVersionReq;
import org.scuyang.rest.api.vo.ImageGroupVo;
import org.scuyang.rest.api.vo.ImageVersionVo;
import org.scuyang.rest.api.vo.tag.ImageInfoVo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

import static org.scuyang.common.model.ApproveCommon.*;

@Log4j2
@Service
public class ApproveServiceImpl implements ApproveService {
    @Resource
    private ApproveMapper  approveMapper;

    @Resource
    private ImageInfoMapper imageInfoMapper;

    @Value("${oss.img.url}")
    private String imgUrlPrefix;

    @Resource
    private RedisService redisService;

    @Resource
    private AccountApi accountApi;

    @Value("${account-center.tenantCode:c5779512-fd63-4f20-bffe-e34c66612640}")
    private String tenantCode;

    @Value("${we-tagging.scheduled.synApproveConfig.enable:false}")
    private String synApproveConfigEnabled;

    private static List<ApproveConfig> approveConfigList = new ArrayList<>(0);

    @Scheduled(fixedRate = 1000*60*5)
    public void synApproveConfig() {
        if (!Boolean.parseBoolean(synApproveConfigEnabled)) {
            return;
        }
        approveConfigList = approveMapper.getApproveConfigList();
        for (ApproveConfig approveConfig : approveConfigList) {
            try{
                GetUserIdListByRoleReq req = new GetUserIdListByRoleReq();
                req.setRole(approveConfig.getApproverValue());
                Response<Set<Long>> response = accountApi.getUserIdListByRole(req,tenantCode);
                log.info("synApproveConfig,getUserIdListByRole,response:{}",response);
                if (response != null || response.getData() != null){
                    approveConfig.setApproverUidList(response.getData());
                }
                log.info("synApproveConfig success,approveConfig:{}", approveConfig);
            }catch (Exception e){
                log.error("获取审批人列表失败",e);
            }
        }
        log.info("synApproveConfig success, size: {}", approveConfigList.size());
    }

    private ApproveConfig getApproveConfig(ImageInfo imageInfo) {
        if (approveConfigList.size() == 0 || imageInfo == null) {
            return null;
        }
        for (ApproveConfig config : approveConfigList) {
            ApproveTaskType taskType = ApproveTaskType.of(config.getTaskType());
            if (taskType == null){
                return null;
            }
            switch (taskType) {
                case IMAGE_VERSION:
                    if (imageInfo.getVersion().equals(config.getTaskValue())) {
                        return config;
                    }
                    break;
                case IMAGE_GROUP:
                    if (imageInfo.getImgGroup().equals(config.getTaskValue())) {
                        return config;
                    }
                    break;
                case IMAGE:
                    if (imageInfo.getId().toString().equals(config.getTaskValue())) {
                        return config;
                    }
                    break;
                default:
                    return null;
            }
        }
        return null;
    }

    @Transactional
    @Override
    public boolean requestApprove(RequestApproveReq req) {
        ImageInfo imageInfo = imageInfoMapper.getImageInfo(req.getImageId());
        ApproveConfig config = getApproveConfig(imageInfo);
        if (config == null){
            throw new BaseException(WeTagErrorCode.APPROVE_NO_APPROVER);
        }
        if (CollectionUtils.isEmpty(config.getApproverUidList())){
            throw new BaseException(WeTagErrorCode.APPROVE_NO_APPROVER);
        }
        ApproveTask task = new ApproveTask();
        task.setImageVersion(imageInfo.getVersion());
        task.setImageId(imageInfo.getId());
        task.setImageGroup(imageInfo.getImgGroup());
        task.setRequestUid(req.getUid());
        task.setStartTime(new Date());
        task.setApproveNum(config.getRequireNum());
        try{
            int id = approveMapper.insertApproveTask(task);
        }catch (DuplicateKeyException e){
            throw new BaseException(WeTagErrorCode.APPROVE_REQUEST_DUPLICATE);
        }

        // 确定ID是多少
        List<ApproveTaskLog> taskLogList = config.getApproverUidList().stream().map(uid->{
            ApproveTaskLog taskLog = new ApproveTaskLog();
            taskLog.setTaskId(task.getId());
            taskLog.setApproverUid(uid);
            taskLog.setApproveStatus(ApproveStatus.WAITING.getCode());
            return taskLog;
        }).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(taskLogList)){
            throw new BaseException(WeTagErrorCode.APPROVE_NO_APPROVER);
        }
        approveMapper.batchInsertTaskLog(taskLogList);
        imageInfoMapper.changeImageLabelState(req.getImageId(), ImageLabelStateEnum.WAIT_APPROVE.getCode());
        return true;
    }

    @Transactional
    @Override
    public boolean approveImage(ApproveImageReq req) {
        ApproveTaskLog taskLog = approveMapper.getTaskLog(req);
        if (taskLog == null){
            throw new BaseException(WeTagErrorCode.APPROVE_TASK_NOT_EXIST);
        }
        taskLog.setApproveTime(new Date());
        taskLog.setRemark(req.getRemark());
        taskLog.setApproveTime(new Date());
        if (req.getIsPass()){
            taskLog.setApproveStatus(ApproveStatus.SUCCESS.getCode());
            ApproveTask task = approveMapper.getTask(taskLog.getTaskId());
            if (task == null){
                throw new BaseException(WeTagErrorCode.APPROVE_TASK_NOT_EXIST);
            }
            Integer oldApproveNum = task.getApproveNum();
            task.setApproveNum(task.getApproveNum()==null ? 0: task.getApproveNum());
            task.setApproveNum(Math.max(0, task.getApproveNum()-1));

            // 如果已经满足了审批通过的条件
            if (0 == task.getApproveNum()){
                task.setDeleteId(task.getId());
                task.setEndTime(new Date());
                imageInfoMapper.changeImageLabelState(req.getImageId(), ImageLabelStateEnum.HAD_TAG.getCode());
                approveMapper.setAllTaskLogPass(taskLog.getTaskId());
            }
            approveMapper.updateApproveTask(task, oldApproveNum);
        }else{
            taskLog.setApproveStatus(ApproveStatus.FAIL.getCode());
            imageInfoMapper.changeImageLabelState(req.getImageId(), ImageLabelStateEnum.NOT_TAG.getCode());
        }
        taskLog.setDeleteId(taskLog.getId());
        approveMapper.updateTaskLog(taskLog);
        return true;
    }

    @Override
    public List<RequestApproveReq> getMyRequestList(Long uid) {
        return null;
    }

    @Override
    public List<ImageVersionVo> getAllImageVersion(GetAllImageVersionReq req) {
        List<ImageInfo> imageInfoList = approveMapper.getAllImageVersionInfo(req);
        if (CollectionUtils.isEmpty(imageInfoList)){
            return null;
        }
        Map<String, ImageVersionVo> imageVersionVoMap = new HashMap<>();
        for (ImageInfo imageInfo : imageInfoList) {
            ImageVersionVo imageVersionVo = imageVersionVoMap.get(imageInfo.getImgGroup());
            if (imageVersionVo == null){
                imageVersionVo = new ImageVersionVo();
                imageVersionVo.setVersion(imageInfo.getVersion());
                imageVersionVo.setImageGroupVoList(new ArrayList<>());
                imageVersionVoMap.put(imageInfo.getImgGroup(), imageVersionVo);
            }
            ImageGroupVo imageGroupVo = new ImageGroupVo();
            imageGroupVo.setGroupName(imageInfo.getImgGroup());
            imageVersionVo.getImageGroupVoList().add(imageGroupVo);
        }
        return imageVersionVoMap.values().stream().collect(Collectors.toList());
    }

    @Override
    public PageHelperUtil<ImageInfoVo> getImageInfoPage(GetImagePageReq req) {
        PageHelper.startPage(req.getPage(), req.getRows());
        Page<ImageInfoVo> imageInfoVoPage = approveMapper.getImageInfoPage(req);
        List<Long> imageIds = imageInfoVoPage.getResult().stream().map(ImageInfoVo::getId).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(imageIds)){
            return new PageHelperUtil<>();
        }
        List<ImageInfo> imageInfoList = imageInfoMapper.getImageByIdList(imageIds);
        Map<Long, ImageInfo> imageInfoMap = imageInfoList.stream().collect(Collectors.toMap(ImageInfo::getId, imageInfo -> imageInfo));

        Map<Long, UserImage> imageIdUserMap = redisService.getAllLockedImageIds();
        for(ImageInfoVo vo: imageInfoVoPage.getResult()){
            ImageInfo info = imageInfoMap.get(vo.getId());
            vo.setLabelNum(info==null ? 0 : info.getLabelNum());
            vo.setImgUrl(info==null ? "" : imgUrlPrefix + info.getUri());
            vo.setImgSrcName(info==null ? "" : info.getImgSrcName());
            vo.setLockState(LockStateEnum.FREE);
            vo.setLabelState(ImageLabelStateEnum.WAIT_APPROVE.getCode());
            vo.setImgHeight(info==null ? 0 : info.getImgWidth());
            vo.setImgWidth(info==null ? 0 : info.getImgWidth());
            if (imageIdUserMap.containsKey(vo.getId())){
                UserImage userImage = imageIdUserMap.get(vo.getId());
                if (userImage.getUid().equals(req.getUid())) {
                    vo.setLockState(LockStateEnum.OWNED);
                }else{
                    vo.setLockState(LockStateEnum.LOCKED);
                }
            }
        }

        PageHelperUtil<ImageInfoVo> imageInfoVoPageHelperUtil = new PageHelperUtil<>();
        imageInfoVoPageHelperUtil.setList(imageInfoVoPage.getResult());
        imageInfoVoPageHelperUtil.setTotal(imageInfoVoPage.getTotal());
        Integer pageTotal = imageInfoVoPage.getTotal() == 0 ? 0 : (int) (imageInfoVoPage.getTotal()/req.getRows() + 1);
        imageInfoVoPageHelperUtil.setPageTotal(pageTotal);
        imageInfoVoPageHelperUtil.setPage(imageInfoVoPage.getPageNum());
        imageInfoVoPageHelperUtil.setPageSize(imageInfoVoPage.getPageSize());
        return imageInfoVoPageHelperUtil;
    }

}
