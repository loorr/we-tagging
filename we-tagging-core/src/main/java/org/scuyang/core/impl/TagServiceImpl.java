package org.scuyang.core.impl;

import cn.dev33.satoken.stp.StpUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.tove.web.infra.common.BaseErrorCode;
import com.tove.web.infra.common.BaseException;
import com.tove.web.infra.common.PageHelperUtil;
import com.tove.web.infra.common.PageResult;
import org.scuyang.common.enums.ImageLabelStateEnum;
import org.scuyang.common.model.ImageGroupInfo;
import org.scuyang.common.model.ImageInfo;
import org.scuyang.common.model.ImageLabel;
import org.scuyang.common.model.ModelConfig;
import org.scuyang.common.ws.domain.Do.UserImage;
import org.scuyang.common.ws.domain.vo.LockStateEnum;
import org.scuyang.core.ModelConfigService;
import org.scuyang.core.RedisService;
import org.scuyang.core.TagService;
import org.scuyang.dao.ImageGroupInfoMapper;
import org.scuyang.dao.ImageInfoMapper;
import org.scuyang.dao.LabelInfoMapper;
import org.scuyang.oss.api.req.GetImageLabelPageAdminReq;
import org.scuyang.oss.api.vo.ImageLabelVo;
import org.scuyang.rest.api.error.WeTagErrorCode;
import org.scuyang.rest.api.error.WeTagException;
import org.scuyang.rest.api.req.*;
import org.scuyang.rest.api.req.label.GetImagePageReq;
import org.scuyang.rest.api.vo.*;
import org.scuyang.rest.api.vo.tag.ImageInfoVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Service
public class TagServiceImpl implements TagService {

    @Value("${oss.img.url}")
    private String imgUrlPrefix;

    private Integer historyMaxNum = 10;

    @Resource
    private LabelInfoMapper labelInfoMapper;

    @Resource
    private ImageInfoMapper imageInfoMapper;

    @Resource
    private ImageGroupInfoMapper imageGroupInfoMapper;

    @Resource
    private RedisService redisService;

    @Resource
    private ModelConfigService modelConfigService;

    private final ExecutorService executorService = Executors.newFixedThreadPool(8);

    @Override
    public Boolean saveTagInfo(SaveTagInfoReq req) {
        ImageLabel imageLabel = new ImageLabel();
        BeanUtils.copyProperties(req, imageLabel);
        try {
            labelInfoMapper.insert(imageLabel);
        } catch (DuplicateKeyException e) {
            throw new WeTagException(WeTagErrorCode.DUPLICATE_KEY);
        }
        imageInfoMapper.addOneImageLabelNum(req.getImgId());
        return true;
    }

    @Transactional
    @Override
    public Boolean saveAllTagInfo(SaveAllTageInfoReq req) {
        boolean imageState = imageInfoMapper.getImageLabelState(req.getImgId());
        if (imageState){
            throw new BaseException(WeTagErrorCode.IMAGE_TAG_FINISH);
        }

        labelInfoMapper.deleteLabels(req.getImgId());
        if (CollectionUtils.isEmpty(req.getLabelItemVos())) {
           return true;
        }
        Date historyTime = new Date();
        List<ImageLabel> infoList = new ArrayList<>();
        for (ImageLabelItemVo i : req.getLabelItemVos()){
            ImageLabel imageLabel = new ImageLabel();
            imageLabel.setImgId(req.getImgId());
            BeanUtils.copyProperties(i, imageLabel);
            imageLabel.setTime(historyTime);
            imageLabel.setLabelExt(i.getLabelExt());
            infoList.add(imageLabel);
        }
        try {
            labelInfoMapper.batchInsert(infoList);
            executorService.submit(()->saveImageLabelHistory(req, infoList));
        }catch (DuplicateKeyException e){
            throw new WeTagException(WeTagErrorCode.DUPLICATE_KEY);
        }
        executorService.submit(()->{
            imageInfoMapper.updateImageLabelNumAndModifyUid(req.getLastModifyUsername(), infoList.size(), req.getImgId());
            imageInfoMapper.updateImageExt(req.getImgExt(), req.getImgId());
        });
        return true;
    }

    private void saveImageLabelHistory(SaveAllTageInfoReq req, List<ImageLabel> infoList){
        List<ImageLabel> history =  labelInfoMapper.getLastHistory(req.getImgId());
        //  比较是否有改变
        if (history.size() == infoList.size()){
            Map<String, ImageLabel> newMap = new HashMap<>(infoList.size());
            for (ImageLabel item: infoList){
                newMap.put(getKey(item), item);
            }
            for (ImageLabel item: history){
                ImageLabel newItem = newMap.get(getKey(item));
                if (newItem==null || !newItem.isEqualContent(item)){
                    labelInfoMapper.batchInsertHistory(infoList, req.getLastModifyUsername());
                    break;
                }
            }
        }else{
            labelInfoMapper.batchInsertHistory(infoList, req.getLastModifyUsername());
        }
        labelInfoMapper.deleteHistory(req.getImgId(), historyMaxNum);
    }

    private String getKey(ImageLabel item){
        return String.format("%s_%s_%s_%s_%s_%s", item.getCategoryId(), item.getClassId(),
                item.getCoordinateX().setScale(15, RoundingMode.HALF_UP),
                item.getCoordinateY().setScale(15, RoundingMode.HALF_UP),
                item.getWidth().setScale(15, RoundingMode.HALF_UP),
                item.getHeight().setScale(15, RoundingMode.HALF_UP)
        );
    }

    @Override
    public Boolean deleteTagInfo(DeleteTagInfoReq req) {
        ImageLabel imageLabel = new ImageLabel();
        try {
            BeanUtils.copyProperties(req, imageLabel);
        }catch (Exception e) {
            throw new WeTagException(BaseErrorCode.ILLEGAL_PARAMETERS);
        }
        int rows = labelInfoMapper.delete(imageLabel);
        if (rows == 0){
            throw new WeTagException(WeTagErrorCode.NO_RECORD_MATCH);
        }
        imageInfoMapper.subOneImageLabelNum(imageLabel.getImgId());
        return true;
    }

    @Override
    public PageHelperUtil<ImageInfoVo> getImageInfoPage(GetImagePageReq req) {
        req.setRows(checkAndGetRows(req.getRows()));
        PageHelper.startPage(req.getPage(), req.getRows(), req.getOrderStatement());
        Page<ImageInfoVo> imageInfoVoPage = imageInfoMapper.selectImageInfoVo(req);

        Map<Long, UserImage> imageIdUserMap = redisService.getAllLockedImageIds();
        PageHelperUtil<ImageInfoVo> imageInfoVoPageHelperUtil = new PageHelperUtil<>();
        Long uid = Long.valueOf(String.valueOf(StpUtil.getLoginId()));
        List<ImageInfoVo> imageInfoVoList = imageInfoVoPage.getResult().stream()
                .map(o->{
                    o.setImgUrl(imgUrlPrefix + o.getImgUrl());
                    // 图片是否被加锁
                    o.setLockState(LockStateEnum.FREE);
                    if (imageIdUserMap.containsKey(o.getId())){
                        UserImage userImage = imageIdUserMap.get(o.getId());
                        if (userImage.getUid().equals(uid)) {
                            o.setLockState(LockStateEnum.OWNED);
                        }else{
                            o.setLockState(LockStateEnum.LOCKED);
                        }
                    }
                    return o;
                })
                .collect(Collectors.toList());
        
        imageInfoVoPageHelperUtil.setList(imageInfoVoList);
        imageInfoVoPageHelperUtil.setTotal(imageInfoVoPage.getTotal());
        Integer pageTotal = imageInfoVoPage.getTotal() == 0 ? 0 : (int) (imageInfoVoPage.getTotal()/req.getRows() + 1);
        imageInfoVoPageHelperUtil.setPageTotal(pageTotal);
        imageInfoVoPageHelperUtil.setPage(imageInfoVoPage.getPageNum());
        imageInfoVoPageHelperUtil.setPageSize(imageInfoVoPage.getPageSize());
        return imageInfoVoPageHelperUtil;
    }
    private String addString(String uri){
      return imgUrlPrefix + uri;
    };

    @Override
    public ImageLabelInfoVo getImageLabelInfo(GetImageLabelInfoReq req) {
        List<ImageLabel> imageLabels = labelInfoMapper.getImageLabelList(req);
        if (imageLabels == null){
            imageLabels = new ArrayList<>();
        }
        ImageInfo imageInfo = imageInfoMapper.getImageInfo(req.getImgId());
        if (imageInfo == null){
            throw new WeTagException(WeTagErrorCode.NO_IMAGE);
        }
        ImageLabelInfoVo imageLabelInfoVo = new ImageLabelInfoVo();
        imageLabelInfoVo.setImgId(imageInfo.getId());
        imageLabelInfoVo.setImgUrl(imageInfo.getUri());
        imageLabelInfoVo.setImgSrcName(imageInfo.getImgSrcName());
        imageLabelInfoVo.setImgExt(imageInfo.getImageExt());
        imageLabelInfoVo.setLastModifyUsername(imageInfo.getLastModifyUsername());
        List<ImageLabelItemVo> imageLabelItemVos = imageLabels.stream().map(o->{
            ImageLabelItemVo imageLabelItemVo = new ImageLabelItemVo();
            BeanUtils.copyProperties(o, imageLabelItemVo);
            return imageLabelItemVo;
        }).collect(Collectors.toList());
        imageLabelInfoVo.setImageLabels(imageLabelItemVos);

        if (req.getNeedHistory() != null && req.getNeedHistory()){
            List<ImageLabel> historyLabel = labelInfoMapper.getHistoryByImgId(req.getImgId());
            Map<Long,HistoryItem> map = new HashMap<>(10);
            for(ImageLabel label: historyLabel){
                Long timeKey = label.getTime().getTime();
                if (!map.containsKey(timeKey)){
                    HistoryItem item = new HistoryItem();
                    item.setImageLabels(new ArrayList<>());
                    item.setTimestamp(timeKey);
                    item.setLastModifyUsername(label.getImgModifyUsername());
                    map.put(timeKey, item);
                }
                HistoryItem historyItem = map.get(timeKey);
                ImageLabelItemVo vo = new ImageLabelItemVo();
                BeanUtils.copyProperties(label, vo);
                historyItem.getImageLabels().add(vo);
            }
            List<HistoryItem> historyItems = map.values()
                    .stream()
                    .sorted((o1,o2)-> (int) (o2.getTimestamp() - o1.getTimestamp()))
                    .collect(Collectors.toList());
            imageLabelInfoVo.setHistoryItems(historyItems);
        }
        // 算法标签
        if (req.getNeedAlgorithmLabel() != null && req.getNeedAlgorithmLabel()){
            req.setImageVersion(imageInfo.getVersion());
            fileAlgoLabel(req, imageLabelInfoVo);
        }
        return imageLabelInfoVo;
    }

    public void fileAlgoLabel(GetImageLabelInfoReq req, ImageLabelInfoVo result){
        List<ModelConfig> models = modelConfigService.getModelConfig(req.getImageVersion());
        List<AlgoLabelVo> algoLabelVos = new ArrayList<>();

        for (ModelConfig model : models){
            List<ImageLabel> modelLabel = labelInfoMapper.getAlgorithmLabels(req.getImgId(), model.getModelCode(), model.getModelVersion());
            List<ImageLabelItemVo> modelLabelItemVos = modelLabel.stream().map(o->{
                ImageLabelItemVo imageLabelItemVo = new ImageLabelItemVo();
                BeanUtils.copyProperties(o, imageLabelItemVo);
                return imageLabelItemVo;
            }).collect(Collectors.toList());
            AlgoLabelVo algoLabelVo = new AlgoLabelVo();
            algoLabelVo.setAlgorithmLabels(modelLabelItemVos);
            algoLabelVo.setModelCode(model.getModelCode());
            algoLabelVo.setModelVersion(model.getModelVersion());

            algoLabelVos.add(algoLabelVo);
        }
        result.setAlgoLabelVo(algoLabelVos);
    }

    @Override
    public Boolean confirmImageHaveTag(ConfirmImageHaveTagReq req) {
        ImageInfo imageInfo = imageInfoMapper.getImageInfo(req.getImgId());
        if (imageInfo == null || imageInfo.getMd5()==null){
            throw new WeTagException(WeTagErrorCode.NO_RECORD_MATCH);
        }
        ImageLabelStateEnum labelStateEnum = ImageLabelStateEnum.of(req.getState());
        int rows = imageInfoMapper.changeImageLabelState(req.getImgId(), labelStateEnum.getCode());
        return rows>0;
    }

    @Override
    public Boolean addImageGroupNote(AddImageGroupNoteReq req) {
        if (req.getImgId() != null){
            ImageInfo imageInfo = checkIsExistImageGroup(req.getImgId());
            req.setImgGroup(imageInfo.getImgGroup());
            req.setVersion(imageInfo.getVersion());
        }else {
            checkIsExistImageGroup(req.getVersion(),req.getImgGroup());
        }
        ImageGroupInfo imageGroupInfo = new ImageGroupInfo(req.getVersion(),req.getImgGroup(), req.getDescribe());
        imageGroupInfoMapper.replaceNote(imageGroupInfo);
        return true;
    }

    @Override
    public ImageGroupNoteVo getImageGroupNote(GetImageGroupNoteReq req) {
        if (req.getImgId() != null){
            ImageInfo imageInfo = checkIsExistImageGroup(req.getImgId());
            req.setImgGroup(imageInfo.getImgGroup());
            req.setVersion(imageInfo.getVersion());
        }else {
            checkIsExistImageGroup(req.getVersion(),req.getImgGroup());
        }
        ImageGroupInfo imageGroupInfo = imageGroupInfoMapper.getNote(req.getVersion(), req.getImgGroup());
        if(imageGroupInfo == null){
            return null;
        }
        ImageGroupNoteVo result = new ImageGroupNoteVo();
        BeanUtils.copyProperties(imageGroupInfo, result);
        return result;
    }

    @Override
    public PageResult<ImageLabelVo> getImageLabelPage(GetImageLabelPageAdminReq req) {
       if (req.getNeedPage()){
           PageHelper.startPage(req.getPage(), req.getRows());
       }
        Page<ImageLabelVo> page = labelInfoMapper.getImageLabelPage(req);
        PageResult<ImageLabelVo> result = new PageResult<>(page.getTotal(), page.getResult());
        return result;
    }

    /**
     * 分页接口参数校验
     * TODO 后续需要在example.commom中更新这个
     * @param row
     * @return
     */
    private int checkAndGetRows(Integer row){
        if (row==null || row == 0){
            return 10;
        }
        return row;
    }

    private ImageInfo checkIsExistImageGroup(Long id){
        ImageInfo imageInfo = imageInfoMapper.getImageInfo(id);
        if (imageInfo == null){
            throw new WeTagException(WeTagErrorCode.NO_RECORD_MATCH);
        }
        return imageInfo;
    }

    private void checkIsExistImageGroup(String version, String imageGroup){
        if (imageInfoMapper.isExistImageGroup(version, imageGroup) == 0){
            throw new WeTagException(WeTagErrorCode.NO_RECORD_MATCH);
        }
    }
}
