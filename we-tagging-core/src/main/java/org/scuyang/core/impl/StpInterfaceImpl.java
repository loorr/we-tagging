package org.scuyang.core.impl;

import cn.dev33.satoken.stp.StpInterface;
import org.example.account.center.api.entity.AccountVo;
import org.scuyang.core.AuthService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class StpInterfaceImpl implements StpInterface {
    @Resource
    private AuthService authService;

    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        Long uid = Long.valueOf(String.valueOf(loginId));
        AccountVo accountVo = authService.getAccountInfo(uid);
        return accountVo.getDefaultPermissions();
    }

    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        Long uid = Long.valueOf(String.valueOf(loginId));
        AccountVo accountVo = authService.getAccountInfo(uid);
        return accountVo.getDefaultRoles();
    }

}
