package org.scuyang.core.constant;


import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum AsynTaskName {

    ADD_BATCH_IMAGES_BY_LOCAL_ZIP("批量导入图片，通过本地图片压缩包"),
    ADD_BATCH_IMAGES_BY_UPLOAD_ZIP("批量导入图片，通过上传图片压缩包"),
    EXPORT_IMAGE_LABELS_AND_IMAGE("导出图片和图片标签"),
    ;

    private String name;

}
