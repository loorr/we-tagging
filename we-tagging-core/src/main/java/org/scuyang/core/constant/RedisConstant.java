package org.scuyang.core.constant;

public interface RedisConstant {
    /** message queue */
    String MODEL_TASK_QUEUE = "tagging-model-queue";

    /** ws online */
    String ONLINE_USER_UID = "user:%s";

    /** 模型识别队列 */
    String IMAGE_ID_SET = "image-id:%s";
    String IMAGE_GROUP_SET = "image-group:%s";

    /** 分布式锁 */
    String IMAGE_LOCK = "image-lock:%s";

    /** map<imageId:UserImage> */
    String USER_LOCK_IMAGES = "user-lock-images";
}
