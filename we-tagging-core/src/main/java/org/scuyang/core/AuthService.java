package org.scuyang.core;

import org.example.account.center.api.entity.AccountVo;
import org.scuyang.rest.api.req.LoginReq;
import org.scuyang.rest.api.req.SignReq;
import org.scuyang.rest.api.req.auth.ModifyUserInfoReq;
import org.scuyang.rest.api.req.auth.UserInfo;

import java.util.List;

public interface AuthService {
    UserInfo login(LoginReq req);

    UserInfo getUserInfo();

    Long sign(SignReq req);

    boolean modifyUserInfo(ModifyUserInfoReq req);

    boolean checkRoles(List<String> roles);

    AccountVo getAccountInfo(Long uid);
}
