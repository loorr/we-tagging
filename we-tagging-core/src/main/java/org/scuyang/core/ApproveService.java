package org.scuyang.core;


import com.tove.web.infra.common.PageHelperUtil;
import org.scuyang.rest.api.req.image.GetImageGroupsReq;
import org.scuyang.rest.api.req.label.GetImagePageReq;
import org.scuyang.rest.api.req.approve.ApproveImageReq;
import org.scuyang.rest.api.req.approve.RequestApproveReq;
import org.scuyang.rest.api.req.image.GetAllImageVersionReq;
import org.scuyang.rest.api.vo.ImageGroupVo;
import org.scuyang.rest.api.vo.ImageVersionVo;
import org.scuyang.rest.api.vo.tag.ImageInfoVo;

import java.util.List;

public interface ApproveService {

    boolean requestApprove(RequestApproveReq req);

    boolean approveImage(ApproveImageReq req);
    List<RequestApproveReq> getMyRequestList(Long uid);

    List<ImageVersionVo> getAllImageVersion(GetAllImageVersionReq req);

    PageHelperUtil<ImageInfoVo> getImageInfoPage(GetImagePageReq req);
}
