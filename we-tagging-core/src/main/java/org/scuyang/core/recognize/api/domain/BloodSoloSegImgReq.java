package org.scuyang.core.recognize.api.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BloodSoloSegImgReq {
    private String image_path;
    private String image_name;
    private String uuid;
    private String env = "dev";
}
