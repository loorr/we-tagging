package org.scuyang.core.recognize;

import lombok.extern.slf4j.Slf4j;
import org.scuyang.common.model.ImageInfo;
import org.scuyang.core.oss.ImportService;
import org.scuyang.core.recognize.api.RecognizeApi;
import org.scuyang.core.recognize.api.domain.BloodRet;
import org.scuyang.core.recognize.api.domain.BloodSoloSegImgReq;
import org.scuyang.dao.ImageInfoMapper;
import org.scuyang.oss.api.req.SingleImageRecognizeReq;
import org.scuyang.rest.api.error.WeTagErrorCode;
import org.scuyang.rest.api.error.WeTagException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.UUID;

@Slf4j
@Service("bloodService")
public class BloodService implements RecognizeFactory {

    private RecognizeApi recognizeApi;

    @Resource
    private ImageInfoMapper imageInfoMapper;

    private final String rootPath = "/home/image_root/ossRoot";

    @Value("${app-env:dev}")
    private String appEnv;

    @Resource
    private ImportService importService;

    @Override
    public void singleImageRecognize(SingleImageRecognizeReq req) {
        log.info("BloodService singleImageRecognize: {}", req);
        String uuid = UUID.randomUUID().toString();
        ImageInfo imageInfo = imageInfoMapper.getImageInfo(req.getImgId());
        if (imageInfo == null){
            throw new WeTagException(WeTagErrorCode.NO_IMAGE);
        }

        String newUri = imageInfo.getUri().replace("//","/");
        req.setImageName(imageInfo.getMd5());
        req.setImagePath(rootPath + newUri);
        BloodSoloSegImgReq soloSegImgReq = new BloodSoloSegImgReq(req.getImagePath(), req.getImageName(),uuid, appEnv);
        BloodRet ret = recognizeApi.getBloodResult(soloSegImgReq);
        log.info("BloodService singleImageRecognize: {}", ret);
        if (ret == null){
            log.warn("BloodService singleImageRecognize fail");
            return;
        }
        importService.importImageLabelFromTxt(ret.getLabel_file());
    }
}
