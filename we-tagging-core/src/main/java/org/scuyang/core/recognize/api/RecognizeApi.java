package org.scuyang.core.recognize.api;

import org.scuyang.core.recognize.api.domain.BloodSoloSegImgReq;
import org.scuyang.core.recognize.api.domain.BloodRet;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "oss-algorithm-service-2",url = "${feign.oss-algorithm-service-2}")
public interface RecognizeApi {

    @PostMapping(value = "/solo-seg-img", produces = MediaType.APPLICATION_JSON_VALUE)
    BloodRet getBloodResult(BloodSoloSegImgReq req);

}
