package org.scuyang.core.recognize;

import org.scuyang.oss.api.req.SingleImageRecognizeReq;

public interface RecognizeFactory {
    void singleImageRecognize(SingleImageRecognizeReq req);
}
