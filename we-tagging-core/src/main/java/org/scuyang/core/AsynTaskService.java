package org.scuyang.core;

import org.scuyang.common.enums.TaskNameEnum;
import org.scuyang.common.enums.TaskStateEnum;
import org.scuyang.common.model.AsynTask;

import java.util.List;

public interface AsynTaskService {

    void addTask(AsynTask asynTask);

    void startNewTask(TaskNameEnum taskNameEnum);

    void updateTask(AsynTask asynTask);

    AsynTask getTask(Integer id);

    List<AsynTask> getTaskList(TaskNameEnum taskNameEnum, TaskStateEnum taskStateEnum);

    void getTaskState(Integer id);

}
