package org.scuyang.core;

import com.tove.web.infra.common.PageResult;
import org.scuyang.oss.api.req.AddOrUpdateLabelAdminReq;
import org.scuyang.oss.api.req.DeleteLabelAdminReq;
import org.scuyang.oss.api.req.GetLabelInfoPageAdminReq;
import org.scuyang.oss.api.vo.LabelConfigOssVo;
import org.scuyang.rest.api.req.AddOrUpdateLabelReq;
import org.scuyang.rest.api.req.DeleteLabelReq;
import org.scuyang.rest.api.req.GetLabelConfigReq;
import org.scuyang.rest.api.req.config.AddOrUpdateCategoryLabelReq;
import org.scuyang.rest.api.req.feature.DeleteImageFeatureConfigReq;
import org.scuyang.rest.api.req.image.GetImageGroupsReq;
import org.scuyang.rest.api.req.label.*;
import org.scuyang.rest.api.vo.ImageGroupVo;
import org.scuyang.rest.api.vo.ImageVersionVo;
import org.scuyang.rest.api.vo.LabelConfigVo;
import org.scuyang.rest.api.vo.LabelUsedVo;
import org.scuyang.rest.api.vo.LabelVersionVo;

import java.util.List;

public interface LabelConfigService {
    LabelConfigVo getLabelConfig(GetLabelConfigReq req);

    List<LabelVersionVo> getAllLabelVersion(GetAllLabelVersionReq req);

    List<ImageVersionVo> getAllImageVersion();

    boolean addLabel(AddOrUpdateLabelReq req);

    boolean updateLabel(AddOrUpdateLabelReq req);

    boolean deleteLabel(DeleteLabelReq req);

    boolean updateCategoryLabel(AddOrUpdateCategoryLabelReq req);

    boolean addCategoryLabel(AddOrUpdateCategoryLabelReq req);

    /** admin */
    boolean addLabelInfo(AddOrUpdateLabelAdminReq req);

    boolean updateLabelInfo(AddOrUpdateLabelAdminReq req);

    boolean deleteLabelInfo(DeleteLabelAdminReq req);

    PageResult<LabelConfigOssVo> getLabelInfoPage(GetLabelInfoPageAdminReq req);

    boolean addLabelVersionSet(AddLabelVersionSetReq req) ;

    boolean deleteLabelVersionSet(DeleteImageFeatureConfigReq req);

    boolean updateLabelVersionSet(UpdateLabelVersionSetReq req);

    Boolean userLabelConfigUpdate(UserLabelConfigUpdateReq req);

    LabelConfigVo userLabelConfigGet(UserLabelConfigGetReq req);

    Boolean resetUserLabelConfig(ResetUserLabelConfigReq req);

    LabelUsedVo checkLabelUsed(CheckLabelUsedReq req);

    Boolean replaceLabel(ReplaceLabelReq req);

    Boolean labelResort(LabelResortReq req);

    List<ImageGroupVo> getImageGroups(GetImageGroupsReq req);
}
