package org.scuyang.core.mq;

import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBlockingQueue;
import org.redisson.api.RedissonClient;
import org.scuyang.core.constant.RedisConstant;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Slf4j
@Service
public class RedisQueueMsgProducer {

    @Resource(name = "weTaggingRedissonClient")
    private RedissonClient redissonClient;

    public void sendData(String data){
        RBlockingQueue<String> blockingQueue = redissonClient.getBlockingQueue(RedisConstant.MODEL_TASK_QUEUE);
        if (blockingQueue == null){
            log.error("blockingQueue is null");
        }
        try {
            blockingQueue.put(data);
            log.info("queue " + data.length() + " bytes");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
