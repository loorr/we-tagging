package org.scuyang.core;

import com.tove.web.infra.common.PageResult;
import org.scuyang.oss.api.req.GetImagePageAdminReq;
import org.scuyang.oss.api.vo.ImageVo;
import org.scuyang.rest.api.req.GetImageFeatureReq;
import org.scuyang.rest.api.req.UpdateImageFeatureReq;
import org.scuyang.rest.api.vo.ImageFeatureVo;

public interface ImageService {
    ImageFeatureVo getImageFeature(GetImageFeatureReq req);

    Boolean updateImageFeature(UpdateImageFeatureReq req);

    PageResult<ImageVo> getImagePage(GetImagePageAdminReq req);


}
