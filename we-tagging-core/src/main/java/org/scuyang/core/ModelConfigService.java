package org.scuyang.core;

import org.scuyang.common.model.ModelConfig;
import org.scuyang.dao.ModelConfigMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class ModelConfigService {
    @Value("${we-tagging.model-config-refresh-enabled:false}")
    private String modelConfigRefreshEnabled;

    @Resource
    private ModelConfigMapper modelConfigMapper;

    private final HashMap<String, List<ModelConfig>> modelConfigMap = new HashMap<>();

    @Scheduled(fixedDelay = 1000 * 60)
    public void refreshModelConfig() {
        if (!Boolean.parseBoolean(modelConfigRefreshEnabled)) {
            return;
        }
        List<ModelConfig> modelConfigs = modelConfigMapper.getAllModelConfig();
        HashMap<String, List<ModelConfig>> newModelConfigMap = new HashMap<>();
        for (ModelConfig modelConfig : modelConfigs) {
            List<ModelConfig> modelConfigList = newModelConfigMap.getOrDefault(modelConfig.getVersion(), new ArrayList<>());
            modelConfigList.add(modelConfig);
            newModelConfigMap.put(modelConfig.getVersion(), modelConfigList);
        }
        synchronized (modelConfigMap) {
            modelConfigMap.clear();
            modelConfigMap.putAll(newModelConfigMap);
        }
    }

    public List<ModelConfig> getAllModelConfig() {
        return modelConfigMapper.getAllModelConfig();
    }

    public List<ModelConfig> getModelConfig(String imageVersion) {
        if (!StringUtils.hasLength(imageVersion)) {
            return null;
        }
        if (Boolean.parseBoolean(modelConfigRefreshEnabled)) {
            return modelConfigMap.getOrDefault(imageVersion, new ArrayList<>());
        } else {
            return modelConfigMapper.getModelConfig(imageVersion);
        }
    }

    public ModelConfig getModelConfigByCode(String imageVersion, String modelCode){
        if (!StringUtils.hasLength(modelCode)) {
            return null;
        }
        return modelConfigMapper.getModelConfigByModelCodeAndVersion(imageVersion, modelCode);
    }
}
