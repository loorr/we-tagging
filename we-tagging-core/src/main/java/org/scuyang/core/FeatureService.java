package org.scuyang.core;

import org.scuyang.rest.api.req.GetImageFeatureReq;
import org.scuyang.rest.api.req.feature.*;
import org.scuyang.rest.api.vo.feature.FeatureItem;
import org.scuyang.rest.api.vo.feature.FeatureVo;

import java.util.List;

public interface FeatureService {

    FeatureVo getImageFeature(GetImageFeatureReq req);

    Boolean saveImageFeatureData(SaveImageFeatureReq req);

    Boolean addImageFeatureConfig(AddImageFeatureConfigReq req);

    Boolean deleteImageFeatureConfig(DeleteImageFeatureConfigReq req);

    List<FeatureItem> getImageFeatureConfig(GetImageFeatureConfigReq req);

    Boolean updateImageFeatureConfig(UpdateImageFeatureConfigReq req);

    FeatureVo getImageGroupFeature(GetImageGroupFeatureReq req);

    Boolean saveImageGroupFeature(SaveImageGroupFeatureReq req);
}
