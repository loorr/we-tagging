
## Websockt Server

地址: `ws://127.0.0.1:8080/we-tagging-ws/<uid>`

1. 消息格式
```json
{
  "type": "<message-type>",
  "uid": "long 类型 <uid>",
  "data": "<message-object>"
}
```

2. 消息类型包括
```json
LOCK_IMAGE(String.class, LockImageArg.class),
UNLOCK_IMAGE(String.class, UnLockImageArg.class),
LOCK_STATE(String.class, GetLockStateArg.class),
```
+ LOCK_IMAGE 图片锁定
```json
{
  "type": "LOCK_IMAGE",
  "data": {
    "image_id": 12345
  }
}
```
`响应体: 成功`
```json
{
  "code":"200",
  "data":{
    "imageId":12345,
    "lockState":"OWNED"
  },
  "msg":"OK",
  "type":"LOCK_IMAGE"
}


```
`响应体: 失败`
```json
{
  "code":"30002",
  "msg":"lock image fail"
}
```

+ UNLOCK_IMAGE 图片解锁

`请求参数`
```json
{
  "type": "UNLOCK_IMAGE",
  "data": {
    "image_id": 12345
  }
}
```
`响应体: 成功`
```json
{
  "code":"200",
  "data":{
    "imageId":12345,
    "lockState":"FREE"
  },
  "msg":"OK",
  "type":"LOCK_IMAGE"
}
```

    
`响应体: 失败`
```json
{
  "code":"200",
  "data":{
    "imageId":12345,
    "lockState":"LOCKED"
  },
  "msg":"OK",
  "type":"LOCK_STATE"
}
```

+ LOCK_STATE 获取图片锁定状态

`请求参数`
```json
{
  "type": "LOCK_STATE",
  "data": {
    "image_id": 12345
  }
}
```
`响应体: 成功`
```json
{
    "code":"200",
    "data":{
        "imageId":12345,
        "lockState":"LOCKED"
    },
    "msg":"OK",
    "type":"LOCK_STATE"
}
```


+ 请求出错响应示例
```json
{
  "code":"50002",
  "msg":"参数不合法"
}
```
