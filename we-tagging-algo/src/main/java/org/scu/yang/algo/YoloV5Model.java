package org.scu.yang.algo;

import ai.djl.Device;
import ai.djl.inference.Predictor;
import ai.djl.modality.Classifications;
import ai.djl.modality.cv.Image;
import ai.djl.modality.cv.output.BoundingBox;
import ai.djl.modality.cv.output.DetectedObjects;
import ai.djl.modality.cv.output.Rectangle;
import ai.djl.modality.cv.translator.YoloV5Translator;
import ai.djl.repository.zoo.Criteria;
import ai.djl.repository.zoo.ModelZoo;
import ai.djl.repository.zoo.ZooModel;
import ai.djl.translate.TranslateException;
import ai.djl.translate.Translator;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Data
public class YoloV5Model extends Reason {
    private final static String ENGINE_TYPE = "OnnxRuntime";
    private Translator<Image, DetectedObjects> translator;
    private Criteria<Image, DetectedObjects> criteria;
    private ZooModel<Image, DetectedObjects> model;
    private Boolean isLoaded = false;
    private AlgoContext context;

    public YoloV5Model(AlgoContext context) {
        if (!context.isEnable()){
            isLoaded = false;
            return;
        }
        this.context = context;
        synchronized (isLoaded) {
            isLoaded =  loadModel(context);
        }
    }

    private boolean loadModel(AlgoContext context) {
        boolean result = false;
        try {
            translator = YoloV5Translator
                    .builder()
                    .optSynsetArtifactName(context.getArtifactPath())
                    .optThreshold(context.getThreshold())
                    .build();
            criteria =Criteria.builder()
                    .setTypes(Image.class, DetectedObjects.class)
                    .optDevice(Device.cpu())
                    .optModelPath(Paths.get(context.getModelFile()))
                    .optModelName(context.getModelCode())
                    .optTranslator(translator)
                    .optEngine(ENGINE_TYPE)
                    .build();
            log.info("criteria: {}", criteria.getDevice());
            model = ModelZoo.loadModel(criteria);
            result = true;
        }catch (Exception e) {
            log.error("BloodCellYoloModelContent loadModel Exception: {}", e);
        }
        return result;
    }


    @Override
    public DetectedObjects detectObjects(String path) {
        DetectedObjects detectedObjects = null;
        try {
            InputStream inputStream = new FileInputStream(path);
            Image image = ImageUtil.resizeImage(inputStream, context.getWidth(), context.getHeight());
            if (!isLoaded || model == null) {
                log.warn("BloodCellYoloModelContent detectObjects model is null: {} {}", isLoaded, model);
                return null;
            }
            Predictor<Image, DetectedObjects> predictor = model.newPredictor();
            detectedObjects = predictor.predict(image);
            log.info("BloodCellYoloModelContent detectObjects detectedObjects: {}", detectedObjects);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (TranslateException e) {
            throw new RuntimeException(e);
        }
        return transferDetectedObjects(detectedObjects);
    }

    private DetectedObjects transferDetectedObjects(DetectedObjects data) {
        if (data == null) {
            return null;
        }
        int size = data.items().size();
        List<BoundingBox> boxes = new ArrayList<>(size);
        List<String> names = new ArrayList<>(size);
        List<Double> prob = new ArrayList<>(size);
        for (Classifications.Classification obj :data.items()) {
            DetectedObjects.DetectedObject objConvered = (DetectedObjects.DetectedObject) obj;
            BoundingBox box = objConvered.getBoundingBox();
            Rectangle rec = box.getBounds();
            double x = rec.getX() + rec.getWidth() / 2;
            double y = rec.getY() + rec.getHeight() / 2;
            Rectangle rec2 = new Rectangle(
                    x / context.getWidth(),
                    y / context.getWidth(),
                    rec.getWidth() / context.getWidth(),
                    rec.getHeight() / context.getWidth()
            );
            boxes.add(rec2);
            names.add(obj.getClassName());
            prob.add(obj.getProbability());
        }
        return new DetectedObjects(names, prob, boxes);
    }

}
