package org.scu.yang.algo;

import ai.djl.Device;
import ai.djl.engine.Engine;
import ai.djl.modality.cv.output.DetectedObjects;

public abstract class Reason {
    public DetectedObjects detectObjects(String path) {
        return null;
    }

    public boolean havaGpu() {
        return Engine.getInstance().getGpuCount()>0;
    }


}
