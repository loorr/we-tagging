package org.scu.yang.algo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AlgoContext {

    private String artifactPath;
    private String classMappingFile;
    private String modelType;
    private String modelFile;
    private float threshold = 0.1f;
    private String modelCode = UUID.randomUUID().toString();
    private int width = 640;
    private int height = 640;
    private boolean enable = false;

    private HashMap<String, LabelItemVo> classMapping = new HashMap<>();
}
