package org.scu.yang.algo;

import lombok.Data;

@Data
public class LabelItemVo {
    private Integer categoryId;
    private Integer classId;
}
