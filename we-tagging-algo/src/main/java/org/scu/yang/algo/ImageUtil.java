package org.scu.yang.algo;

import ai.djl.modality.cv.Image;
import ai.djl.modality.cv.ImageFactory;
import ai.djl.modality.cv.output.DetectedObjects;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ImageUtil {
    public static Image resizeImage(InputStream inputStream, int width, int height) {
        BufferedImage input = null;
        try {
            input = ImageIO.read(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int iw = input.getWidth(), ih = input.getHeight();
        int w = width, h = height;
        double scale = Math.min(1. *  w / iw, 1. * h / ih);
        int nw = (int) (iw * scale), nh = (int) (ih * scale);
        java.awt.Image img;
        //只有太长或太宽才会保留横纵比，填充颜色
        //boolean needResize = 1. * iw / ih > 1.4 || 1. * ih / iw > 1.4;
        boolean needResize = false;
        if (needResize) {
            img = input.getScaledInstance(nw, nh, BufferedImage.SCALE_SMOOTH);
        } else {
            img = input.getScaledInstance(width, height, BufferedImage.SCALE_SMOOTH);
        }
        BufferedImage out = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics g = out.getGraphics();
        //先将整个224*224区域填充128 128 128颜色
        g.setColor(new Color(128, 128, 128));
        g.fillRect(0, 0, width, height);
        out.getGraphics().drawImage(img, 0, needResize ? (height - nh) / 2 : 0, null);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            ImageOutputStream imageOutputStream = ImageIO.createImageOutputStream(outputStream);
            ImageIO.write(out, "jpg", imageOutputStream);
            //去D盘看效果
            ImageIO.write(out, "jpg", new File("C:\\Users\\loorr\\Desktop\\out.jpg"));
            InputStream is = new ByteArrayInputStream(outputStream.toByteArray());
            return ImageFactory.getInstance().fromInputStream(is);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("图片转换失败");
        }
    }

    /**
     * 裁剪图片
     * @param x   起始x坐标
     * @param y   起始y坐标
     * @param w  要裁剪的图片的宽度
     * @param h  要裁剪的图片的高度
     */
    public BufferedImage tailor(BufferedImage input, int x, int y, int w, int h) {
        return input.getSubimage(0,0,300,300);
    }

    public static void saveImage(Image img, String outPath, DetectedObjects detection){
        try {
            Path folder = Paths.get(outPath);
            Path outputDir = Paths.get(folder.getParent().toString());
            Files.createDirectories(outputDir);

            if (detection != null){
                img.drawBoundingBoxes(detection);
            }
            // OpenJDK can't save jpg with alpha channel
            img.save(Files.newOutputStream(folder), "png");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void saveImage(Image img, String outPath){
        saveImage(img, outPath, null);
    }
}
