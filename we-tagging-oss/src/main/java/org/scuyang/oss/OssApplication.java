package org.scuyang.oss;

import lombok.extern.log4j.Log4j2;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;


@Log4j2
@EnableAsync
@MapperScan("org.scuyang.dao")
@EnableFeignClients(basePackages = {
        "org.example.account.center.api",
        "org.scuyang.rest.api.feign"
})
@SpringBootApplication(scanBasePackages = {"org.scuyang.oss", "org.scuyang.core"})
public class OssApplication {
    public static void main(String[] args) {
        SpringApplication.run(OssApplication.class, args);
    }
}
