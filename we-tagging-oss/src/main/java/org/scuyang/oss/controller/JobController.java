package org.scuyang.oss.controller;

import com.tove.web.infra.common.BaseErrorCode;
import com.tove.web.infra.common.Response;
import lombok.extern.slf4j.Slf4j;
import org.scuyang.oss.api.JobApi;
import org.scuyang.oss.api.error.OssException;
import org.scuyang.oss.api.req.*;
import org.scuyang.core.oss.JobService;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

@Slf4j
@RestController
public class JobController implements JobApi {

    @Resource
    private JobService jobService;

    @Override
    public Response<Boolean> batchImageRecognize(BatchImageRecognizeReq req) {
        jobService.batchImageRecognize(req);
        return Response.getOk(true);
    }

    @Override
    public Response<Boolean> singleImageRecognize(SingleImageRecognizeReq req) {
        //jobService.singleImageRecognize(req);
        jobService.imageRecognize(req);
        return Response.getOk(true);
    }

    @Override
    public Response<Boolean> importImageFolder(ImportImageFolderJobReq req) {
        try {
            jobService.importImageFolder(req);
        }catch (OssException e){
            throw e;
        } catch (IOException e) {
            throw new OssException(BaseErrorCode.SYSTEM_BUSY);
        }
        return Response.getOk(true);
    }

    @Override
    public Response<Boolean> importMulImageFolder(ImportMulImageFolderJobReq req) {
        try {
            ArrayList<File> files = new ArrayList<>();
            File file = new File(req.getSourcePath());
            File[] tempList = file.listFiles();

            for (int i = 0; i < tempList.length; i++) {
                if (tempList[i].isFile()) {
                    //files.add(tempList[i].toString());
                    continue;
                }
                if (tempList[i].isDirectory()) {
                    files.add(tempList[i]);
                }
            }
            for (File groupFile: files){
                ImportImageFolderJobReq importImageFolderJobReq = new ImportImageFolderJobReq();
                importImageFolderJobReq.setSourcePath(groupFile.getAbsolutePath());
                importImageFolderJobReq.setVersion(req.getVersion());
                importImageFolderJobReq.setGroup(groupFile.getName());
                log.info("importImageFolderJobReq: {}", importImageFolderJobReq);
                jobService.importImageFolder(importImageFolderJobReq);
            }
        }catch (OssException e){
            throw e;
        } catch (IOException e) {
            throw new OssException(BaseErrorCode.SYSTEM_BUSY);
        }
        return Response.getOk(true);
    }

//    @Override
//    public Response<Boolean> importLocalImageFolder(ImportLocalFolderReq req) {
//        try {
//            jobService.importLocalImageFolder(req);
//        }catch (OssException e){
//            throw e;
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new OssException(BaseErrorCode.SYSTEM_BUSY);
//        }
//        return Response.getOk(true);
//    }

    @Override
    public Response<Boolean> exportImageLabelInfo(ImportImageFolderJobReq req) {
        return null;
    }

}
