package org.scuyang.oss.controller;

import com.tove.web.infra.common.PageResult;
import com.tove.web.infra.common.Response;
import org.scuyang.core.ImageService;
import org.scuyang.oss.api.ImageManagerApi;
import org.scuyang.oss.api.req.GetImagePageAdminReq;
import org.scuyang.oss.api.req.ImportImageReq;
import org.scuyang.oss.api.vo.ImageVo;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class ImageController implements ImageManagerApi {

    @Resource
    private ImageService imageService;

    @Deprecated
    @Override
    public Response<Boolean> importImage(ImportImageReq req) {
        return null;
    }

    @Deprecated
    @Override
    public Response<Boolean> batchImportImages(ImportImageReq req) {
        return null;
    }

    @Override
    public Response<PageResult<ImageVo>> getImagePage(GetImagePageAdminReq req) {
        PageResult<ImageVo> result = imageService.getImagePage(req);
        return Response.getOk(result);
    }
}
