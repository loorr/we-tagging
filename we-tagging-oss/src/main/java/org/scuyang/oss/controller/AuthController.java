package org.scuyang.oss.controller;

import com.tove.web.infra.common.BaseErrorCode;
import com.tove.web.infra.common.Response;
import org.scuyang.oss.api.AuthApi;
import org.scuyang.oss.api.error.OssErrorCode;
import org.scuyang.oss.api.error.OssException;
import org.scuyang.oss.api.req.LoginReq;
import org.scuyang.oss.api.vo.LoginVo;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class AuthController implements AuthApi {

    private HashMap<String,String> usernamePwdMap;

    public AuthController(){
        usernamePwdMap = new HashMap<>();
        usernamePwdMap.put("admin", "admin111");
        usernamePwdMap.put("user1", "wydh34829");
        usernamePwdMap.put("oprator", "chjkwkw1");
        usernamePwdMap.put("user2-page1", "test");
    }

    @Override
    public Response<LoginVo> login(LoginReq req) {
        String password = usernamePwdMap.get(req.getUsername());
        if (password == null || !req.getPassword().equals(password)){
            throw new OssException(OssErrorCode.USER_PASSWORD_ERROR);
        }
        LoginVo result = new LoginVo();
        result.setToken(req.getUsername());
        return Response.getOk(result);
    }

    @Override
    public Response<LoginVo> getUserInfo(String token) {
        if (!StringUtils.hasText(token)){
            throw new OssException(BaseErrorCode.ILLEGAL_PARAMETERS);
        }
        return null;
    }
}
