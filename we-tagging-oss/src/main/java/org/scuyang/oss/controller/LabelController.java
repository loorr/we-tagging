package org.scuyang.oss.controller;

import com.tove.web.infra.common.PageResult;
import com.tove.web.infra.common.Response;
import org.scuyang.core.LabelConfigService;
import org.scuyang.oss.api.LabelManagerApi;
import org.scuyang.oss.api.req.AddOrUpdateLabelAdminReq;
import org.scuyang.oss.api.req.DeleteLabelAdminReq;
import org.scuyang.oss.api.req.GetLabelInfoPageAdminReq;
import org.scuyang.oss.api.vo.LabelConfigOssVo;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class LabelController implements LabelManagerApi {

    @Resource
    private LabelConfigService labelConfigService;

    @Override
    public Response<Boolean> addLabelInfo(AddOrUpdateLabelAdminReq req) {
        return Response.getOk(labelConfigService.addLabelInfo(req));
    }

    @Override
    public Response<Boolean> updateLabelInfo(AddOrUpdateLabelAdminReq req) {
        return Response.getOk(labelConfigService.updateLabelInfo(req));
    }

    @Override
    public Response<Boolean> deleteLabelInfo(DeleteLabelAdminReq req) {
        return Response.getOk(labelConfigService.deleteLabelInfo(req));
    }

    @Override
    public Response<PageResult<LabelConfigOssVo>> getLabelInfoPage(GetLabelInfoPageAdminReq req) {
        PageResult<LabelConfigOssVo> result = labelConfigService.getLabelInfoPage(req);
        return Response.getOk(result);
    }
}
