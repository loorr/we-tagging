package org.scuyang.oss.controller;

import com.tove.web.infra.common.Response;
import lombok.extern.slf4j.Slf4j;
import org.scuyang.core.oss.FileManageService;
import org.scuyang.oss.api.FileManagerApi;
import org.scuyang.oss.api.error.OssErrorCode;
import org.scuyang.oss.api.error.OssException;
import org.scuyang.oss.api.req.AddFileReq;
import org.scuyang.oss.api.req.AddImagesByLocalZipReq;
import org.scuyang.oss.api.req.ExportLabelAndImageReq;
import org.scuyang.oss.api.vo.ExportLabelAndImageVo;
import org.scuyang.core.oss.JobService;
import org.scuyang.oss.api.vo.ImageAccessVo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Slf4j
@RestController
public class FileManagerController implements FileManagerApi {

    @Value("${oss.root.temp.folder}")
    private String rootTempFolder;

    @Resource
    private JobService jobService;

    @Resource
    private FileManageService fileManageService;

    @Override
    public Response<ImageAccessVo> addFile(MultipartFile file, AddFileReq req) {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        Path path = Paths.get(rootTempFolder + fileName);
        try {
            Files.copy(file.getInputStream(),path, StandardCopyOption.REPLACE_EXISTING);
            ImageAccessVo imageAccessVo = jobService.addNewImage(new File(rootTempFolder + fileName), req.getVersion(), req.getGroup());
            return Response.getOk(imageAccessVo);
        } catch (IOException e) {
            throw new OssException(OssErrorCode.FILE_ERROE);
        }
    }

    @Override
    public Response<Boolean> addCosFile(AddFileReq req) {
        return null;
    }

    @Override
    public Response<ExportLabelAndImageVo> exportLabelAndImage(ExportLabelAndImageReq req) {
        ExportLabelAndImageVo exportLabelAndImageVo = jobService.exportLabelAndImage(req);
        return Response.getOk(exportLabelAndImageVo);
    }

    @Override
    public Response<Boolean> addBatchImagesByZip(MultipartFile file) {
        String imageVersion = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            Path path = Paths.get(rootTempFolder + imageVersion);
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
            log.info("文件上传完成 {}", file.getName());
            file.getInputStream().close();
            fileManageService.addBatchImagesByZip(path.toString(), rootTempFolder);

            return Response.getOk(true);
        } catch (IOException e) {
            e.printStackTrace();
            throw new OssException(OssErrorCode.FILE_ERROE);
        }
    }

    @Override
    public Response<Boolean> addBatchImagesByLocalZip(AddImagesByLocalZipReq req) {
        return null;
    }


}
