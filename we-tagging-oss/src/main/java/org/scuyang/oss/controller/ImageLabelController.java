package org.scuyang.oss.controller;

import com.tove.web.infra.common.PageResult;
import com.tove.web.infra.common.Response;
import org.scuyang.core.TagService;
import org.scuyang.oss.api.ImageLabelApi;
import org.scuyang.oss.api.req.GetImageLabelPageAdminReq;
import org.scuyang.oss.api.vo.ImageLabelVo;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class ImageLabelController implements ImageLabelApi {

    @Resource
    private TagService tagService;

    @Override
    public Response<PageResult<ImageLabelVo>> getImageLabelPage(GetImageLabelPageAdminReq req) {
        PageResult<ImageLabelVo> result = tagService.getImageLabelPage(req);
        return Response.getOk(result);
    }
}
