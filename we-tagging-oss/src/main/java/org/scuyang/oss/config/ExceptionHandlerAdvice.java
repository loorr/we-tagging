package org.scuyang.oss.config;

import com.tove.web.infra.common.BaseErrorCode;
import com.tove.web.infra.common.Response;
import lombok.extern.slf4j.Slf4j;
import org.scuyang.oss.api.error.OssException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ExceptionHandlerAdvice {

    @ExceptionHandler(value = OssException.class)
    public Response<?> exception(OssException exception, HandlerMethod handlerMethod, HttpServletResponse response){
        log.warn("{} OssException : {}", handlerMethod.getMethod(), exception.getMsg());
        return Response.getFail(exception);
    }

    @ExceptionHandler(value = Exception.class)
    public Response<?> exception(Exception exception, HandlerMethod handlerMethod, HttpServletResponse response){
        log.warn("{} Exception : {}", handlerMethod.getMethod(), exception);
        return Response.getFail(BaseErrorCode.SYSTEM_BUSY);
    }
}
