package org.scuyang.oss.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.StringCodec;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class RedissonConfig {

    @Value("${spring.redis.host}")
    private String redisHost;
    @Value("${spring.redis.port}")
    private String redisPort;
    @Value("${spring.redis.db:0}")
    private String redisDb;

    @Bean(name = "weTaggingRedissonClient", destroyMethod = "shutdown")
    public RedissonClient redissonClient() {
        Config config = new Config();
        config.setCodec(new StringCodec()).useSingleServer()
                .setDatabase(Integer.parseInt(redisDb))
                .setPingConnectionInterval(1000)
                .setAddress("redis://" + redisHost + ":" + redisPort);

        return Redisson.create(config);
    }
}