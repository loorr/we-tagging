package org.scuyang.oss.job;

import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.scuyang.core.oss.JobService;
import org.scuyang.dao.ImageInfoMapper;
import org.scuyang.dao.LabelInfoMapper;
import org.scuyang.oss.api.req.SingleImageRecognizeReq;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@Service
public class AlgoJob {

    @Value("${we.tagging.algo.job-enabled:false}")
    private String jobEnabled;

    @Value("${we.tagging.algo.image-version:血细胞图V1}")
    private String imageVersion;

    @Resource
    private RedissonClient redissonClient;

    @Resource
    private ImageInfoMapper imageInfoMapper;

    @Resource
    private LabelInfoMapper labelInfoMapper;
    @Resource
    private JobService jobService;

    private Boolean isRunning = false;
    private Integer interval = 1000*5;

    @Scheduled(fixedRate = 1000 * 60)
    public void synConfig() {
        log.info("synConfig( {}", System.currentTimeMillis());
        RBucket<String> bucket = redissonClient.getBucket("we.tagging.algo.job-enabled");
        String data = bucket.get();
        jobEnabled = data == null ? jobEnabled : data;


        RBucket<String> bucket2 = redissonClient.getBucket("we.tagging.algo.interval");
        String data2 = bucket2.get();
        if (data2 == null) {
            bucket2.set(String.valueOf(interval));
        }else{
            interval = Integer.valueOf(data2);
        }
        runAlgo();
    }

    @Scheduled(cron = "0 0 2 * * ?")
    public void runAlgo(){
        if (!Boolean.parseBoolean(jobEnabled)) {
            return;
        }
        if (isRunning) {
            return;
        }
        try {
            isRunning = true;
            long startTime = System.currentTimeMillis();
            for (String version : imageVersion.split(",")) {
                List<Long> imageIds = imageInfoMapper.getAllNoTagImageIdList(version);
                for (int i = 0; i < imageIds.size(); i++) {
                    Long imageId = imageIds.get(i);
                    int count = labelInfoMapper.getAlgorithmLabelsCount(imageId, "HEMOCYTE-yolo", "1.0.0");
                    log.info("imageId:{}, count:{}", imageId, count);
                    if (count > 0) {
                        continue;
                    }
                    SingleImageRecognizeReq req = new SingleImageRecognizeReq();
                    req.setImgId(imageId);
                    jobService.singleImageRecognize(req);
                    Thread.sleep(interval);
                    long end = System.currentTimeMillis();
                    if (i % 10 == 0) {
                        log.info("version: {}, 已处理{}张图片, 耗时： {} s", version, i, (end - startTime) / 1000);
                        if (!Boolean.parseBoolean(jobEnabled)) {
                            return;
                        }
                    }
                    if (end -startTime > 1000 * 60 * 60 * 12) {
                        return;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            isRunning = false;
        }
        System.out.println("end algo job");
    }
}

