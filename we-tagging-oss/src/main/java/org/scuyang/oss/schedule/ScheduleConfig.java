package org.scuyang.oss.schedule;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import javax.annotation.Resource;
import java.util.concurrent.Executor;

@Configuration
@EnableScheduling
public class ScheduleConfig implements SchedulingConfigurer {
    @Resource(name = "scheduleExecutor")
    private Executor scheduleExecutor;

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setScheduler(scheduleExecutor);
    }
}
