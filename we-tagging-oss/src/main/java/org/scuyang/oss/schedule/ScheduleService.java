package org.scuyang.oss.schedule;

import org.scuyang.common.enums.TaskNameEnum;
import org.scuyang.common.enums.TaskStateEnum;
import org.scuyang.common.model.AsynTask;
import org.scuyang.core.AsynTaskService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;


@Component
public class ScheduleService {
    @Resource
    private AsynTaskService asynTaskService;

//    @Scheduled(fixedRate=300 * 1000)
//    public void listenAsynState(){
//        List<AsynTask> asynTaskList = asynTaskService.getTaskList(TaskNameEnum.BATCH_IMAGE_RECOGNIZE, TaskStateEnum.CREATE);
//        if (CollectionUtils.isEmpty(asynTaskList)){
//            return;
//        }
//        // read Task
//        for (AsynTask asynTask: asynTaskList){
//            asynTask.setTaskState(TaskStateEnum.ABORT_END);
//            asynTaskService.updateTask(asynTask);
//        }
//    }
}
