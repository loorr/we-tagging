package org.scuyang.oss.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.scuyang.common.enums.TaskNameEnum;
import org.scuyang.common.enums.TaskStateEnum;
import org.scuyang.common.model.AsynTask;
import org.scuyang.core.AsynTaskService;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@SpringBootTest
public class AsynTaskServiceTest {

    @Resource
    private AsynTaskService asynTaskService;

    @Test
    void testAddNewTask(){
        asynTaskService.startNewTask(TaskNameEnum.BATCH_IMAGE_RECOGNIZE);
        List<AsynTask> tasks = asynTaskService.getTaskList(TaskNameEnum.BATCH_IMAGE_RECOGNIZE, TaskStateEnum.CREATE);
        log.info(tasks.toString());
    }
}
