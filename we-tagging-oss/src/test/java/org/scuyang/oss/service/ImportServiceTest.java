package org.scuyang.oss.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.scuyang.core.oss.ImportService;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@Slf4j
@SpringBootTest
public class ImportServiceTest {

    @Resource
    private ImportService importService;

    @Test
    void importImageLabelFromTxtTest(){
        importService.importImageLabelFromTxt("C:\\Users\\zjianfa\\Pictures\\ossRoot\\test1\\3294");
    }

}
