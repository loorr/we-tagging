package org.scuyang.oss;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.scuyang.core.mq.RedisQueueMsgProducer;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;


@Slf4j
@SpringBootTest
class RedisQueueMsgProducerTest {

    @Resource
    private RedisQueueMsgProducer redisQueueMsgProducer;

    @Test
    void sendData() {
        String data = "hello";
        StringBuffer stringBuffer = new StringBuffer(10000);
        for (int i = 0; i < 1000; i++) {
            stringBuffer.append(data);
        }
        redisQueueMsgProducer.sendData(stringBuffer.toString());
    }
}