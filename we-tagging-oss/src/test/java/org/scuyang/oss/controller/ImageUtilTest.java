package org.scuyang.oss.controller;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.scuyang.common.util.ImageUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;

@Slf4j
@SpringBootTest
public class ImageUtilTest {

    @Value("${org.scuyang.oss.root.folder}")
    private String folderPath;
    private File file;

    @BeforeEach
    void setUp() {
        String fileName = "/IMG_20190410_193252.jpg";
        String path = folderPath + fileName;
        this.file = new File(path);
    }


    @Test
    void testImageInfo(){
        ImageUtil.getImageInfo(file);
    }
}
