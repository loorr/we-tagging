
port=4000
#一、根据端口号查询对应的pid,两种都行
pid=$(netstat -nlp | grep :$port | awk '{print $7}' | awk -F"/" '{ print $1 }');
#pid=$(ps -ef | grep 你的进程或端口 | grep -v grep | awk '{print $2}')
# 二、杀掉对应的进程，如果pid不存在，则不执行
if [ -n "$pid" ]; then 　　kill -9 $pid; fi

mv /home/frp_0.38_app/frp_0.38_app.log /home/frp_0.38_app/frp_0.38_app.log.$(date "+%Y%m%d-%H%M%S")
nohup /home/frp_0.38_app/frpc -c /home/frp_0.38_app/frpc.ini >/home/frp_0.38_app/frp_0.38_app.log 2>&1 &
echo "frp_0.38_app is start ..."
while read line
do
    echo $line
done < /home/frp_0.38_app/frp_0.38_app.log

mv /home/java/oss-prod/v4/application-oss.log /home/java/oss-prod/v4/application-oss.log.$(date "+%Y%m%d-%H%M%S")
nohup java -Djava.security.egd=file:/prod/./urandom -Dspring.profiles.active=prod -Dserver.port=4000 -jar /home/java/oss-prod/v4/application-oss.jar >/home/java/oss-prod/v4/application-oss.log 2>&1 &
echo "oss-prod dev is start ..."
while read line
do
    echo $line
done < /home/java/oss-prod/v4/application-oss.log

mv /home/java/oss-dev/application-oss.log /home/java/oss-dev/application-oss.log.$(date "+%Y%m%d-%H%M%S")
nohup java -Djava.security.egd=file:/dev/./urandom -Dspring.profiles.active=dev-linux -Dserver.port=4001 -jar /home/java/oss-dev/application-oss.jar >/home/java/oss-dev/application-oss.log 2>&1 &
echo "oss-dev dev is start ..."
while read line
do
    echo $line
done < /home/java/oss-dev/application-oss.log
su
conda activate yolo
ps -ef | grep /home/model/image_server.py | grep -v grep | awk '{print $2}' | xargs kill -9

mv /home/model/image_server.log /home/model/image_server.log.$(date "+%Y%m%d-%H%M%S")
nohup python /home/model/image_server.py >/home/model/image_server.log 2>&1 &
echo "python server is start ..."
while read line
do
    echo $line
done < /home/model/image_server.log

# 启动python server


